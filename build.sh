
# this needs to be a full path
#BUILD_DIR=/home/ubuntu/luna/
BUILD_DIR=/Users/shaun/src/luna
MAKE_FLAGS="-j 8"

##
## External dependencies
##

#tar -czvf extern-1.1.tar.gz extern/fftw-3.3.4.tar.gz extern/fidlib-0.9.10.tgz \
#  extern/libharu-libharu-RELEASE_2_3_0RC3-19-gd84867e.tar.gz \
#  extern/libpng-1.6.21.tar.gz extern/libsamplerate-0.1.8.tar.gz \
#  extern/wxWidgets-3.0.2.tar.gz extern/zlib-1.2.8.tar.gz

## 
## Notes
##


# wxWidgets on MacOS
# 1) wxWidgets build on MacOS10.10 will fail unless the following change is made:
#         edit 'src/osx/webview_webkit.mm '
#         -#include <WebKit/WebKit.h> 
#         +#include <WebKit/WebKitLegacy.h> 
# 2) use configure below with --with-macosx option

## 
## libhpdf requires autoconf: 
#  sudo port install autoconf
#  sudo port install automake
#  sudo port install libtool
# and libpng:
#  sudo port install libpng


##
## Build psgtools dependencies (in extern/)
##

tar -xzvf extern-1.1.tar.gz

cd extern

FFTW=fftw-3.3.4
#FIDLIB=fidlib-0.9.10
LIBSAMPLERATE=libsamplerate-0.1.8


ZLIB=zlib-1.2.8
LIBPNG=libpng-1.6.21
LIBHPDF=libharu-libharu-RELEASE_2_3_0RC3-19-gd84867e
LIBHPDF2=libharu-libharu-d84867e

tar -xzvf ${FFTW}.tar.gz
tar -xzvf ${LIBSAMPLERATE}.tar.gz
tar -xzvf ${ZLIB}.tar.gz
tar -xzvf ${LIBPNG}.tar.gz
tar -xzvf ${LIBHPDF}.tar.gz
#tar -xzvf ${FIDLIB}.tgz

mkdir -p ${BUILD_DIR}/exec/depends


##
## FFTW
##

cd ${FFTW}
./configure --prefix=${BUILD_DIR}/exec/depends/
make ${MAKE_FLAG} && make install


##
## FIDLIB
##

cd ../${FIDLIB}
./mk-firun
cp fidlib.h fidlib.c fidmkf.h fidrf_cmdlist.h ../../dsp/
#To use 'fidlib' (one way at least):
# Copy fidlib.h, fidlib.c, fidmkf.h and fidrf_cmdlist.h to your project directory
#  Build fidlib.c as part of your project
#  #include "fidlib.h" in your source for prototypes and structures
#  See fidlib.txt for full docs and API
#  See also fidlib.c and fidlib.h for additional notes


##
## LIBSAMPLERATE
##

cd ../${LIBSAMPLERATE}
./configure --prefix=${BUILD_DIR}/exec/depends/
make ${MAKE_FLAGS} && make install


##
## ZLIB
##

cd ../${ZLIB}
./configure --prefix=${BUILD_DIR}/exec/depends/
make ${MAKE_FLAGS} && make install


##
## LIBPNG
##

cd ../${LIBPNG}
./configure --prefix=${BUILD_DIR}/exec/depends/
make ${MAKE_FLAGS} && make install


##
## LIBHPDF
##

cd ../${LIBHPDF2}
./buildconf.sh 
./configure --prefix=${BUILD_DIR}/exec/depends --with-zlib=${BUILD_DIR}/exec/depends/
make ${MAKE_FLAGS} && make install



##
## Build primary luna 
##

cd ../..
make ${MAKE_FLAGS}


##
## Build misc. utilities
##

cd ../utils
sh make.sh
<



##
## WXWIDGETS
## 

WXWIDGETS=wxWidgets-3.0.2

tar -xzvf ${WXWIDGETS}.tar.gz

cd ../${WXWIDGETS}
#./configure --prefix=${BUILD_DIR}/depends --with-macosx-version-min=10.10
./configure --prefix=${BUILD_DIR}/exec/depends 
make ${MAKE_FLAGS} && make install


##
## Build scope viewer
##

cd scope
make ${MAKE_FLAGS}




##
## All done
##

cd ..
 
echo 
echo "Finished build."
echo

