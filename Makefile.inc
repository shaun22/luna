
############
## Linux   #
############

CXX             = g++
CXXFLAGS	= -O2 -I.. -I/usr/local/include -I./exec/depends/include -I../exec/depends/include -std=c++0x -Wno-logical-op-parentheses -Wno-deprecated-register
CFLAGS = -O2 -DSQLITE_THREADSAFE=0 -DSQLITE_OMIT_LOAD_EXTENSION -DT_LINUX
LD		= g++ 


export CFLAGS
export CXXFLAGS
export CFLAGS
export LD

############
## OSX     #
############


#CXXFLAGS	= -g -O2 -I.. -I/usr/local/include -I./depends/include -std=c++11 -stdlib=libc++ 

## Alternate (clang)

#CXX            = clang++
#CXXFLAGS	= -g -O2 -I.. -I/usr/local/include -I./depends/include -I./splot/inst/include 
#CXXFLAGS	= -g -O2 -I.. -stdlib=libstdc++



#LD		= clang++
#LD		= g++ -stdlib=libstdc++
LDLAGS          = -L/usr/local/lib
AR		= ar
ARFLAGS		=
RANLIB		= ranlib
RM		= rm -f
ECHO		= echo
SHELL		= /bin/sh

.SILENT :



