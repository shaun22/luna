
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#include "lw.h"
#include "web.h"
#include "param.h"
#include "misc.h"
#include "../sstore/sstore.h"
#include "../fftw/fftwrap.h"
#include "../clocs/topo.h"

extern lw_param_t  param;

lw_signals_t::lw_signals_t( const std::string & edf_file , const std::string & indiv_id )
{
  edf = new edf_t;
  
  bool okay = edf->attach( edf_file , indiv_id );
  
  if ( ! okay ) Helper::halt( "could not load EDF" + edf_file );
  else std::cout << "<p><em>Attached :</em> " << edf_file << "</p>";   
}




void lw_signals_t::epoch( )
{


  
  //
  // epoch_duration ranges from 1 to 6
  //
  
  epoch_duration_sec.clear();
  epoch_duration_sec.push_back( 0 );   // null
  epoch_duration_sec.push_back( 1 );   // 1
  epoch_duration_sec.push_back( 3 );   // 2
  epoch_duration_sec.push_back( 10 );  // 3 (default)
  epoch_duration_sec.push_back( 30 );  // 4
  epoch_duration_sec.push_back( 90 );  // 5
  epoch_duration_sec.push_back( 270 ); // 6


  //
  // Either an epoch specified, or an 'a/b' pair in seconds
  // In the latter case, we need to convert to an epoch format
  //
  
  if ( param.e == -1 ) 
    {
      
      if ( param.a < 0 || param.b < 0 ) Helper::halt( "bad window" );
      
      // total EDF duration in seconds
      double seconds = edf->header.nr * edf->header.record_duration;
      
      double width = param.b - param.a;       
      
      if      ( width < 1 )  { param.epoch_duration = 1; } // 1 sec bounding window
      else if ( width < 3 )  { param.epoch_duration = 2; } 
      else if ( width < 10 ) { param.epoch_duration = 3; } // default 10 sec
      else if ( width < 30 ) { param.epoch_duration = 4; } // 30 sec epoch
      else if ( width < 90 ) { param.epoch_duration = 5; } // 3 epochs
      else                   { param.epoch_duration = 6; } // largest possible window
      
      // which epoch does 'a' start in? 
      int epocha = floor( param.a / epoch_duration_sec[ param.epoch_duration ] );

      int epochb = floor( param.b / epoch_duration_sec[ param.epoch_duration ] );
      
      // does the interval fall within a single epoch? if not, 
      // we need to go up one size if we can
      if ( epocha != epochb ) 
	{ 
	  if ( param.epoch_duration < 6 ) ++param.epoch_duration;
	}
      
      // Set epoch as that which 'a' falls in 

      param.e = floor( param.a / epoch_duration_sec[ param.epoch_duration ] );
      
    }

    
  //
  // Epoch number
  //

  int epoch = param.e; 
  

  //
  // Epoch duration
  //
  
  if ( param.epoch_duration  < 1 || param.epoch_duration  > 6 ) Helper::halt( "bad window" );
    
  int ed = epoch_duration_sec[ param.epoch_duration ];

  
  //
  // Load display parameters and channel locations
  //

  
  
  lw_signal_disp_t disp( param.folder , edf );


  
  //
  // Determine which signals to display (either in topoplot or signal view)
  //

  
  
  
  //
  // Header
  //
  
  std::cout << "<hr><small>";

  // total EDF duration in seconds (may be fractional)
  double total_duration_sec = edf->header.nr * edf->header.record_duration; 

  // number of (whole) 30-second epochs (defulat is 30,but could be 10 or 20, etc)
  // we'll keep the label as 'ne30' for simplicity within the code however...
  int ne30 = edf->timeline.set_epoch( param.esize , param.esize  );
  
  // number of (whole) ed-second epochs
  int ne = edf->timeline.set_epoch( ed , ed  );

  // i.e. we also need this, the effective duration of the EDF given the epoch size
  int total_epoched_duration_sec = ne * ed;



  //
  // Get time interval for this epoch
  //

  // first sanity check (i.e. w/ zooming in/out, sometimes epoch may end up being larger than ne)

  if ( epoch >= ne ) epoch = ne-1;
  
  interval_t interval = edf->timeline.epoch( epoch );  
  

  //
  // 
  //

  std::cout << "<p>";
  
  std::cout << "[" << web_t::signals_link( "<em>start</em>" , param.folder , param.edf_file , param.indiv , param.signals , 0 , param.epoch_duration ) << "]"; 

  if ( epoch > 1 ) 
    std::cout << "[" << web_t::signals_link( "<em>prev</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch - 1 , param.epoch_duration ) << "]"; 
  
  std::cout << " <b>epoch " << epoch+1 << "</b> of " << ne << " ";

  //  std::cout << " (" << Helper::timestring( interval.duration() ) << " second epochs ) " ;

  // next epoch button
  if ( epoch + 1 < ne ) 
    std::cout << "[" << web_t::signals_link( "<em>next</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch + 1 , param.epoch_duration ) << "]"; 
  
  std::cout << "[" << web_t::signals_link( "<em>end</em>" , param.folder , param.edf_file , param.indiv , param.signals , ne-1 , param.epoch_duration ) << "]"; 

  std::cout << "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";

  //
  // Zooms
  //
  
  if ( param.epoch_duration > 1 ) // shink
    std::cout << "[" << web_t::signals_link( "<em>in</em>" , param.folder , param.edf_file , param.indiv , param.signals , 2 + 3*epoch  , param.epoch_duration-1 ) << "]"; 

  std::cout << " <b>" << ed << "</b> sec(s) ";
  
  if ( param.epoch_duration < 6 ) 
    std::cout << "[" << web_t::signals_link( "<em>out</em>" , param.folder , param.edf_file , param.indiv , param.signals , int( epoch/3) , param.epoch_duration+1 ) << "]"; 
   

  //
  // Duration
  //
  
  uint64_t duration_tp = globals::tp_1sec * (uint64_t)edf->header.nr * edf->header.record_duration ;
  std::cout << " | <em>duration</em>: <b>" <<   Helper::timestring( duration_tp ) << "</b>";

  
  //
  // Clocktime and/or elapsed time
  //

  double hours1 = interval.start_sec() / 3600.0;
  double hours2 = interval.stop_sec() / 3600.0;
  clocktime_t zerotime1, zerotime2;
  zerotime1.advance( hours1 );
  zerotime2.advance( hours2 );

  clocktime_t starttime( edf->header.starttime );
  if ( starttime.valid ) 
    {
      starttime.advance( hours1 );
      std::cout << " | <em>clock:</em> <b>" << starttime.as_string() << "</b>";
    }
  
  std::cout << " | <em>elapsed:</em> <b>" << zerotime1.as_string() << " .. " << zerotime2.as_string() << "</b>";
  
  
  //
  // Signals
  //

  signal_list_t signals;

  // if param.signals is empty, this means to look at all signals
  
  if ( param.signals == "." ) 
    { 
      param.signal_set.clear();
      for (int o=0; o < disp.size(); o++ ) 
	{
	  signals.add( o , disp.order(o) );
	  param.signal_set.insert( disp.order(o) );
	}
    }
  else
    {
      
      //
      // create a signal-string, in the correct order
      //
      
      std::string ordered_signal_string = "";
      bool firstdone = false;
      for (int o=0; o < disp.size(); o++ ) 
	{
	  if ( param.signal_set.find( disp.order(o) ) == param.signal_set.end() ) { continue; }
	  if ( firstdone ) ordered_signal_string += ",";
	  ordered_signal_string += "," + disp.order( o );       
	  firstdone = true;
	}
      
      signals = edf->header.signal_list( ordered_signal_string );
      
    }



  //
  // link to psd-view and topo-plot, if we have that option
  //

  std::cout << "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
 
  if ( param.hilbert ) 
    std::cout << "[" << web_t::raw_link( "<em>raw</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , param.epoch_duration ) << "]";
  else
    std::cout << "[" << web_t::hilbert_link( "<em>Hilbert</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , param.epoch_duration) << "]";
  

  std::cout << "[" << web_t::psd_link( "<em>PSD</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch ) << "]";


  std::cout << " [" << web_t::topo_link( "<em>topoplot</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch ) << "]";
  
  
  //
  // print options
  //

  std::cout << "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";

  std::set<std::string> excls;
  for (int o=0; o < disp.size(); o++ ) 
    {
      if ( param.signal_set.find( disp.order(o) ) != param.signal_set.end() ) { continue; }
      excls.insert( disp.order(o) );
    }
  
  std::cout << "<em><b>signals</b> :</em> [" 
	    << web_t::signals_link( "<em>all</em>" , param.folder , param.edf_file , param.indiv , "." , epoch , param.epoch_duration ) 
	    << "] ["
	    << web_t::signals_link( "<em>none</em>" , param.folder , param.edf_file , param.indiv , "" , epoch , param.epoch_duration ) 
	    << "] ["
	    << web_t::signals_link( "<em>swap</em>" , param.folder , param.edf_file , param.indiv , stringize( excls ) , epoch , param.epoch_duration ) 
	    << "]";
  
  std::cout << "</p><hr>";

  std::cout << "<p><em>Incl:</em>";
  std::set<std::string>::const_iterator ii = param.signal_set.begin();
  while ( ii != param.signal_set.end() )
    {
      std::cout << " [" << web_t::signals_link( disp.display_label( *ii ) , param.folder , param.edf_file , param.indiv , stringize_except( param.signal_set , *ii ) , epoch , param.epoch_duration ) << "] " ;
      ++ii;
    }
  
  std::cout << "</p><p><em>Excl:</em>";
  
  ii = excls.begin();
  while ( ii != excls.end() )
    {
      std::cout << " [" << web_t::signals_link( disp.display_label( *ii ) , param.folder , param.edf_file , param.indiv , stringize_add( param.signal_set , *ii ) , epoch , param.epoch_duration ) << "] " ;
      ++ii;
    }
  
  std::cout << "</p></small><hr>";
  






  //
  // Get all signal data
  //

  const int ns = signals.size();

  mslice_t * slice = new mslice_t( *edf , signals , interval );
  
  const int ns2 = slice->size();

  if ( ns != ns2 ) Helper::halt( "ns != n2" );

  double wid = param.width;

  // +2 is 1 for a spacer (half at top, half at bottom)
  // second is to add staging track (if present) in the top line
  double hgt = (ns+2) * param.sighgt;


  //
  // Mask information?
  //

  std::string mask_db = param.folder + "/" + param.indiv + "/ss/mask.db";
  
  bool has_mask = Helper::fileExists( mask_db );

  // record all 30-second epoch masks
  std::vector<bool> mask30( ne30 , false ) ;
  
  if ( has_mask )
    {
      fetch_mask( mask_db , &mask30 );
    }



  //
  // Staging information
  //

  std::string staging_db = param.folder + "/" + param.indiv + "/ss/staging.db";
  
  bool has_staging = Helper::fileExists( staging_db );

  // record all 30-second epoch stages
  std::vector<std::string> stages30;
  std::vector<bool> wake30;
    

  if ( has_staging )
    {

      
      stage_plot( staging_db , ne, ne30 , epoch , &stages30 , &wake30 , has_mask ? &mask30 : NULL );
      
      
      //
      // Allow mouse click to redirect to this (30-second) epoch
      //
      
      std::cout << "function getMousePos(canvas, evt) { var rect = canvas.getBoundingClientRect(); return { x: evt.clientX - rect.left, y: evt.clientY - rect.top }; }";
										   

      // note: this sets 'ed' to 4 which here means 30 second epochs
      
      std::cout << "canvas1.addEventListener( \"click\", function ( e ) { var mousePos = getMousePos(canvas1, e); "
		<< "window.location = \"lunaweb.cgi?m=sv";
      if ( param.hilbert ) std::cout << "&hlt=1";
      std::cout << "&edf=" << param.edf_file 
		<< "&i=" << param.indiv 
		<< "&s=" << stringize( param.signal_set ) 
		<< "&p=" << param.folder 
		<< "&e=\" + Math.floor( (mousePos.x/" << param.width << ") * " << ne30 << ") + \"&ed=" << param.def_ed << "\"" ;
      
      std::cout << "; }  , false ); "; 

      // end staging canvas
      std::cout << "}</script></canvas>";
      
    }
  


  
  //
  // Pull out per-second stages from stages30
  //
  
  std::vector<std::string> stages;  
  std::vector<bool> mask;  
  if ( has_staging || has_mask )
    {
  
      int sec1 = interval.start_sec();
      int sec2 = sec1 + ed - 1;

      for (int sec=sec1;sec<=sec2;sec++)
	{
	  int i = sec / param.esize ; // default = 30 	  
	  if ( has_mask ) mask.push_back( mask30[ i ] );
	  if ( has_staging) stages.push_back( stages30[ i ] );
	}
    }




  
  
  //
  // PSD/Hjorth panel 
  // If staging/mask present, only look at clean/sleep
  //

  std::string hjorth_db = param.folder + "/" + param.indiv + "/ss/hjorth.db";

  bool has_hjorth = Helper::fileExists( hjorth_db );

  if ( has_hjorth )
    {

      // make plot
      hjorth_plot( hjorth_db , ne30 , &wake30 , &mask30 );
      
      
      //
      // Allow mouse click to redirect to this (30-second) epoch
      //
      
      std::cout << "function getMousePos(canvas, evt) { var rect = canvas.getBoundingClientRect(); return { x: evt.clientX - rect.left, y: evt.clientY - rect.top }; }";
      
      // note: this sets 'ed' to 4 which here means 30 second epochs
      
      std::cout << "canvas1.addEventListener( \"click\", function ( e ) { var mousePos = getMousePos(canvas1, e); "
		<< "window.location = \"lunaweb.cgi?m=sv";
      if ( param.hilbert ) std::cout << "&hlt=1";
      std::cout << "&edf=" << param.edf_file 
  		<< "&i=" << param.indiv 
		<< "&s=" << stringize( param.signal_set ) 
		<< "&p=" << param.folder 
		<< "&e=\" + Math.floor( (mousePos.x/" << param.width << ") * " << ne30 << ") + \"&ed=" << param.def_ed << "\"" ;
      
      std::cout << "; }  , false ); "; 
      
      // END of hjorth plot
      std::cout << "}</script></canvas>";
      
    }


  //
  // Annotations
  //

  lw_annot_t annots;
  
  bool has_annot = fetch_annots( &annots );
  
  if ( has_annot ) annotplots( annots , disp , total_epoched_duration_sec , ne30 );
  
  //
  // Zoomed-in (i.e. epoch window) annot-plot
  //

  std::cout << "</p>";

  if ( has_annot ) annotplots( annots , disp , interval );



  //
  // Main signal plot: either standard or hilbert
  //


      
  //  std::cout << "<p>" << ns << " signal(s) selected</p>";
  
  std::cout << "<canvas id=\"sigCanvas\" height=\"" << hgt << "\" width=\"" << wid << "\">"
	    << "<script>"
	    << "var canvas = document.getElementById('sigCanvas');"
	    << "if (canvas.getContext) { "
	    << "var ctx = canvas.getContext('2d');";
  
  std::cout << "ctx.font = \"10px Arial\";";
  
  // 5%   label
  // 5%   scale
  // 80%  signal
  // 10%  PSD
  
  // signal takes 80%
  
  double pxleft    = param.width * 0.02;
  double pxlabel   = param.width * 0.05;
  
  double smain     = param.width * 0.1; // start of main signal window
  double pxwidth   = param.width * 0.88; // width of this window
  
  //   double spsd         = param.width * 0.92; // start of PSD window, w/ spacer
  //   double pxpsd_width  = param.width * 0.06; // width (i.e. 6% of window) --> 92..98%
  
  double spsd         = param.width * 0.0; // start of PSD window, w/ spacer
  double pxpsd_width  = param.width * 0.0; // width (i.e. 6% of window) --> 92..98%
  
  double h      = param.sighgt;
      
      
  //
  // Staging track and MASK track (each 50% of the plot)
  //
  
  if ( has_staging )
    {
      double s2 = 0.25 * h;
      
      // size of 1 second in pixels
      double px_1sec = pxwidth / (double)ed;
      double px = smain;
      for (int i=0;i<ed;i++)
	{
	  std::cout << "ctx.fillStyle = '" << stages[i]<< "';";      
	  std::cout << "ctx.fillRect(" << px << " , " << s2 << " , " << px_1sec << ", " << h/2 << ");";      
	  px += px_1sec;
	}
    }
  
  if ( has_mask )
    {
      double s2 = 0.75 * h ;
      
      // size of 1 second in pixels
      double px_1sec = pxwidth / (double)ed;
      double px = smain;
      for (int i=0;i<ed;i++)
	{
	  if ( mask[i] ) 
	    {
	      std::cout << "ctx.fillStyle = 'black';";      
	      std::cout << "ctx.fillRect(" << px << " , " << s2 << " , " << px_1sec << ", " << h/2 << ");";      
	    }
	  px += px_1sec;
	}
      
      
    }


  //
  // Either hilbert mode
  //
  
  
  if ( param.hilbert ) 
    {
      
      activityplots( signals , disp , interval , slice );
    }
  else 
    {
      
      //
      // or raw signals
      //
      
      for (int ord=0; ord <  disp.size(); ord++ ) 
	{
	  
	  
	  int s = signals.find( disp.order( ord ) );
	  
	  
	  // skip if we do not find the signal?
	  if ( s == - 1 ) continue;
	  
	  std::vector<double>   * data  = slice->channel[s]->nonconst_pdata();
	  const std::vector<uint64_t> * tp    = slice->channel[s]->ptimepoints();
	  const std::vector<int>      * rec   = slice->channel[s]->precords();
	  
	  //
	  // Get implied time interval
	  //
	  
	  const uint64_t start_tp = (*tp)[0];
	  const uint64_t stop_tp   = (*tp)[ tp->size() - 1 ];
	  const uint64_t interval_tp = stop_tp - start_tp;
    
	  // number of implied datapoints (unless a discontinuous EDF, should equal 'npoints'
	  const double   interval_sec    = interval_tp * globals::tp_duration;
	  const int      Fs              = edf->header.sampling_freq( signals(s) ) ;
	  const uint64_t interval_points = 1 + interval_sec * Fs;      
	  
	  const uint64_t np = data->size();      
	  
	  
	  //
	  // Signal label
	  //
	  
	  std::string label = edf->header.label[ signals(s) ];
	  std::string disp_label = disp.display_label( edf->header.label[ signals(s) ] );
	  
	  
	  //
	  // Get range
	  //
	  
	  double mymin = (*data)[0];
	  double mymax = (*data)[0];
	  
	  for (uint64_t i=0;i<np;i++) 
	    {
	      if      ( (*data)[i] < mymin ) mymin = (*data)[i];
	      else if ( (*data)[i] > mymax ) mymax = (*data)[i];
	    }
      
	  if ( mymax - mymin == 0 ) 
	    {
	      mymin = edf->header.physical_min[s];
	      mymax = edf->header.physical_max[s];
	      if ( mymin > mymax ) 
		{
		  double t = mymin;
		  mymin = mymax; mymax = t;
		}
	    }
	  
	  // scale 0...1
	  for (uint64_t i=0;i<np;i++) 
	    {
	      (*data)[i] = ( (*data)[i] - mymin ) / ( mymax - mymin );
	      // flip, as grid is from top (0) to bottom (1)
	      (*data)[i] = 1 - (*data)[i];
	    }
	  
	  
	  //
	  // Colour
	  //
	  
	  std::cout << "ctx.fillStyle = '" << disp.col( label ) << "';";
	  std::cout << "ctx.strokeStyle = '" << disp.col( label ) << "';";
	  
	  
	  double s2 = s + 1.5; // spacer + staging track at top
	  
	  //
	  // Coords; x .. 0 to np-1              sleft + pxwidth * x 
	  //         y .. 0 to 1                 (s+y) * h 
	  //  where s is signal number ( 0, 1, ...) 
	  
	  //
	  // Label and scales; note: scale min/max flipped, as canvas is 'upside' down, i.e. smaller numbers are higher
	  //
	  
	  
	  std::cout << "ctx.fillText(\"" << disp_label << "\", 5 , " << ( s2+0.5 ) * h << ");";
	  
	  std::stringstream ss;
	  ss << std::fixed;
	  ss << std::setprecision(2);
	  ss << mymin;
	  std::cout << "ctx.fillText(\"" << ss.str()  << "\", " << pxlabel << " , " << (s2+0.75) * h << ");";
	  
	  ss.str(std::string());
	  ss << mymax;      
	  std::cout << "ctx.fillText(\"" << ss.str() << "\", " << pxlabel << " , " << (s2+0.25) * h << ");";
	  
	  //
	  // Actual signal
	  //
	  
	  std::cout << "ctx.beginPath();";      
	  std::cout << "ctx.moveTo(" << smain + (0/(double)np) * pxwidth << " , " << (s2 + (*data)[0] ) * h  << ");";
	  for (int i=1;i<np;i++)
	    std::cout << "ctx.lineTo(" << smain + (i/(double)np) * pxwidth << " , " << ( s2 + (*data)[i] ) * h  << ");";
	  
	  std::cout << "ctx.stroke();";
	  std::cout << "ctx.closePath();";
	 
	  
	}
    

    }
  
  //
  // All done
  //
  
  std::cout << "ctx.strokeStyle = 'purple';"
	    << "ctx.lineWidth = '3';"
	    << "ctx.strokeRect(0, 0, " << param.width << "," << hgt << ");";
  
  
  
  //
  // Finished with main signal window 
  //
  
  std::cout << " } </script> "
	    << "</canvas>";
  
  
        
  //
  // All done, can delete data
  //


  delete slice;
  
}


lw_signals_t::~lw_signals_t()
{
  if ( edf )
    {
      delete edf;
      edf = NULL;
    }
}

