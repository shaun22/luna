
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "lw.h"
#include "web.h"
#include "param.h"

extern lw_param_t  param;


lw_headers_t::lw_headers_t( const std::string & edf_file , const std::string & indiv_id )
{
  
  edf = new edf_t;

  bool okay = edf->attach( edf_file , indiv_id );

  if ( ! okay ) Helper::halt( "could not load EDF" + edf_file );
  else std::cout << "<p><em>Attached : </em> " << edf_file << "</p>";
      
}


lw_headers_t::~lw_headers_t()
{
  if ( edf ) 
    {
      delete edf; 
      edf = NULL; 
    }
}



void lw_headers_t::tabulate()
{
  
  uint64_t duration_tp = globals::tp_1sec * (uint64_t)edf->header.nr * edf->header.record_duration ;

  std::cout << "<table style=\"width:100%;  \">"
	    << "<tr><td width=33%><b>ID</b></td><td>" << edf->id << "</td></tr>"
	    << "<tr><td>EDF header ID</td><td>" << edf->header.patient_id << "</td></tr>"
	    << "<tr><td>Recording info</td><td>" << edf->header.recording_info << "</td></tr>"
	    << "<tr><td>Start date</td><td>" << edf->header.startdate << "</td></tr>"
	    << "<tr><td>Start time</td><td>" << edf->header.starttime << "</td></tr>"
	    << "<tr><td>Number of records</td><td>" << edf->header.nr << "</td></tr>"
	    << "<tr><td>Record duration</td><td>" << edf->header.record_duration << "</td></tr>"
	    << "<tr><td><b>Number of signals</b></td><td>" << edf->header.ns << "</td></tr>"
	    << "<tr><td><b>Total duration</b></td><td>" <<   Helper::timestring( duration_tp ) << "</td></tr>"
	    << "</table>"
    
	    << "<p>&nbsp;</p>"
	    << "<table style=\"width:100%\">"
	    << "<tr><th>#</th><th>Signal</th><th>Sample rate</th><th>Transducer type</th>"
	    << "<th>Phys. dimension</th><th>Phys min/max</th><th>Pre-filtering</th></tr>";

  for (int s=0; s < edf->header.ns; s++)
    {

      std::cout << "<tr>"
		<< "<td>" << s+1 << "\t"
		<< "<td>" << edf->header.label[s] << "</td>" 
		<< "<td>" << edf->header.sampling_freq( s ) << "</td>"
		<< "<td>" << edf->header.transducer_type[s]  << "</td>"
		<< "<td>" << edf->header.phys_dimension[s]  << "</td>"
		<< "<td>" << edf->header.physical_min[s] << "/" << edf->header.physical_max[s] << "</td>"
		<< "<td>" << edf->header.prefiltering[s] << "</td>"
		<< "</tr>";
    }
  
  std::cout << "</table>";


}


