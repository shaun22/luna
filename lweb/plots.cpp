
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#include "lw.h"
#include "web.h"
#include "param.h"
#include "misc.h"
#include "../sstore/sstore.h"
#include "../fftw/fftwrap.h"
#include "../clocs/topo.h"

#include "../dsp/hilbert.h"

extern lw_param_t  param;




void lw_signals_t::activityplots(   const signal_list_t & signals,     const lw_signal_disp_t & disp , const interval_t & interval , mslice_t * slice )
{



  //
  // Get data 
  //
  
  const int ns = signals.size();

  const int ns2 = slice->size();

  if ( ns != ns2 ) Helper::halt( "ns != n2" );

  // epoch-level
  // parallel to signal view, but after running filters to get amplitude 
  // do a stacked area plot per sample point of slow, delta, etc


  double hgt = (ns+2) * param.sighgt;
  double wid = param.width;
  



  double pxleft    = param.width * 0.02;
  double pxlabel   = param.width * 0.05;

  double smain     = param.width * 0.1; // start of main signal window
  double pxwidth   = param.width * 0.88; // width of this window
  
  double h      = param.sighgt;
  

  int cnt = 0;
  
  for (int ord=0; ord <  disp.size(); ord++ ) 
    {
      
      int s = signals.find( disp.order( ord ) );      
      
      // skip if we do not find the signal?
      if ( s == -1 ) continue;
      
            std::vector<double>   * data  = slice->channel[s]->nonconst_pdata();
      const std::vector<uint64_t> * tp    = slice->channel[s]->ptimepoints();
      const std::vector<int>      * rec   = slice->channel[s]->precords();

      // skip in HIlbert mode if sample rate too low
      const int      Fs              = edf->header.sampling_freq( signals(s) ) ;
      
      if ( Fs < 50 ) continue;

      //
      // Get implied time interval
      //
      
      const uint64_t start_tp = (*tp)[0];
      const uint64_t stop_tp   = (*tp)[ tp->size() - 1 ];
      const uint64_t interval_tp = stop_tp - start_tp;
    
      // number of implied datapoints (unless a discontinuous EDF, should equal 'npoints'
      const double   interval_sec    = interval_tp * globals::tp_duration;
      const uint64_t interval_points = 1 + interval_sec * Fs;      
      
      const uint64_t np = data->size();      

      
      //
      // Signal label
      //

      std::string label = edf->header.label[ signals(s) ];
      std::string disp_label = disp.display_label( edf->header.label[ signals(s) ] );


      //
      // Filter-hilbert signal
      //
      
      
      hilbert_t h_delta( *data , Fs , 0.5 , 4 , 0.005 , 0.2 );
      hilbert_t h_theta( *data , Fs , 4 , 8   , 0.005 , 0.2 );
      hilbert_t h_alpha( *data , Fs , 8 , 11  , 0.005 , 0.2 );
      hilbert_t h_sigma( *data , Fs , 11 , 15 , 0.005 , 0.2 );
      hilbert_t h_beta( *data  , Fs , 15 , 20 , 0.005 , 0.2 );

      std::vector<double> m_delta = *(h_delta.magnitude());
      std::vector<double> m_theta = *(h_theta.magnitude());
      std::vector<double> m_alpha = *(h_alpha.magnitude());
      std::vector<double> m_sigma = *(h_sigma.magnitude());
      std::vector<double> m_beta  = *(h_beta.magnitude());
      

      // band means across all time points

      double avg_delta = 0 , avg_theta = 0 , avg_alpha = 0 , avg_sigma = 0 , avg_beta = 0; 
      
      for (int i=0; i<np;i++)
	{
	  avg_delta += m_delta[i] ;
	  avg_theta += m_theta[i] ;
	  avg_alpha += m_alpha[i] ;
	  avg_sigma += m_sigma[i] ;
	  avg_beta  += m_beta[i] ;
	}

      // sum per point

      std::vector<double> summ( np , 0 );
      
      for (int i=0; i<np;i++)
	{
	  m_delta[i] = m_delta[i] /  avg_delta ;
	  m_theta[i] = m_theta[i] / avg_theta ;
	  m_alpha[i] = m_alpha[i] / avg_alpha ;
	  m_sigma[i] = m_sigma[i] / avg_sigma ;
	  m_beta[i]  = m_beta[i] / avg_beta ;
	  
	  summ[i] = m_delta[i] + m_theta[i] + m_alpha[i] + m_sigma[i] + m_beta[i];
	}
      
      //
      // Get range
      //
      
      double mymin = summ[0];
      double mymax = summ[0];

      for (uint64_t i=0;i<np;i++) 
	{
	  if      ( summ[i] < mymin ) mymin = summ[i];
	  else if ( summ[i] > mymax ) mymax = summ[i];
	}
      
      if ( mymax - mymin == 0 ) 
	{
	  mymin = 0;
	  mymax = 1;
	}
      


      //
      // Colour
      //

      std::cout << "ctx.fillStyle = '" << disp.col( label ) << "';";
      std::cout << "ctx.strokeStyle = '" << disp.col( label ) << "';";
      
      
      double s2 = ( cnt++) *h + 1.5; // spacer + staging track at top
      
      double ymid = s2 + h/0.5;
      
      std::cout << "ctx.fillText(\"" << disp_label << "\", 5 , " << ymid << ");";
      
       std::stringstream ss;
       ss << std::fixed;
       ss << std::setprecision(2);
//        ss << mymin;
//        std::cout << "ctx.fillText(\"" << ss.str()  << "\", " << pxlabel << " , " << ymid + 0.25 * h << ");";
      
//       ss.str(std::string());
       ss << mymax;      
       std::cout << "ctx.fillText(\"" << ss.str() << "\", " << pxlabel << " , " << ymid - 0.25 * h << ");";
      


      //
      // Actual signal
      //
                  
      for (int i=1;i<np-1;i++)
	{
	  // normalized total at this point summ[i];
	  
	  const double ytot = h * ( summ[i] / mymax );
	  
	  double ybase = ymid - ytot/2.0;
	  
	  double x = smain + pxwidth * i/(double)np;

	  std::cout << "ctx.fillStyle = '" << param.rbow[99] << "';";	  
	  double yinc = ytot * ( m_beta[i] / summ[i] ) ;
	  std::cout << "ctx.fillRect(" << x << "," << ybase << ",1," << yinc     << ");";

	  ybase += yinc;	  	  
	  std::cout << "ctx.fillStyle = '" << param.rbow[75]  << "';";
	  yinc = ytot * ( m_sigma[i] / summ[i] ) ;
	  std::cout << "ctx.fillRect(" << x << "," << ybase << ",1," << yinc     << ");";

	  ybase += yinc;
	  std::cout << "ctx.fillStyle = '" << param.rbow[50]  << "';";
	  yinc = ytot * ( m_alpha[i] /summ[i] ) ;
	  std::cout << "ctx.fillRect(" << x << "," << ybase << ",1," << yinc     << ");";

	  ybase += yinc;
	  std::cout << "ctx.fillStyle = '" << param.rbow[25]  << "';";
	  yinc = ytot * ( m_theta[i] / summ[i] ) ;
	  std::cout << "ctx.fillRect(" << x << "," << ybase << ",1," << yinc    << ");";
	  
	  ybase += yinc;
	  std::cout << "ctx.fillStyle = '" << param.rbow[0]  << "';";
	  yinc = ytot * ( m_delta[i] / summ[i] ) ;
	  std::cout << "ctx.fillRect(" << x << "," << ybase << ",1," << yinc     << ");";
	  

	  
// 	  std::cout << "ctx.fillStyle = '" << param.lightgreen  << "';";

// 	  std::cout << "ctx.fillStyle = '" << param.yellow  << "';";

// 	  std::cout << "ctx.fillStyle = '" << param.red  << "';";
      
	}
      
      
      

//       std::cout << "ctx.beginPath();";      
//       std::cout << "ctx.moveTo(" << smain + (0/(double)np) * pxwidth << " , " << (s2 + (*m_delta)[0]/mymax ) * h  << ");";
      
// 	std::cout << "ctx.lineTo(" << smain + (i/(double)np) * pxwidth << " , " << ( s2 + (*m_delta)[i]/mymax ) * h  << ");";
      
//       std::cout << "ctx.stroke();";
//       std::cout << "ctx.closePath();";

      // next signal
    }
  
 
  
  
}


bool lw_signals_t::fetch_stage( const std::string & staging_db , std::vector<std::string> * stage30 )
{

  if ( ! Helper::fileExists( staging_db ) ) return false;

  sstore_t ss( staging_db );
  

  int ne30 = stage30->size();

  std::map<int,sstore_data_t> epochs = ss.fetch_epochs();

  if ( epochs.size() != ne30 ) return false;
  
  std::map<int,sstore_data_t>::const_iterator ee = epochs.begin();
  while ( ee != epochs.end() )
    {      
      // epochs stored in db as 1-based
      int e = ee->first - 1; 
      
      const std::map<sstore_key_t,sstore_value_t> & datum   = ee->second.data;
      
      std::map<sstore_key_t,sstore_value_t>::const_iterator kk = datum.find( sstore_key_t( "STAGE" , "" , "" ) ); 
      
      if ( kk == datum.end() ) continue;	 

      if ( e < 0 || e >= ne30 ) Helper::halt( "mismatched staging db" );
      
      // store
      (*stage30)[ e ] = kk->second.str_value ; 

      // next staged epoch
      ++ee;
      
    }
  return true;
}


bool lw_signals_t::fetch_mask( const std::string & mask_db , std::vector<bool> * mask30 )
{
  
  if ( ! Helper::fileExists( mask_db ) ) return false;

  sstore_t ss( mask_db );

  int ne30 = mask30->size();

  std::map<int,sstore_data_t> epochs = ss.fetch_epochs();
  // any entry means it is flagged
  
  std::map<int,sstore_data_t>::const_iterator ee = epochs.begin();
  while ( ee != epochs.end() ) 
    {
      // db is 1-based epoch reporting
      int e = ee->first - 1 ; 
      if ( e >= 0 && e < ne30 ) 
	(*mask30)[ e ] = true;
      else
	{
	  std::cout << "<p><em>warning: unexpected number of epochs in mask file</p>";
	  return false;
	}
      ++ee;
    }
  return true;
}

void lw_signals_t::stage_plot( const std::string & staging_db , 
			       const int ne , 
			       const int ne30 , 
			       const int current_epoch , 
			       std::vector<std::string> * stages30 , 
			       std::vector<bool> * wake30 , 
			       const std::vector<bool> * mask30 , 
			       const std::map<int,int> * cycles ) 
{


  sstore_t ss( staging_db );
  
  std::map<int,sstore_data_t> epochs = ss.fetch_epochs();
  
  // standard height
  double height = 50;
  
  // adding NREM cycles?
  if ( cycles != NULL ) height += 30;
  
  if ( epochs.size() != ne30 ) 
    std::cout << "<p><em>warning... incorrect number of staged epochs found in staging.db</em></p>";
  
  // start of staging canvas 
  
  std::cout << "<canvas id=\"stgCanvas\" height=\"" << height << "\" width=\"" << param.width << "\">"
	    << "<script>"
	    << "var canvas1 = document.getElementById('stgCanvas');"
	    << "if (canvas1.getContext) { "
	    << "var ctx1 = canvas1.getContext('2d');";
  
  std::cout << "ctx1.strokeStyle = 'orange';"
	    << "ctx1.lineWidth = '3';"
	    << "ctx1.strokeRect(0, 0, " << param.width << "," << height << ");";
  
  std::cout << "ctx1.lineWidth = '1';";
  
  
  // draw function
  
  //std::cout << " function point(x, y, canvas) {canvas.beginPath(); canvas.moveTo(x, y); canvas.lineTo(x+1, y+1); canvas.stroke(); } ";
  
  
  for (int e=0;e<ne30;e++)
    {
      
      // look up is 1-based
      std::map<int,sstore_data_t>::const_iterator ee = epochs.find( e + 1 );
      
      if ( ee == epochs.end() ) continue;
      
      const std::map<sstore_key_t,sstore_value_t> & datum   = ee->second.data;
      
      std::map<sstore_key_t,sstore_value_t>::const_iterator kk = datum.find( sstore_key_t( "STAGE" , "" , "" ) ); 
      
      if ( kk == datum.end() ) continue;	 
      
      std::string scol = "gray";
      double sh = 0; // from 
      //   W  45  , N1 25  N2 35  N3 45  REM 15   ? 5
      
      if ( kk->second.str_value == "Wake" ) 
	{
	  sh = 5;
	  scol = "silver";
	}
      else if ( kk->second.str_value == "NREM1" ) 
	{
	  sh = 20;	     
	  scol = "powderblue";
	}
      else if ( kk->second.str_value == "NREM2" ) 
	{
	  sh = 25;	     	     
	  scol = "dodgerblue";
	}
      else if ( kk->second.str_value == "NREM3" || kk->second.str_value == "NREM4" ) 
	{
	  sh = 30;	     	     
	  scol = "navy";
	}
      else if ( kk->second.str_value == "REM" ) 
	{
	  sh = 15;	     	     
	  scol = "darkorange";
	}
      else // missing/unscored
	{
	  sh = 2;
	  scol = "orchid";
	}
      
      // store col as 30-second epoch for entire record
      if ( stages30 )
	stages30->push_back( scol );
      if ( wake30 ) 
	wake30->push_back( kk->second.str_value == "Wake" );
      
      double x  = param.width * ( e / (double)ne30 );
      double x2 = param.width * ( (e+1) / (double)ne30 ) - x + 1 ; 
      
      std::cout << "ctx1.fillStyle = '" << scol << "';"
		<< "ctx1.fillRect(" << x << "," << sh << "," << x2 << ",8);";
      

      // masked? (0-based lookup here)
      
      if ( mask30 && (*mask30)[e] )
	std::cout << "ctx1.fillStyle = 'black';"
		  << "ctx1.fillRect(" << x << "," << 46 << ",1,3);";
      
      
    }


  //
  // Cycles?
  //

  if ( cycles ) 
    {
      std::map<int,int>::const_iterator cc = cycles->begin();
      int cnt = 1;
      while ( cc != cycles->end() )
	{
	  int epoch1 = cc->first - 1;  // make 0-base
	  int epoch2 = cc->second -1; 

	  int x1 = param.width * ( epoch1 / (double)ne30 );
	  int x2 = param.width * ( epoch2 / (double)ne30 ) - x1 + 1 ;

	  int y = cnt % 2 == 0 ? 55 : 65 ;
	  
	  std::cout << "ctx1.strokeStyle = '" << "brown" << "';"
		    << "ctx1.strokeRect(" << x1 << "," << y << "," << x2 << ",10);";
	  ++cnt;
	  ++cc;
	}
    }
  
  
  //
  // Indicate current epoch position
  //
  
  if ( current_epoch != -1 ) 
    {
      int x = param.width * (   current_epoch   / (double)ne );
      int w = param.width * (  (current_epoch+1) / (double)ne ) - x;
      if ( w <= 5 ) w = 5;
      
      std::cout << "ctx1.fillStyle = 'red';"
		<< "ctx1.fillRect(" << x-w/2.0 << "," << 0 << "," << w << ",8);";
    }


} 



void lw_signals_t::hjorth_plot( const std::string & hjorth_db , 
				const int ne30 , 
				const std::vector<bool> * wake30 , 
				const std::vector<bool> * mask30 ) 
{
  
  bool has_staging = wake30 != NULL;
  
  bool has_mask = mask30 != NULL;
  
  sstore_t ss( hjorth_db );
  
  std::map<int,sstore_data_t> epochs = ss.fetch_epochs();
  
  if ( epochs.size() != ne30 ) 
    std::cout << "<p><em>warning... incorrect number of staged epochs found in hjorth.db</em></p>";
  
  std::vector<double> h1(ne30), h2(ne30), h3(ne30);
  double h1_max = 0 , h2_max = 0 , h3_max = 0;
  double h1_min = 999 , h2_min = 999 , h3_min = 999;
  
  // Assume only a single channel
  
  for (int e=0;e<ne30;e++)
    {
      
      // look up is 1-based
      std::map<int,sstore_data_t>::const_iterator ee = epochs.find( e + 1 );
      
      if ( ee == epochs.end() ) continue;
      
      if ( has_staging && (*wake30)[e] ) continue;
      if ( has_mask    && (*mask30)[e] ) continue;
      
      const std::map<sstore_key_t,sstore_value_t> & datum   = ee->second.data;
      
      std::map<sstore_key_t,sstore_value_t>::const_iterator kk = datum.begin();
      while ( kk != datum.end() )
	{	  
	  if      ( kk->first.id == "H1" )
	    h1[e] = log(kk->second.dbl_value);
	  else if ( kk->first.id == "H2" )
	    h2[e] = kk->second.dbl_value;
	  else if ( kk->first.id == "H3" )
	    h3[e] = kk->second.dbl_value;
	  
	  ++kk;
	}
      
    }
      
  
  // normalize
  for (int e=0;e<ne30;e++)
    {
      if ( has_staging && (*wake30)[e] ) continue;
      if ( has_mask    && (*mask30)[e] ) continue;
      
      if ( h1[e] > h1_max ) h1_max = h1[e];
      if ( h2[e] > h2_max ) h2_max = h2[e];
      if ( h3[e] > h3_max ) h3_max = h3[e];
      
      if ( h1[e] < h1_min ) h1_min = h1[e];
      if ( h2[e] < h2_min ) h2_min = h2[e];
      if ( h3[e] < h3_min ) h3_min = h3[e];
    }
  
  for (int e=0;e<ne30;e++)
    {
      if ( has_staging && (*wake30)[e] ) continue;
      if ( has_mask    && (*mask30)[e] ) continue;
      h1[e] = ( h1[e] - h1_min ) / ( h1_max - h1_min );
      h2[e] = ( h2[e] - h2_min ) / ( h2_max - h2_min );
      h3[e] = ( h3[e] - h3_min ) / ( h3_max - h3_min );
    }
  
  // start of staging canvas 
      
  std::cout << "<canvas id=\"hjorthCanvas\" height=\"" << 50 << "\" width=\"" << param.width << "\">"
	    << "<script>"
	    << "var canvas1 = document.getElementById('hjorthCanvas');"
	    << "if (canvas1.getContext) { "
	    << "var ctx1 = canvas1.getContext('2d');";
  
  std::cout << "ctx1.strokeStyle = 'orange';"
	    << "ctx1.lineWidth = '3';"
	    << "ctx1.strokeRect(0, 0, " << param.width << "," << 50 << ");";
  
  std::cout << "ctx1.lineWidth = '1';";
  
  const double hgt1 = param.sighgt / 3.0;
  const double hgt2 = hgt1 * 2;
  
  for (int e=0;e<ne30;e++)
    {
      
      if ( has_staging && (*wake30)[e] ) continue;
      if ( has_mask    && (*mask30)[e] ) continue;
      
      int x = param.width * ( e / (double)ne30 );
      
      int y1 = h1[e] * 100;
      if ( y1 == 100 ) y1 = 99;
      
      int y2 = h2[e] * 100;
      if ( y2 == 100 ) y2 = 99;
      
      int y3 = h3[e] * 100;
      if ( y3 == 100 ) y3 = 99;
      
      //	  std::cout << "<p>e " << e << " " << h1[e] << " " << y1 << " " << y2 << " " << y3 << "</p>";
      
      std::cout << "ctx1.fillStyle = '" << param.rbow[y1] << "';"
		<< "ctx1.fillRect(" << x << "," << 0 << ",1," << hgt1 << ");";
      
      std::cout << "ctx1.fillStyle = '" << param.rbow[y2] << "';"
		<< "ctx1.fillRect(" << x << "," << hgt1 << ",1," << hgt2  << ");";
      
      std::cout << "ctx1.fillStyle = '" << param.rbow[y3] << "';"
		<< "ctx1.fillRect(" << x << "," << hgt2 << ",1," << param.sighgt << ");";
      
      // 	  std::cout << "ctx1.fillRect(" << x << "," << 0 << ",1," << param.sighgt << ");";
      
    }
    
}





