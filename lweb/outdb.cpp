
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "lw.h"
#include "param.h"
#include "web.h"

#include "../helper/helper.h"
#include "../sstore/sstore.h"

#include <string>
#include <fstream>

extern lw_param_t param;

lw_outdbs_t::lw_outdbs_t( const std::string & f )
{
  std::cout << "<p><em>Individual :</em> " << param.indiv << "</p>";

  // Expecting tab-delimited file: name, description
  std::string fname = f + "/outputs" ;
  if ( ! Helper::fileExists( fname ) ) Helper::halt( "could not find DB filelist [ " + fname + " ] found" );

  std::cout << "<table style=\"width:100%\">";
  std::ifstream IN1( fname.c_str() , std::ios::in );
  while ( !IN1.eof() ) 
    {
      std::string line;
      std::getline( IN1 , line , '\n' );
      if ( IN1.eof() ) break;
      std::vector<std::string> tok = Helper::parse( line , "\t" );
      if ( tok.size() == 0 ) continue;
      if ( tok.size() != 2 ) Helper::halt( "bad format in outputs file: " + line );

      // does this output ss file exist?
      std::string outdb = param.folder + "/" + param.indiv + "/ss/" + tok[0] + ".db";
      
      if ( Helper::fileExists( outdb ) )
	std::cout << "<tr><td>" << web_t::outputs_link( tok[0] , param.folder , param.indiv, tok[0]  )
		  << "</td><td>" << tok[1] << "</td></tr>";
      else
	std::cout << "<tr><td>" << tok[0] << "</td><td>" << tok[1] << "</td></tr>";
    }
  std::cout << "</table>";
  IN1.close();
     
  
};



lw_outdb_t::lw_outdb_t( const std::string & filename )  { 
  

  //
  // param.outdb_dump= n,a,e,i for no,  baseline, epoch, intervals
  //
    
  std::cout << "<p><em>Individual :</em> " << web_t::outputs_link( param.indiv , 
								   param.folder , 
								   param.indiv ) << "</p>";

  std::cout << "<p><em>Datastore :</em> " << web_t::outputs_link( param.outdb , param.folder , param.indiv , param.outdb ) << "</p>";
  
  if ( param.outdb_dump != "n" ) { dump( filename ); return; }
  
  //
  // Show summaries if not dumping
  //

  sstore_t ss( filename );

  std::map<sstore_key_t,int> keys = ss.keys();
  std::map<sstore_key_t,int> ekeys = ss.keys_epoch();
  std::map<sstore_key_t,int> ikeys = ss.keys_interval();

  std::cout << "<hr>";
  
  //
  // Baseline-level info
  //

  if ( keys.size() > 0 ) 
    {
      std::cout << "<p><em>Baseline-level data</em></p>"; 

      std::cout << web_t::outputs_link( "(dump)" , param.folder , param.indiv, param.outdb , "a" );

      std::set<std::string> vars, chs, lvls;
      
      std::map<sstore_key_t,int>::const_iterator kk = keys.begin();
      while ( kk != keys.end() )
	{
	  const sstore_key_t & key = kk->first;
	  vars.insert( key.id ); chs.insert( key.ch ); lvls.insert( key.lvl );
	  ++kk;
	}
      
      std::cout << "<table style=\"width:100%\">"
		<< "<tr><th>Domain</th><th>N</th><th>Items</th></tr>";      
      std::cout << "<tr><th>Variables</th><td>" << vars.size() << "</td><td>";
      std::set<std::string>::const_iterator ii = vars.begin();
      while ( ii != vars.end() ) 
	{ std::cout << " " << web_t::outputs_link( *ii , param.folder , param.indiv, param.outdb , "a" , *ii ); ++ii; }
      std::cout << "</td></tr>";
      std::cout << "<tr><th>Levels</th><td>" << lvls.size() << "</td><td>";
      if ( lvls.size() == 0 ) std::cout << ".";
      ii = lvls.begin();
      while ( ii != lvls.end() ) { std::cout << " " << *ii ; ++ii; }
      std::cout << "</td></tr>";
      std::cout << "<tr><th>Channels</th><td>" << chs.size() << "</td><td>";
      if ( chs.size() == 0 ) std::cout << ".";
      ii = chs.begin();
      while ( ii != chs.end() ) { std::cout << " " << *ii ; ++ii; }
      std::cout << "</td></tr>";
      std::cout << "</table>";
    }



  //
  // Epoch-level info
  //

  if ( ekeys.size() > 0 ) 
    {
      std::cout << "<p><em>Epoch-level data</em></p>"; 
      
      std::cout << web_t::outputs_link( "(dump)" , param.folder , param.indiv, param.outdb , "e" );

      std::set<std::string> vars, chs, lvls;
      
      std::map<sstore_key_t,int>::const_iterator kk = ekeys.begin();
      while ( kk != ekeys.end() )
	{
	  const sstore_key_t & key = kk->first;
	  vars.insert( key.id ); chs.insert( key.ch ); lvls.insert( key.lvl );
	  ++kk;
	}
      
      std::cout << "<table style=\"width:100%\">"
		<< "<tr><th>Domain</th><th>N</th><th>Items</th></tr>";      
      std::cout << "<tr><th>Variables</th><td>" << vars.size() << "</td><td>";
      std::set<std::string>::const_iterator ii = vars.begin();
      while ( ii != vars.end() ) 
	{ std::cout << " " << web_t::outputs_link( *ii , param.folder , param.indiv, param.outdb , "e" , *ii ); ++ii; }
      std::cout << "</td></tr>";
      std::cout << "<tr><th>Levels</th><td>" << lvls.size() << "</td><td>";
      if ( lvls.size() == 0 ) std::cout << ".";
      ii = lvls.begin();
      while ( ii != lvls.end() ) { std::cout << " " << *ii ; ++ii; }
      std::cout << "</td></tr>";
      std::cout << "<tr><th>Channels</th><td>" << chs.size() << "</td><td>";
      if ( chs.size() == 0 ) std::cout << ".";
      ii = chs.begin();
      while ( ii != chs.end() ) { std::cout << " " << *ii ; ++ii; }
      std::cout << "</td></tr>";
      std::cout << "</table>";
    }




  //
  // Interval-level info
  //

  if ( ikeys.size() > 0 ) 
    {
      std::cout << "<p><em>Interval-level data</em></p>"; 

      std::cout << web_t::outputs_link( "(dump)" , param.folder , param.indiv, param.outdb , "i" );
      
      std::set<std::string> vars, chs, lvls;
      
      std::map<sstore_key_t,int>::const_iterator kk = ikeys.begin();
      while ( kk != ikeys.end() )
	{
	  const sstore_key_t & key = kk->first;
	  vars.insert( key.id ); chs.insert( key.ch ); lvls.insert( key.lvl );
	  ++kk;
	}
      
      std::cout << "<table style=\"width:100%\">"
		<< "<tr><th>Domain</th><th>N</th><th>Items</th></tr>";      
      std::cout << "<tr><th>Variables</th><td>" << vars.size() << "</td><td>";
      std::set<std::string>::const_iterator ii = vars.begin();
      while ( ii != vars.end() )
	{ std::cout << " " << web_t::outputs_link( *ii , param.folder , param.indiv, param.outdb , "i" , *ii ); ++ii; }
      std::cout << "</td></tr>";
      std::cout << "<tr><th>Levels</th><td>" << lvls.size() << "</td><td>";
      if ( lvls.size() == 0 ) std::cout << ".";
      ii = lvls.begin();
      while ( ii != lvls.end() ) { std::cout << " " << *ii ; ++ii; }
      std::cout << "</td></tr>";
      std::cout << "<tr><th>Channels</th><td>" << chs.size() << "</td><td>";
      if ( chs.size() == 0 ) std::cout << ".";
      ii = chs.begin();
      while ( ii != chs.end() ) { std::cout << " " << *ii ; ++ii; }
      std::cout << "</td></tr>";
      std::cout << "</table>";
    }
  
}



void lw_outdb_t::dump( const std::string & filename ) {

  if ( ! Helper::fileExists( filename ) ) Helper::halt( "could not find " + filename );
  sstore_t ss( filename );

  // focus on only one variable? 
  bool onevar = param.outdb_var != ".";


  //
  // Baseline
  //

  if ( param.outdb_dump == "a" ) 
    {
      
      sstore_data_t data = ss.fetch_base(); 
      
      std::cout << "<table style=\"width:100%\">";
      
      std::cout << "<tr><th>Variable</th><th>Channel</th><th>Level</th><th>Value</th></tr>";
      
      std::map<sstore_key_t,sstore_value_t>::const_iterator kk = data.data.begin();
      while ( kk != data.data.end() )
	{
	  // skip?
	  if ( onevar && param.outdb_var != kk->first.id ) { ++kk; continue; } 
	  
	  std::cout << "<tr><td>" << kk->first.id << "</td>"
		    << "<td>" <<  kk->first.ch << "</td>"
		    << "<td>" <<  kk->first.lvl << "</td>";
	  std::cout << "<td>";
	  
	  if      ( kk->second.is_text ) std::cout << kk->second.str_value ;
	  else if ( kk->second.is_double ) std::cout << kk->second.dbl_value ;
	  else {
	    const int nn = kk->second.vec_value.size() ;
	    for (int i=0;i<nn;i++) std::cout << " " << kk->second.vec_value[i] ;
	  }
	  
	  std::cout << "</td></tr>";
	  ++kk;
	  
	}
      
      std::cout << "</table>";

    }

  //
  // Epoch-level
  //
  
  else if ( param.outdb_dump == "e" ) 
    {
      
      std::map<int,sstore_data_t> epochs = ss.fetch_epochs();
       
      std::cout << "<p>" <<  epochs.size() << "<em> epochs</em></p>";

      std::cout << "<table style=\"width:100%\">";

      std::cout << "<tr><th>Variable</th><th>Epoch</th><th>Channel</th><th>Level</th><th>Value</th></tr>";
      

      std::map<int,sstore_data_t>::const_iterator ii = epochs.begin();

      while ( ii != epochs.end() )
	{
	  	                             
	  const std::map<sstore_key_t,sstore_value_t> & datum   = ii->second.data;
          
	  std::map<sstore_key_t,sstore_value_t>::const_iterator kk = datum.begin();
	  while ( kk != datum.end() )
	    {
	      
	      // skip?
	      if ( onevar && param.outdb_var != kk->first.id ) { ++kk; continue; } 

	      std::cout << "<tr><td>" << kk->first.id << "</td>"
			<< "<td>" << ii->first << "</td>"
			<< "<td>" <<  kk->first.ch << "</td>"
			<< "<td>" <<  kk->first.lvl << "</td>";
	      std::cout << "<td>";

	      if      ( kk->second.is_text ) std::cout << kk->second.str_value ;
	      else if ( kk->second.is_double ) std::cout << kk->second.dbl_value ;
	      else {
		const int nn = kk->second.vec_value.size() ;
		for (int i=0;i<nn;i++) std::cout << " " << kk->second.vec_value[i] ;
	      }

	      std::cout << "</td></tr>";
	      ++kk;

	    }

	  // next epoch
	  ++ii;
	}
      
      std::cout << "</table>";



    }


  //
  // Interval-level
  //
  
  else if ( param.outdb_dump == "i" ) 
    {
      
      clocktime_t zero_start;
      zero_start.reset();

      std::map<interval_t,sstore_data_t> intervals = ss.fetch_intervals(); 

      std::cout << "<p>" <<  intervals.size() << "<em> intervals</em></p>";

      std::cout << "<table style=\"width:100%\">";

      std::cout << "<tr><th>Interval</th><th>Elapsed time</th><th>Variable</th><th>Channel</th><th>Level</th><th>Value</th></tr>";
      
      std::map<interval_t,sstore_data_t>::const_iterator ii = intervals.begin();
      
      int icnt = 0;
      
      while ( ii != intervals.end() )
	{

	  ++icnt;

	  const std::map<sstore_key_t,sstore_value_t> & datum   = ii->second.data;
           
	  std::map<sstore_key_t,sstore_value_t>::const_iterator kk = datum.begin();
	  while ( kk != datum.end() )
	    {
	      
	      // skip?
	      if ( onevar && param.outdb_var != kk->first.id ) { ++kk; continue; } 

	      std::cout << "<tr><td>" << icnt << "</td>";
	      
	      std::string t1, t2;
	      Helper::hhmmss( zero_start , ii->first , &t1 , &t2 );
	      
	      std::cout << "<td>" << t1 << "</td>";
	      //std::cout << "<td>" << t1 << " .. " << t2 << "</td>";
	      
	      std::cout 		<< "<td>" << kk->first.id << "</td>"			
					<< "<td>" <<  kk->first.ch << "</td>"
					<< "<td>" <<  kk->first.lvl << "</td>";
	      std::cout << "<td>";

	      if      ( kk->second.is_text ) std::cout << kk->second.str_value ;
	      else if ( kk->second.is_double ) std::cout << kk->second.dbl_value ;
	      else {
		const int nn = kk->second.vec_value.size() ;
		for (int i=0;i<nn;i++) std::cout << " " << kk->second.vec_value[i] ;
	      }

	      std::cout << "</td></tr>";
	      ++kk;

	    }

	  // next epoch
	  ++ii;
	}
      
      std::cout << "</table>";



      
    }
  
}





