
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "lw.h"
#include "web.h"
#include "param.h"

#include <string>
#include <map>
#include <set>
#include <vector>
#include "../helper/helper.h"

// for 'ls'
#include "dirent.h"

extern lw_param_t  param;

void lw_exe_t::display_exe_list()
{

  std::cout << "<p><em>Epoch-by-epoch clustering</em></p><hr>";
  
  std::string folder = param.folder + "/" + param.indiv + "/exe" ; 
  
  std::map<std::string,std::set<std::string> > channels;
  
  DIR *dir;
  struct dirent *ent;
  if ( (dir = opendir ( folder.c_str() ) ) != NULL) 
    {
      /* print all the files and directories within directory */
      while ((ent = readdir (dir)) != NULL) 
	{
	  
	  std::string filename = ent->d_name;
	  if ( filename == "." || filename == ".." ) continue;
	  
	  // expect format as follows: 
	  // epochs10-{signalname}
	  // epochs00-
	  if ( filename.size() < 10 )  continue;

	  std::string sig = filename.substr(9);
	  
	  if ( filename.substr( 0 , 9 ) == "epochs10-" ) 
	    {
	      channels[sig].insert( "K10" );
	    }
	  else if ( filename.substr( 0 , 9 ) == "epochs00-" ) 
	    {
	      channels[sig].insert( "ALL" );
	    }
	  
	}
      closedir (dir);
    } 
  else 
    {
      return;
    }


  std::cout << "<table style=\"width:100%;  \">"
	    << "<tr><th width=60%>Channel</th><th width=20%>Default</th><th width=20%>K10</th></tr>";
  
  std::map<std::string,std::set<std::string> >::const_iterator ii = channels.begin();
  while ( ii != channels.end() )
    {
      std::cout << "<tr><td>" << ii->first << "</td>";

      if ( ii->second.find( "ALL" ) != ii->second.end() ) std::cout << "<td>" << web_t::exe_link( "view" , param.folder , param.edf_file , param.indiv , ii->first , "ALL" ) << "</td>";
      else std::cout << "<td></td>";

      if ( ii->second.find( "K10" ) != ii->second.end() ) std::cout << "<td>" << web_t::exe_link( "view" , param.folder , param.edf_file , param.indiv , ii->first , "K10" ) << "</td>";
      else std::cout << "<td></td>";

      std::cout << "</tr>";
      ++ii;
    }

  std::cout << "</table>";
  
}


lw_exe_t::lw_exe_t()
{

  edf = NULL;

  // nothing to do
  if ( param.exe_signal == "." ) return;
  if ( param.exe_type == "." ) return;

  std::string filename = param.folder + + "/" + param.indiv + "/exe/";
  if      ( param.exe_type == "K10" ) filename += "epochs10-" + param.exe_signal ;
  else if ( param.exe_type == "ALL" ) filename += "epochs00-" + param.exe_signal ;
  else return;  // unknown plot type
  
  // check file exists
  
  if ( ! Helper::fileExists( filename ) ) return;

  exe.clear();
  
  std::ifstream IN1( filename.c_str() , std::ios::in );
  while ( ! IN1.eof() ) 
    {
      std::string line;
      std::getline( IN1 , line , '\n' );
      if ( IN1.eof() || line == "" ) continue;
      std::vector<std::string> tok = Helper::parse( line );  // likely space delim, but tab okay too
      int exemplar = 0;
       if ( ! Helper::str2int( tok[0] , &exemplar ) ) Helper::halt( "bad format for ExE files" );
       for (int i=1;i<tok.size();i++)
 	{
 	  int member = 0;
 	  if ( ! Helper::str2int( tok[i] , &member) ) Helper::halt( "bad format for ExE files" );	  
 	  // record
 	  exe[ exemplar ].insert( member );
 	}
    }
  IN1.close();

  
  std::cout << "<p>Signal <b>" << param.exe_signal << "</b> has <b>" << exe.size() << "</b> clusters, based on ";
  if ( param.exe_type == "K10" ) std::cout << "K10 clustering</p>";
  if ( param.exe_type == "ALL" ) std::cout << "default clustering</p>";


  //
  // Attach EDF
  //

  edf = new edf_t;
  
  bool okay = edf->attach( param.edf_file , param.indiv );
  
  if ( ! okay ) Helper::halt( "could not load EDF" + param.edf_file );
  
  
  
  //
  // Now, we EITHER plot all exemplars, 
  //         OR     for one cluster, plot exemplar and all epochs
  //
  
  int cluster = 0;
  if ( param.exe_cluster != "." && Helper::str2int( param.exe_cluster , &cluster ) )
    display( cluster );
  else 
    display();

}




void lw_exe_t::display()
{

  //
  // For each cluster, plot 1 global plot and 1 single plot (for the exemplar)
  //

  // masking and staging?
  int ne30 = edf->timeline.set_epoch( param.esize , param.esize  );
  std::vector<std::string> stages30( ne30 , "." );
  bool has_stages = lw_signals_t::fetch_stage( param.folder + "/" + param.indiv + "/ss/staging.db" , &stages30 ); 
  std::vector<bool> masks30( ne30 , false );
  bool has_mask = lw_signals_t::fetch_mask( param.folder + "/" + param.indiv + "/ss/mask.db" , &masks30 );
    
  int cn = 0;
  std::map<int,std::set<int> >::const_iterator ii = exe.begin();
  while ( ii != exe.end() )
    {
      std::cout << "<br>" 
		<< web_t::exe_link( "Cluster " + Helper::int2str(++cn) , 
				    param.folder , param.edf_file , param.indiv , 
				    param.exe_signal , param.exe_type , Helper::int2str( ii->first ) )
		<< " (<em>" << 1 + ii->second.size() << " epochs</em>)<br>";

      // plot all members
      global_epoch_plot( ii->second , ii->first , ne30 , has_stages ? &stages30 : NULL , has_mask ? &masks30 : NULL );
      // plot exemplar signal
      single_epoch_plot( ii->first );
      ++ii;
    }
  
}


void lw_exe_t::display( const int exemplar )
{
  
  std::cout << "<p>" 
	    << web_t::exe_link( "Back to cluster list" ,  param.folder , param.edf_file , param.indiv , param.exe_signal , param.exe_type ) 
	    << "</p>";

  if ( exe.find( exemplar ) == exe.end() ) 
    Helper::halt( "could not find this exemplar, internal error" );
  
  std::set<int> members = exe.find( exemplar )->second ;
  
  //
  // For one cluster, plot 1 global plot and single-plots for the exemplar and all members
  //

  // masking and staging?
  int ne30 = edf->timeline.set_epoch( param.esize , param.esize  );
  std::vector<std::string> stages30( ne30 , "." );
  bool has_stages = lw_signals_t::fetch_stage( param.folder + "/" + param.indiv + "/ss/staging.db" , &stages30 ); 
  std::vector<bool> masks30( ne30 , false );
  bool has_mask = lw_signals_t::fetch_mask( param.folder + "/" + param.indiv + "/ss/mask.db" , &masks30 );
  
  std::cout << "<p>Exemplar epoch " << exemplar << " (<em>" << 1 + members.size() << " epochs</em>)</p>";

  // global plot ( members + exemplar )
  std::set<int> members2 = members;
  members2.insert( exemplar );
  global_epoch_plot( members2 , exemplar , ne30 , has_stages ? &stages30 : NULL , has_mask ? &masks30 : NULL );

  // exemplar
  single_epoch_plot( exemplar );
 
  // all members
  std::set<int>::const_iterator ii = members.begin();
  while ( ii != members.end() )
    {
      // plot exemplar signal
      single_epoch_plot( *ii );
      ++ii;
    }

}


void lw_exe_t::global_epoch_plot(const std::set<int> & epochs , 
				 const int exemplar , 
				 const int ne30 , 
				 const std::vector<std::string> * stages , 
				 const std::vector<bool> * masks )
{
  
  double hgt = 25;
  double wid = param.width;
  
  double smain     = param.width * 0.1; // start of main signal window
  double pxwidth   = param.width * 0.88; // width of this window

  std::cout << "<canvas id=\"g" << exemplar << "Canvas\" height=\"" << hgt << "\" width=\"" << wid << "\">"
	    << "<script>"
	    << "var canvas = document.getElementById('g" << exemplar << "Canvas');"
	    << "if (canvas.getContext) { "
	    << "var ctx = canvas.getContext('2d');";

  // plot
  std::cout << "ctx.strokeStyle = 'orange';"
	    << "ctx.lineWidth = '3';"
	    << "ctx.strokeRect(0, 0, " << wid << "," << hgt << ");";

  
  // epochs
  std::cout << "ctx.lineWidth = '1';";
  
  std::set<int>::const_iterator ee = epochs.begin();
  while ( ee != epochs.end() )
    {

      int epoch = *ee - 1; 

      std::string col = "black";

      // fill stage if we have it, likewise w/ mask

      if ( stages != NULL ) col = param.stagecol[ (*stages)[ epoch ] ] ;
      if ( masks != NULL && (*masks)[epoch] ) col = "green";

      std::cout << "ctx.fillStyle = '" << col << "';";
      
      double x = epoch / (double)ne30;
      
      std::cout << "ctx.fillRect(" << smain + x * pxwidth << "," << 0 << ",1," << hgt << ");";
	
      ++ee;
    }
  
  // done w/ canvas
  std::cout << " } </script> "
	    << "</canvas>";

  
}

void lw_exe_t::single_epoch_plot(const int epoch )
{
  double hgt = param.sighgt;
  double wid = param.width;
  double smain     = param.width * 0.1; // start of main signal window
  double pxwidth   = param.width * 0.88; // width of this window
  
  // get intevnal (nb. is 0-based internally)
  interval_t interval = edf->timeline.epoch( epoch - 1 );

  // fetch signal  
  int s = edf->header.signal( param.exe_signal );
  
  // skip if we do not find the signal?
  if ( s == - 1 ) return;

  slice_t * slice = new slice_t( *edf , s , interval );
  
  std::vector<double> data  = *slice->nonconst_pdata();
  
  const int np = data.size();
  
  if ( np == 0 ) return;

  // scale
  double ymin = data[0];
  double ymax = data[0];
  for (int i=1;i<np;i++)
    {
      if ( data[i] < ymin ) ymin = data[i];
      if ( data[i] > ymax ) ymax = data[i];
    }
  
  for (int i=0;i<np;i++) data[i] = ( data[i] - ymin ) / ( ymax - ymin );
      
  // start canvas
  std::cout << "<canvas id=\"e" << epoch << "Canvas\" height=\"" << hgt << "\" width=\"" << wid << "\">"
	    << "<script>"
	    << "var canvas = document.getElementById('e" << epoch << "Canvas');"
	    << "if (canvas.getContext) { "
	    << "var ctx = canvas.getContext('2d');";
  
  // border
  std::cout << "ctx.strokeStyle = 'purple';"
	    << "ctx.lineWidth = '3';"
	    << "ctx.strokeRect(0, 0, " << wid << "," << hgt << ");";

  // actual signal

  std::cout << "ctx.strokeStyle = 'black';"
	    << "ctx.lineWidth = '1';";
		  
  std::cout << "ctx.beginPath();";      
  std::cout << "ctx.moveTo(" << smain + (0/(double)np) * pxwidth << " , " <<  data[0] * hgt  << ");";
  for (int i=1;i<np;i++)
    std::cout << "ctx.lineTo(" << smain + (i/(double)np) * pxwidth << " , " << data[i] * hgt  << ");";
  
  std::cout << "ctx.stroke();";
  std::cout << "ctx.closePath();";
  
  
  // done w/ canvas
  std::cout << " } </script> "
	    << "</canvas>";
    
}


lw_exe_t::~lw_exe_t()
{ 
  if ( edf ) 
    { 
      delete edf; 
      edf = NULL; 
    }
}
