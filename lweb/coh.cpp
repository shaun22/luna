
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "lw.h"
#include "web.h"
#include "param.h"
#include "topoplots.h"
#include "misc.h"

#include <string>
#include <map>
#include <set>
#include <vector>
#include "../helper/helper.h"
#include "../sstore/sstore.h"
#include "../edf/edf.h"

extern lw_param_t  param;

lw_coh_t::lw_coh_t()
{


  
  std::string filename = param.folder + "/" + param.indiv + "/ss/coh.db" ; 
  
  if ( ! Helper::fileExists( filename ) ) 
    Helper::halt( "no coh.db specified for " + param.indiv );



  //
  // Header
  //

  std::cout << "<p>Coherence view</p>";


  edf_t * edf = new edf_t;
  
  bool okay = edf->attach( param.edf_file , param.indiv );
  
  if ( ! okay ) Helper::halt( "could not load EDF" + param.edf_file );
  else std::cout << "<p><em>Attached :</em> " << param.edf_file << "</p>";   
  
  lw_signal_disp_t disp( param.folder , edf );

  disp.topo.max_radius( 0.55 );
      
  disp.topo.grid( 67 , 67 );

  //
  // Signals control header
  //
  
  std::cout << "<hr><small>";

  signal_list_t signals;

  // if param.signals is empty, this means to look at all signals
  
  if ( param.signals == "." ) 
    { 
      param.signal_set.clear();
      for (int o=0; o < disp.size(); o++ ) 
	{
	  signals.add( o , disp.order(o) );
	  param.signal_set.insert( disp.order(o) );	
	}
    }
  else
    {
      
      //
      // create a signal-string, in the correct order
      //
      
      std::string ordered_signal_string = "";
      bool firstdone = false;
      for (int o=0; o < disp.size(); o++ ) 
	{
	  if ( param.signal_set.find( disp.order(o) ) == param.signal_set.end() ) { continue; }
	  if ( firstdone ) ordered_signal_string += ",";
	  ordered_signal_string += "," + disp.order( o );       
	  firstdone = true;
	}
      
      signals = edf->header.signal_list( ordered_signal_string );
      
    }


  //
  // restrict channels w/ topo info
  //

  const int ntot = disp.topo.size();
  
  std::set<std::string> channels = disp.topo.channels();

  std::set<std::string> sigs2 = param.signal_set;
  
  param.signal_set.clear();
  
  std::set<std::string>::const_iterator ss2 = sigs2.begin();
  while ( ss2 != sigs2.end() )
    {
      if ( channels.find( *ss2 ) != channels.end() ) param.signal_set.insert( *ss2 );
      ++ss2;
    }
  param.signals = stringize( param.signal_set );


  //
  // print options
  //

  std::set<std::string> excls;
  for (int o=0; o < disp.size(); o++ ) 
    {
      if ( param.signal_set.find( disp.order(o) ) != param.signal_set.end() ) { continue; } // already in
      if ( channels.find( disp.order(o) ) == channels.end() ) { continue; }   // no topo info
      excls.insert( disp.order(o) );
    }

  std::cout << "<em><b>signals</b> :</em> [" 
	    << web_t::coh_link( "<em>all</em>" , param.folder , param.edf_file , param.indiv , "." )
	    << "] ["
	    << web_t::coh_link( "<em>none</em>" , param.folder , param.edf_file , param.indiv , ""  )
	    << "] ["
	    << web_t::coh_link( "<em>swap</em>" , param.folder , param.edf_file , param.indiv , stringize(excls) )
	    << "]";
  
  std::cout << "</p><hr>";
  
  std::cout << "<p><em>Incl:</em>";
  std::set<std::string>::const_iterator pp = param.signal_set.begin();
  while ( pp != param.signal_set.end() )
    {
      std::cout << " [" << web_t::coh_link( disp.display_label( *pp ) , param.folder , param.edf_file , param.indiv , stringize_except( param.signal_set , *pp )  ) << "] " ;
      ++pp;
    }
  
  std::cout << "</p><p><em>Excl:</em>";

  pp = excls.begin();
  while ( pp != excls.end() )
    {
      std::cout << " [" << web_t::coh_link( disp.display_label( *pp ) , param.folder , param.edf_file , param.indiv , stringize_add( param.signal_set , *pp )  ) << "] " ;
      ++pp;
    }


  //
  // End of header
  //
    
  std::cout << "</p></small><hr>";





  //
  // Load
  //

  sstore_t ss( filename );

  sstore_data_t data = ss.fetch_base();

  //  std::cout << "read " << data.data.size() << " datapoints</p>";
  
  // expecting CH will give {C1}_x_{C2}
  // LVL gives B={band}
  
  // [freq][c1/c2] = coh
  coh.clear();
  
  std::map<sstore_key_t,sstore_value_t>::const_iterator kk = data.data.begin();
  while ( kk != data.data.end() )
    {
      
      if ( kk->first.lvl.size() <= 2 ) Helper::halt( "bad format" );
      
      // assume lvl always has format F={Hz}
      std::string bnd = kk->first.lvl.substr( 2 ) ;

      // breakdown pair (CHS in C1_x_C2 format)
      lw_coh_pair_t chs( kk->first.ch );

      // only include if on the channel list
      if ( param.signal_set.find( chs.ch1 ) == param.signal_set.end() || 
	   param.signal_set.find( chs.ch2 ) == param.signal_set.end() ) 
	{ ++kk; continue; } 
      
      // coherence value
      double c = kk->second.dbl_value;
      
      // store
      coh[ chs ][ bnd ] = c;
      
      // double entry here
      chcoh[ chs.ch1 ][ bnd ][ chs.ch2 ] = c;
      chcoh[ chs.ch2 ][ bnd ][ chs.ch1 ] = c;
      
      ++kk;
    }
  


  
  //
  // Get list of channels and freqs
  //

  chs.clear();
  
  std::map<lw_coh_pair_t,std::map<std::string,double> >::const_iterator ii = coh.begin();
  while ( ii != coh.end() ) 
    {
      chs.insert( ii->first.ch1 );
      chs.insert( ii->first.ch2 );      
      ++ii;
    }






  //
  // Look for per-channel topo-plot (i.e. seed on each channel) 
  // rows = channels
  // cols = 5 delta, theta, alpha, sigma, beta
  //

  if ( chs.size() < 8 ) 
    Helper::halt( "requires at least 8 channels for a topoplot" );

  std::cout << "<table>";

  std::set<std::string>::const_iterator cc = chs.begin();
  while ( cc != chs.end() )
    {
      
      std::cout << "<tr><td>" << *cc << "</td><td>";
      
      coh_topo_plot( "DELTA" , disp , *cc );
      coh_topo_plot( "THETA" , disp , *cc );
      coh_topo_plot( "ALPHA" , disp , *cc );
      coh_topo_plot( "SIGMA" , disp , *cc );
      coh_topo_plot( "BETA"  , disp , *cc );

      std::cout << "</td></tr>";
      
      ++cc;

    }

  std::cout << "</table>";
  


  //\



  if ( edf ) 
    {
      delete edf; 
      edf = NULL; 
    }

  //
  // All done
  //

  return;
  

  //  std::cout << "<p>Coherence for " << *cc << "<br>" ;
  coh_plot( "SLOW" );
  coh_plot( "DELTA" );
  coh_plot( "THETA" );
  coh_plot( "ALPHA" );
  coh_plot( "SIGMA" );
  coh_plot( "BETA" );
  //  coh_plot( *cc );
  
  //  std::cout << "</P>";

  const int ns = chs.size();
  
  std::cout << "<p>Coherence for " << ns << " channels</p>";
  
}

void lw_coh_t::coh_topo_plot( const std::string & band , lw_signal_disp_t & disp , const std::string & ch )
{

  // topo plot
  // only get channels with topo information
  // extract out row that has CH1 X {all others} 
  
  // find data
  std::map<std::string,std::map<std::string,std::map<std::string,double> > >::const_iterator cc = chcoh.find( ch );
  if ( cc == chcoh.end() ) return;


  std::map<std::string,std::map<std::string,double> >::const_iterator zz = cc->second.find( band );
  if ( zz == cc->second.end() ) return;
  
  const std::map<std::string,double> & dd = zz->second; 
  
  
  Data::Matrix<double> I = disp.topo.interpolate( dd );

  // add point that marks this channel
  xy_t xy;
  double x,y;
  bool check = disp.topo.scaled_xy( ch , &x, &y );
  if ( check ) xy.add( x,y);

  double zmin = 0;
  double zmax = 1;
  int px = 150;
  lweb_make_topo( I , band , "c_" + Helper::search_replace( ch , '-' , '_' ) + "_" + band , 
		  px , px , &zmin , &zmax , check ? &xy : NULL );	  
  
}


void lw_coh_t::coh_plot( const std::string & band )
{
  
  const int ns = chs.size();
  
  int px = 8;
  double hgt = ns * px;
  double wid = ns * px;

  std::cout << "<canvas id=\"coh" << band << "Canvas\" height=\"" << hgt << "\" width=\"" << wid << "\">"
	    << "<script>"
	    << "var canvas = document.getElementById('coh" << band << "Canvas');"
	    << "if (canvas.getContext) { "
	    << "var ctx = canvas.getContext('2d');";

  Data::Matrix<double> C( ns , ns , -1 );
  
  int c1 = 0;
  std::set<std::string>::const_iterator cc = chs.begin();
  while ( cc != chs.end() ) 
    {
      int c2 = 0;
      std::set<std::string>::const_iterator dd = chs.begin();
      while ( dd != chs.end() ) 
	{
	  
	  lw_coh_pair_t ccdd( *cc , *dd );
	  std::map<lw_coh_pair_t,std::map<std::string,double> >::const_iterator ff = coh.find( ccdd );

	  if ( ff == coh.end() )  { ++c2; ++dd; continue; } 
	      
	  std::map<std::string,double>::const_iterator gg = ff->second.find( band );
	  if ( gg == ff->second.end() ) { ++c2; ++dd; continue; }
	  C( c1 , c2 ) = gg->second;

	  //Plot

	  if ( gg->second >= 0 && gg->second <= 1 )
	    {
	      double x = c1/(double)ns * wid;
	      double y = c2/(double)ns * hgt;
	      int col = int( 100 * gg->second );
	      if ( col == 100 ) col = 99;
	      double gs = 255 * (1-gg->second );
	      //	      std::cout << "ctx.fillStyle = '" << param.rbow[ col ] << "';";
	      std::cout << "ctx.fillStyle = 'rgb(" << gs << "," << gs << "," << gs << ")';";
	      std::cout << "ctx.fillRect(" << x << "," << y << "," << px << "," << px << ");";
	      
	    }

	  ++c2;
	  ++dd;
	}

      ++c1;
      ++cc;
    }

  //  std::cout << C.print() << "</p>";

  // may want to think about how to specify order of channels here
  // or, to make a plot of COH by DISTANCE? 
  
 
  
  // done w/ canvas
  std::cout << " } </script> "
	    << "</canvas>";

  
  

}
