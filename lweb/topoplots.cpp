
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#include "lw.h"
#include "web.h"
#include "param.h"
#include "misc.h"
#include "../sstore/sstore.h"
#include "../fftw/fftwrap.h"
#include "../clocs/topo.h"
#include "topoplots.h"

#include "../sstore/sstore.h"
#include "../edf/edf.h"

extern lw_param_t  param;


struct topoplot_sort_t { 
  std::string label; double z; 
  topoplot_sort_t( std::string label, double z ) : label(label) , z(z) { } 
  topoplot_sort_t() { } 
  bool operator<( const topoplot_sort_t & rhs ) const 
  { 
    if ( z == rhs.z ) return label < rhs.label; 
    return z > rhs.z ;
  }
};



void lw_signals_t::topoplots( )
{


  std::vector<std::string> bands;
  bands.push_back( "SLOW" );
  bands.push_back( "DELTA" );
  bands.push_back( "THETA" );
  bands.push_back( "ALPHA" );
  bands.push_back( "SLOW_SIGMA" );
  bands.push_back( "FAST_SIGMA" );
  bands.push_back( "BETA" );
  bands.push_back( "TOTAL" );


  // cols = slow, delta, theta, alpha, sigma, beta 
  // rows = channels 
  
  // either per 30-epoch or per stage

  bool is_epoch = param.e != -1 ;
  
  int epoch = param.e;
  

  //
  // display information, and channel locations
  //
  
  lw_signal_disp_t disp( param.folder , edf );



  //
  // Header
  //
  
  std::cout << "<hr><small>";

  if ( is_epoch )
    {

      // total EDF duration in seconds (may be fractional)
      double total_duration_sec = edf->header.nr * edf->header.record_duration; 
      
      // number of (whole) 30-second epochs
      int ne30 = edf->timeline.set_epoch( param.esize , param.esize  );
      
      
      //
      // Get time interval for this epoch
      //
      
      // first sanity check (i.e. w/ zooming in/out, sometimes epoch may end up being larger than ne)
      
      if ( epoch >= ne30 ) epoch = ne30-1;
      
      interval_t interval = edf->timeline.epoch( epoch );  
  
      
      //
      // Header
      //

      std::cout << "<p>";
      
      std::cout << "[" << web_t::topo_link( "<em>start</em>" , param.folder , param.edf_file , param.indiv , param.signals , 0  ) << "]"; 
      
      if ( epoch > 1 ) 
	std::cout << "[" << web_t::topo_link( "<em>prev</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch - 1 ) << "]"; 
      
      
      std::cout << " <b>epoch " << epoch+1 << "</b> of " << ne30 << " ";

      // next epoch button
      if ( epoch + 1 < ne30 ) 
	std::cout << "[" << web_t::topo_link( "<em>next</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch + 1 , "" ) << "]"; 
      
      std::cout << "[" << web_t::topo_link( "<em>end</em>" , param.folder , param.edf_file , param.indiv , param.signals , ne30-1 , "" ) << "]"; 
      
      std::cout << "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
      
      
      //
      // Duration
      //
      
      uint64_t duration_tp = globals::tp_1sec * (uint64_t)edf->header.nr * edf->header.record_duration ;
      std::cout << " | <em>duration<em>: <b>" <<   Helper::timestring( duration_tp ) << "</b>";


      //
      // Clocktime and/or elapsed time
      //
      
      double hours1 = interval.start_sec() / 3600.0;
      double hours2 = interval.stop_sec() / 3600.0;
      clocktime_t zerotime1, zerotime2;
      zerotime1.advance( hours1 );
      zerotime2.advance( hours2 );

      clocktime_t starttime( edf->header.starttime );
      if ( starttime.valid ) 
	{
	  starttime.advance( hours1 );
	  std::cout << " | <em>clock:</em> <b>" << starttime.as_string() << "</b>";
	}
      
      std::cout << " | <em>elapsed:</em> <b>" << zerotime1.as_string() << " .. " << zerotime2.as_string() << "</b>";
      
      
    }
  else  // ... othewise STAGE level data
    {
      std::cout << "<p>";

      if ( param.stage == "NREM1" ) std::cout << " <b>NREM1</b> "; 
      else std::cout << " [" << web_t::topo_link( "<em>NREM1</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , "NREM1" ) << "]" ; 

      if ( param.stage == "NREM2" ) std::cout << " <b>NREM2</b> "; 
      else std::cout << "[" << web_t::topo_link( "<em>NREM2</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , "NREM2" ) << "] ";

      if ( param.stage == "NREM3" ) std::cout << " <b>NREM3</b> "; 
      else std::cout << " [" << web_t::topo_link( "<em>NREM3</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , "NREM3" ) << "] ";

      if ( param.stage == "REM" ) std::cout << " <b>REM</b> ";
      else std::cout << " [" << web_t::topo_link( "<em>REM</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , "REM" ) << "] "; 
 
    }
  


  //
  // Signals
  //

  signal_list_t signals;




  // if param.signals is empty, this means to look at all signals
  
  if ( param.signals == "." ) 
    { 
      param.signal_set.clear();
      for (int o=0; o < disp.size(); o++ ) 
	{
	  signals.add( o , disp.order(o) );
	  param.signal_set.insert( disp.order(o) );	
	}
    }
  else
    {
      
      //
      // create a signal-string, in the correct order
      //
      
      std::string ordered_signal_string = "";
      bool firstdone = false;
      for (int o=0; o < disp.size(); o++ ) 
	{
	  if ( param.signal_set.find( disp.order(o) ) == param.signal_set.end() ) { continue; }
	  if ( firstdone ) ordered_signal_string += ",";
	  ordered_signal_string += "," + disp.order( o );       
	  firstdone = true;
	}
      
      signals = edf->header.signal_list( ordered_signal_string );
      
    }


  //
  // restrict channels w/ topo info
  //

  const int ntot = disp.topo.size();
  
  std::set<std::string> channels = disp.topo.channels();

  std::set<std::string> sigs2 = param.signal_set;
  
  param.signal_set.clear();
  
  std::set<std::string>::const_iterator ss = sigs2.begin();
  while ( ss != sigs2.end() )
    {
      if ( channels.find( *ss ) != channels.end() ) param.signal_set.insert( *ss );
      ++ss;
    }
  param.signals = stringize( param.signal_set );




  //
  // link to signal view (ed = 5 --> 30 second epochs
  //

  std::cout << "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
  
  std::cout << "[" << web_t::psd_link( "<em>PSD</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , param.stage  ) << "] ";

  std::cout << " [" << web_t::signals_link( "<em>signals</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , 4 ) << "]";


  //
  // print options
  //

  std::cout << "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
  
  std::set<std::string> excls;
  for (int o=0; o < disp.size(); o++ ) 
    {
      if ( param.signal_set.find( disp.order(o) ) != param.signal_set.end() ) { continue; } // already in
      if ( channels.find( disp.order(o) ) == channels.end() ) { continue; }   // no topo info
      excls.insert( disp.order(o) );
    }

  std::cout << "<em><b>signals</b> :</em> [" 
	    << web_t::topo_link( "<em>all</em>" , param.folder , param.edf_file , param.indiv , "." , epoch , param.stage )
	    << "] ["
	    << web_t::topo_link( "<em>none</em>" , param.folder , param.edf_file , param.indiv , "" , epoch , param.stage )
	    << "] ["
	    << web_t::topo_link( "<em>swap</em>" , param.folder , param.edf_file , param.indiv , stringize(excls) , epoch , param.stage )
	    << "]";
  
  std::cout << "</p><hr>";
  
  std::cout << "<p><em>Incl:</em>";
  std::set<std::string>::const_iterator ii = param.signal_set.begin();
  while ( ii != param.signal_set.end() )
    {
      std::cout << " [" << web_t::topo_link( disp.display_label( *ii ) , param.folder , param.edf_file , param.indiv , stringize_except( param.signal_set , *ii ) , epoch , param.stage ) << "] " ;
      ++ii;
    }
  
  std::cout << "</p><p><em>Excl:</em>";

  ii = excls.begin();
  while ( ii != excls.end() )
    {
      std::cout << " [" << web_t::topo_link( disp.display_label( *ii ) , param.folder , param.edf_file , param.indiv , stringize_add( param.signal_set , *ii ) , epoch , param.stage ) << "] " ;
      ++ii;
    }



  
  //
  // End of header
  //
    
  std::cout << "</p></small><hr>";

  
  //
  // Get data from psd sstore_t
  //

  const int ns = signals.size();

  
  //std::set<std::string> sigs = stringize( param.signals );
  


  
  //
  // Stage DB
  //
  

  const std::string & filename = param.folder + "/" + param.indiv + "/ss/stage-summary.db";

  bool has_stage_summary = Helper::fileExists( filename );  
  
  
  //
  // Load sstore, 
  //
  
//   if ( has_stage_summary ) 
//     {
//       sstore_t ss2( filename );
      
//       sstore_data_t data = ss2.fetch_base(); 

//     }

  //
  // Fetch mask, if present
  //

  // number of (whole) 30-second epochs
  int ne30 = edf->timeline.set_epoch( param.esize , param.esize  );
  
  std::string mask_db = param.folder + "/" + param.indiv + "/ss/mask.db";
  
  bool has_mask = Helper::fileExists( mask_db );

  // record all 30-second epoch masks
  std::vector<bool> mask30( ne30 , false ) ;
  
  if ( has_mask )
    {
      lw_signals_t::fetch_mask( mask_db , &mask30 );
    }

  
  //
  // Fetch stages, if present (they should be, if we've been able to get the stage summary)
  //

  
  std::string staging_db = param.folder + "/" + param.indiv + "/ss/staging.db";
  
  bool has_staging = Helper::fileExists( staging_db );

  // record all 30-second epoch stages
  std::vector<std::string> stages30;
  std::vector<bool> wake30;
    

  if ( has_staging )
    {
      
      lw_signals_t::stage_plot( staging_db , ne30, ne30 , -1 , &stages30 , &wake30 , has_mask ? &mask30 : NULL , NULL );
  

      //
      // Allow mouse click to redirect to this (30-second) epoch
      //
      
      std::cout << "function getMousePos(canvas, evt) { var rect = canvas.getBoundingClientRect(); return { x: evt.clientX - rect.left, y: evt.clientY - rect.top }; }";
										   

      // note: this sets 'ed' to 4 which here means 30 second epochs
      
      std::cout << "canvas1.addEventListener( \"click\", function ( e ) { var mousePos = getMousePos(canvas1, e); "
		<< "window.location = \"lunaweb.cgi?m=tv";
      if ( param.hilbert ) std::cout << "&hlt=1";
      std::cout << "&edf=" << param.edf_file 
		<< "&i=" << param.indiv 
		<< "&s=" << stringize( param.signal_set ) 
		<< "&p=" << param.folder 
		<< "&e=\" + Math.floor( (mousePos.x/" << param.width << ") * " << ne30 << ") + \"&ed="<<param.def_ed << "\"" ;
      
      std::cout << "; }  , false ); "; 

      // end staging canvas
      std::cout << "}</script></canvas>";

    }



  
  //
  // Topo Band DB
  //
  
  std::cout << "<br>";

  
  std::string band_db = param.folder + "/" + param.indiv + "/ss/psd-band-epoch.db";
  
  bool has_band = Helper::fileExists( band_db );

  
  if ( is_epoch )
    {

      
      sstore_t ss( band_db );

      // epochs stored as 1-based ints
      sstore_data_t data = ss.fetch_epoch( epoch + 1);
      
      std::map<std::string,std::map<std::string,double> > zvals;

      std::map<sstore_key_t,sstore_value_t>::const_iterator kk = data.data.begin();
      while ( kk != data.data.end() )
	{
	  
	  // include this signal? 	  
	  if ( param.signal_set.find( kk->first.ch ) == param.signal_set.end() ) { ++kk; continue; }
	  
	  // skip SIGMA is we already have SLOW_SIGMA and FAST_SIGMA
	  if ( kk->first.lvl == "SIGMA" ) { ++kk; continue; }

	  //	  std::cout << "<p>" << kk->first.id << " " << kk->first.ch << " " << kk->first.lvl << " = " << kk->second.dbl_value << "</p>";
	  
	  // SLOW DELTA THETA ALPHA SLOW_SIGMA FAST_SIGMA BETA TOTAL 
	  zvals[ kk->first.lvl ][ kk->first.ch ] = kk->second.dbl_value;	  
	  
	  ++kk;

	}
      
    

           
      //
      // Create grid for topo plot
      //
      
      disp.topo.max_radius( 0.55 );
      
      disp.topo.grid( 67 , 67 );
      
      
      
      //
      // Topoplots
      //
      
      int px = 0;

      for (int b=0;b<bands.size();b++)
	{

	  const std::map<std::string,double> & zdata = zvals[ bands[b] ];
      
	  if (zdata.size() < 8 ) 
	    Helper::halt( "requires at least 8 channels for a topoplot" );
	  
	  Data::Matrix<double> I = disp.topo.interpolate( zdata );

	  px = I.dim1() ;
	  
	  lweb_make_topo( I , bands[b] , "c_" + bands[b] , px , I.dim2() );	  

	}

      std::cout << "<br>";
      
      //
      // Channel-specific histograms
      //
      
      for (int b=0;b<bands.size();b++)
	{
	  
	  const std::map<std::string,double> & zdata = zvals[ bands[b] ];
	  
	  lweb_make_hist( "h_" + bands[b] , px , zdata.size() * 10 , zdata , disp );

	}


    }
  
  
      //
      // Stage plot
      //
      
  
}



void lweb_make_topo( Data::Matrix<double> & I , const std::string & label , const std::string & canvas , int px , int py , 
		     double * pzmin , double * pzmax , 
		     xy_t * xy )
{
  
  const int nx = I.dim1();
  const int ny = I.dim2();

  // normalize

  double zmin = 99999;
  double zmax = -9999;
  
  if ( pzmin != NULL ) zmin = *pzmin;
  if ( pzmax != NULL ) zmax = *pzmax;
  else
    {
      
      
      for (int i=0;i<nx;i++)
	for (int j=0;j<ny;j++)
	  {
	    if ( I(i,j) <= -999 ) continue;
	    if ( zmin > I(i,j) ) zmin = I(i,j);
	    if ( zmax < I(i,j) ) zmax = I(i,j);	
	  }
    }
  
  // scale
  
  for (int i=0;i<nx;i++)
    for (int j=0;j<ny;j++)
      {
	if ( I(i,j) <= -999 ) continue;
	I(i,j) = 100 * ( ( I(i,j) - zmin ) / ( zmax - zmin ) ) ;
	//std::cout << (int)I(i,j) << " ";
      }

  const double xspacer = 25;
  const double width = px + xspacer * 2 ; // space each side

  const double ylabel = 25;
  const double height = py + ylabel;

  std::cout << "<canvas id=\"" << canvas << "\" width=\"" << width << "\" height=\"" << height << "\">"
	    << "<script>"
	    << " var ctx_"<< canvas << " = document.getElementById('" << canvas << "').getContext('2d');";

  // scale

  const double xi = px / double(nx+1);  
  const double yi = py / double(ny+1);  
//   const double xw = 1;
//   const double yw = 1;

  const double xw = xi+1;
  const double yw = yi+1;
  
  for (int i=0;i<nx;i++)
    for (int j=0;j<ny;j++)
      {
	if ( I(i,j) <= -999 ) continue;
	double x = xspacer + i * xi;
	double y = ylabel + j * yi;
	int z = I(ny-j-1,i); // note -- kludge, but do a transpose here... and flip 
	if ( z < 0 ) z = 0; 
	if ( z > 99 ) z = 99;
	std::cout << "ctx_" << canvas << ".fillStyle = '" << param.rbow[z] << "';";
    	std::cout << "ctx_" << canvas << ".fillRect( " << x << "," << y << "," << xw << "," << yw << ");";
      }
  

  // specific points? 
  
  if ( xy != NULL ) 
    {
      const int nxy = xy->size();
      for (int p=0;p<nxy;p++)
	{
	  // assume xy_t is on 0..1 scale (note: swapped x/y , and 1-y)
	  double x = xspacer + xy->y[p] * py;
	  double y = ylabel + (1-xy->x[p]) * px;
	  std::cout << "ctx_" << canvas << ".fillStyle = 'black';";
	  std::cout << "ctx_" << canvas << ".fillRect( " << x << "," << y << "," << xw*2 << "," << yw*2 << ");";
	}
    }

  
  // label
  std::cout << "ctx_" << canvas << ".font = \"10px Arial\";";
  std::cout << "ctx_" << canvas << ".fillStyle = 'black';";
  std::cout << "ctx_" << canvas << ".fillText(\"" << label << "\", 5 , " << 15 << ");";
  
  std::cout << "</script></canvas>";
  
}


void lweb_make_hist( const std::string & canvas , int px , int py , const std::map<std::string,double> & z , const lw_signal_disp_t & disp )
{

  const double xspacer = 25;
  const double width = px + 2 * xspacer;
  const double xlabel = 15;
  px -= xlabel;

  const double yspacer = 10;
  const double height = py + yspacer;

  if ( px < 10 ) Helper::halt( "bad plot");
  const int ns = z.size();
  
  double zmin = 99999;
  double zmax = -9999;

  std::map<std::string,double>::const_iterator zz = z.begin();
  while ( zz != z.end() )
    {      
      if ( zz->second < zmin ) zmin = zz->second;
      if ( zz->second > zmax ) zmax = zz->second;
      ++zz;
    }
  


  std::set<topoplot_sort_t> ts;
  
  zz = z.begin();
  while ( zz != z.end() )
    {
      ts.insert( topoplot_sort_t( zz->first , ( zz->second - zmin ) / ( zmax - zmin ) ) );
      ++zz;
    }

  
  std::cout << "<canvas id=\"" << canvas << "\" width=\"" << width << "\" height=\"" << height << "\">"
	    << "<script>"
	    << " var ctx_"<< canvas << " = document.getElementById('" << canvas << "').getContext('2d');";

  // font
  std::cout << "ctx_" << canvas << ".font = \"10px Arial\";";

  // scale
  
  const double xw = px ;
  const double yi = py / double(ns+1);  
  const double yw = 2;
  
  std::set<topoplot_sort_t>::const_iterator tt = ts.begin();
  int ycnt = 0;
  while ( tt != ts.end() )
    {
      double x = xspacer + xlabel;
      double xx = tt->z * xw;
      double y = yspacer + yi * ycnt;
      
      int z = tt->z * 100;
      if ( z < 0 ) z = 0; 
      if ( z > 99 ) z = 99;

      std::cout << "ctx_" << canvas << ".fillStyle = '" << param.rbow[z] << "';";
      std::cout << "ctx_" << canvas << ".fillRect( " << x << "," << y << "," << xx << "," << yw << ");";      
     
      // label
      std::cout << "ctx_" << canvas << ".fillStyle = 'black';";
      std::cout << "ctx_" << canvas << ".fillText(\"" << disp.display_label( tt->label ) << "\", 5 , " << ( y+5 ) << ");";
      
      ++ycnt;
      ++tt;
    }
  
  std::cout << "</script></canvas>";

//   if ( edf ) 
//     {
//       delete edf; 
//       edf = NULL; 
//     }

}
