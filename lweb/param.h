
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#ifndef __LW_PARAM_H__
#define __LW_PARAM_H__

#include <string>
#include <vector>
#include <set>
#include <sstream>
#include <map>

struct lw_param_t
{
   
  // p    project folder

  std::string folder;
  
  // For project folder 'f', expect
  //  f/header     HTML format title and header
  //  f/samples    LUNA-format sample list
  //  f/channels   list order and display information for channels, as well as topographical information
  //  f/outputs    list types and descriptions of 'outputs' potentially available in f/out/

  //  f/ss/        sstore_t databases, named per ID
  //  f/meta/      simple text format per-individual phenotype meta-information
  //  f/out/       destrat database files named f/out/${indiv}-${type}.db

  
  //
  // View mode
  //

  std::string mode;  
  
  // pv    project view  
  // hv    header view
  // ov    output view
  // sv    signal view
  // 1v    single-channel view
  // av    annotation view
  // iv    individual (meta-information) view
  
  
  //
  // Other pieces of options specified on the command line
  //

  // edf file location 
  std::string edf_file;
  
  // i    if under any mode except pv, need this specified
  std::string indiv;
  
  // s    if viewing particular signal(s) under mode=sv
  std::string signals;

  // signals
  std::set<std::string> signal_set;
  
  // o    if viewing a particular outputfile under mode=ov
  std::string outdb;
  
  // d    dump contents of a sstore (n, a, e, i ) 
  std::string outdb_dump;
  
  // v    only give one variable in outdb dump
  std::string outdb_var;
  

  // default epoch size (i.e. if not 30)
  int esize;
  int def_ed; // default ed, should match esize, i.e. if esize = 30, ed = 4;;  esize= 10, ed = 3;

  //
  // Signal view parameters
  //

  // under sv: epoch boundaries
  int e;

  // for topoplots, if not by epoch
  std::string stage;
  
  // under sv: second boundaries --> converted to 'e' and 'ed' internally
  double a, b;
  
  
  // display size 
  double epoch_duration;

  // type of plot
  bool hilbert;
  

  // ExE plots
  
  std::string exe_signal;
  std::string exe_type;
  std::string exe_cluster;

  // files
  bool has_project;
  bool has_title; 
  bool has_channels;
  bool has_meta;
  bool has_topo;
  
  std::vector<std::string> col;

  // rainbow (100-col) pallete
  // rev( rainbow(100 , start = 0, end=0.7) ) 
  std::vector<std::string> rbow;
  
  std::string yellow;
  std::string red;
  std::string orange;
  std::string green;
  std::string lightgreen;
  std::string blue;
  std::string gray;
  
  std::map<std::string,std::string> stagecol;


  // graphics
  
  double width;
  double sighgt;
  
  lw_param_t() 
  {
  
    mode = "pv";
    
    indiv = "";
    outdb_dump = "n";
    outdb_var = ".";

    hilbert = false;
    
    exe_signal = ".";
    exe_type = ".";
    exe_cluster = ".";

    //
    // Epochs
    //
    
    e = -1;
    a = b = -1;
    
    // default epoch size

    esize = 30; 
    def_ed = 4;

    //
    // Cols
    //
    
    red        = "#E06666";
    lightgreen = "#D1E0B2";
    orange     = "#FF9933";
    green      = "#66A366";
    blue       = "#BFD4FF";
    gray       = "#DDDDDD";
    yellow     = "#FFE600";
    
    //
    // stage cols
    //

    stagecol[ "Wake" ]  = "silver";
    stagecol[ "NREM1" ] = "powderblue";
    stagecol[ "NREM2" ] = "dodgerblue";
    stagecol[ "NREM3" ] = "navy";
    stagecol[ "NREM4" ] = "navy";
    stagecol[ "REM" ]   = "darkorange";
    stagecol[ "." ]     = "orchid";


    col.clear();

    col.push_back( "\"#FFFFFF\""); 
    col.push_back( "\"#FFB300\"");
    col.push_back( "\"#803E75\"");
    col.push_back( "\"#FF6800\"");
    col.push_back( "\"#A6BDD7\"");
    col.push_back( "\"#C10020\"");
    col.push_back( "\"#CEA262\"");
    col.push_back( "\"#817066\"");
    
    col.push_back( "\"#007D34\"");
    col.push_back( "\"#F6768E\"");
    col.push_back( "\"#00538A\"");
    col.push_back( "\"#FF7A5C\"");
    col.push_back( "\"#53377A\"");
    col.push_back( "\"#FF8E00\"");
    col.push_back( "\"#B32851\"");
    col.push_back( "\"#F4C800\"");
    col.push_back( "\"#7F180D\"");
    col.push_back( "\"#93AA00\"");
    col.push_back( "\"#593315\"");
    col.push_back( "\"#F13A13\"");
    col.push_back( "\"#232C16\"");


    //
    // Rainbow
    //

    rbow.push_back( "#3300FFFF" );
    rbow.push_back("#2800FFFF" );
    rbow.push_back("#1D00FFFF" );
    rbow.push_back("#1300FFFF" );
    rbow.push_back("#0800FFFF" );
    rbow.push_back("#0003FFFF" );
    rbow.push_back("#000EFFFF" );
    rbow.push_back("#0019FFFF" );
    rbow.push_back("#0024FFFF" );
    rbow.push_back("#002EFFFF" );
    rbow.push_back("#0039FFFF" );
    rbow.push_back("#0044FFFF" );
    rbow.push_back("#004FFFFF" );
    rbow.push_back("#005AFFFF" );
    rbow.push_back("#0064FFFF" );
    rbow.push_back("#006FFFFF" );
    rbow.push_back("#007AFFFF" );
    rbow.push_back("#0085FFFF" );
    rbow.push_back("#0090FFFF" );
    rbow.push_back("#009BFFFF" );
    rbow.push_back("#00A5FFFF" );
    rbow.push_back("#00B0FFFF" );
    rbow.push_back("#00BBFFFF" );
    rbow.push_back("#00C6FFFF" );
    rbow.push_back("#00D1FFFF" );
    rbow.push_back("#00DBFFFF" );
    rbow.push_back("#00E6FFFF" );
    rbow.push_back("#00F1FFFF" );
    rbow.push_back("#00FCFFFF" );
    rbow.push_back("#00FFF7FF" );
    rbow.push_back("#00FFECFF" );
    rbow.push_back("#00FFE2FF" );
    rbow.push_back("#00FFD7FF" );
    rbow.push_back("#00FFCCFF" );
    rbow.push_back("#00FFC1FF" );
    rbow.push_back("#00FFB6FF" );
    rbow.push_back("#00FFACFF" );
    rbow.push_back("#00FFA1FF" );
    rbow.push_back("#00FF96FF" );
    rbow.push_back("#00FF8BFF" );
    rbow.push_back("#00FF80FF" );
    rbow.push_back("#00FF75FF" );
    rbow.push_back("#00FF6BFF" );
    rbow.push_back("#00FF60FF" );
    rbow.push_back("#00FF55FF" );
    rbow.push_back("#00FF4AFF" );
    rbow.push_back("#00FF3FFF" );
    rbow.push_back("#00FF35FF" );
    rbow.push_back("#00FF2AFF" );
    rbow.push_back("#00FF1FFF" );
    rbow.push_back("#00FF14FF" );
    rbow.push_back("#00FF09FF" );
    rbow.push_back("#02FF00FF" );
    rbow.push_back("#0CFF00FF" );
    rbow.push_back("#17FF00FF" );
    rbow.push_back("#22FF00FF" );
    rbow.push_back("#2DFF00FF" );
    rbow.push_back("#38FF00FF" );
    rbow.push_back("#42FF00FF" );
    rbow.push_back("#4DFF00FF" );
    rbow.push_back("#58FF00FF" );
    rbow.push_back("#63FF00FF" );
    rbow.push_back("#6EFF00FF" );
    rbow.push_back("#79FF00FF" );
    rbow.push_back("#83FF00FF" );
    rbow.push_back("#8EFF00FF" );
    rbow.push_back("#99FF00FF" );
    rbow.push_back("#A4FF00FF" );
    rbow.push_back("#AFFF00FF" );
    rbow.push_back("#B9FF00FF" );
    rbow.push_back("#C4FF00FF" );
    rbow.push_back("#CFFF00FF" );
    rbow.push_back("#DAFF00FF" );
    rbow.push_back("#E5FF00FF" );
    rbow.push_back("#F0FF00FF" );
    rbow.push_back("#FAFF00FF" );
    rbow.push_back("#FFF900FF" );
    rbow.push_back("#FFEE00FF" );
    rbow.push_back("#FFE300FF" );
    rbow.push_back("#FFD800FF" );
    rbow.push_back("#FFCE00FF" );
    rbow.push_back("#FFC300FF" );
    rbow.push_back("#FFB800FF" );
    rbow.push_back("#FFAD00FF" );
    rbow.push_back("#FFA200FF" );
    rbow.push_back("#FF9700FF" );
    rbow.push_back("#FF8D00FF" );
    rbow.push_back("#FF8200FF" );
    rbow.push_back("#FF7700FF" );
    rbow.push_back("#FF6C00FF" );
    rbow.push_back("#FF6100FF" );
    rbow.push_back("#FF5700FF" );
    rbow.push_back("#FF4C00FF" );
    rbow.push_back("#FF4100FF" );
    rbow.push_back("#FF3600FF" );
    rbow.push_back("#FF2B00FF" );
    rbow.push_back("#FF2000FF" );
    rbow.push_back("#FF1600FF" );
    rbow.push_back("#FF0B00FF" );
    rbow.push_back("#FF0000FF" );
    

    //
    // Signal view
    //

    epoch_duration = 3; // 3 --> 10 seconds

    // canvas size, and spacing in pixels
    width = 1400;

    sighgt = 50;
    
  }
  

};

#endif
