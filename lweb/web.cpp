
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#include "web.h"
#include "cgi.h"
#include "param.h"
#include "misc.h"

#include "helper/helper.h"
#include "clocs/topo.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <cmath>

extern lw_param_t param;

void web_bail_function( const std::string & msg )
{
  std::cout << "<p><em>Error:</em>: " << msg << "</p>";
  web_t::write_footer();
  std::exit(1);
}


//
// URL argument syntax
//

// p    project name    (i.e. specifies a root folder
//            folder/samples      LUNA file, that may point to feature lists
//            folder/topo         channel topography 
//            folder/channels     channel meta-information and topgraphy
//            folder/individuals  individual meta-information
//            folder/ss/          per-individual sstore_t meta-information
//                                     each item is an sstore_t, simply named for that individual

  // i    indiv name
  // o    output file
  // s    signal name(s) 
  // q    quantity/ies to plot/view 

  // a    start (in seconds)
  // b    stop (in seconds)

  // e    start in epochs (30-seconds)
  // f    stop in epochs (30-seconds)

  // d    dump contents of an ss outdb 

  // Project view:  a table of individuals
  //   ID     headers    signals    output  


  // Signal view: 
  //    



void web_t::parse_cgi()
{


  char ** cgivars = getcgivars();
  
  std::string val1 = "";
  
  for (int i=0; cgivars[i]; i+= 2)
    {
      
      std::string str = cgivars[i];      
      
      if ( str == "p" )
	{
	  param.folder = cgivars[i+1];
	}

      else if ( str == "m" ) 
	{	  
	  std::string m = cgivars[i+1];
	  if      ( m == "pv" ) param.mode = "pv"; // project view
	  else if ( m == "hv" ) param.mode = "hv"; // header view
	  else if ( m == "summv" ) param.mode = "summv"; // hypno summary view
	  else if ( m == "ov" ) param.mode = "ov"; // output-db view
	  else if ( m == "iv" ) param.mode = "iv"; // individual view ?? used?? 
	  else if ( m == "sv" ) param.mode = "sv"; // signal vuew
	  else if ( m == "ev" ) param.mode = "ev"; // ExE mode
	  else if ( m == "tv" ) param.mode = "tv"; // topographical view
	  else if ( m == "cv" ) param.mode = "cv"; // coherence view
	  else if ( m == "psdv" ) param.mode = "psdv"; // PSD view
	  else Helper::halt( "did not recognise command " + m );
	}
      
      else if ( str == "i" ) 
	{
	  param.indiv = cgivars[i+1];
	}

      else if ( str == "o" ) 
	{
	  param.outdb = cgivars[i+1];
	}
      
      else if ( str == "d" )
	{
	  param.outdb_dump = cgivars[i+1];
	}
            
      else if ( str == "v" )
	{
	  param.outdb_var = cgivars[i+1];
	}

      else if ( str == "edf" )
	{
	  param.edf_file = cgivars[i+1];
	}

      else if ( str == "e" ) 
	{
	  std::string s = cgivars[i+1];
	  
	  if ( ! Helper::str2int( s , &param.e ) ) 
	    Helper::halt( "needed an E" );
	}

      else if ( str == "stg" )
	{
	  param.stage = cgivars[i+1];
	}

      else if ( str == "a" ) 
	{ 
	  std::string s = cgivars[i+1];
	  
	  if ( ! Helper::str2dbl( s , &param.a ) ) 
	    Helper::halt( "bad value for window" );
	} 
      
      else if ( str == "b" ) 
	{ 
	  std::string s = cgivars[i+1];
	  
	  if ( ! Helper::str2dbl( s , &param.b ) ) 
	    Helper::halt( "bad value for window" );
	} 

      else if ( str == "s" )
	{
	  // expect comma-delim signal list	  
	  param.signals =  cgivars[i+1];
	  param.signal_set.clear();
	  if ( param.signals != "" ) 
	    param.signal_set = strset( param.signals , "," );
	}

      else if ( str == "exes" ) // for ExE, which signal?
	{
	  param.exe_signal = cgivars[i+1];
	}

      else if ( str == "exet" ) // for ExE, type of plot
	{
	  param.exe_type = cgivars[i+1];
	}

      else if ( str == "exec" ) // for ExE, plot a specific cluster
	{
	  param.exe_cluster = cgivars[i+1];
	}
      
      else if ( str == "ed" )
	{
	  std::string s = cgivars[i+1];
	  
	  if ( ! Helper::str2dbl( s , &param.epoch_duration ) ) 
	    Helper::halt( "needed an E" );
	  
	  if ( param.epoch_duration < 1 ) param.epoch_duration=1;
	  if ( param.epoch_duration > 6 ) param.epoch_duration=6;
	       
	}
      
      else if ( str == "hlt" ) 
	{
	  param.hilbert = true;
	}
      
      
    } // next argument


  //
  // a few checks
  //

  if ( param.hilbert ) 
    {
      if ( param.epoch_duration <= 2 ) param.epoch_duration = 3; // min 10sec for Hilbert view
      if ( param.epoch_duration >  5 ) param.epoch_duration = 5; // and max of 90sec
    }
  
  // free CGI variables  
  for (int i=0; cgivars[i]; i++) free(cgivars[i]) ;
  free(cgivars) ;

}



void web_t::write_header()
{

  //
  // HEAD 
  //
  
  std::cout << "Content-type: text/html\n\n"
    "<!DOCTYPE html>"
    "<html lang=\"en\">"
    "<head><meta charset=\"utf-8\"><title>lunaweb | " << param.folder << "</title>"
    "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />";

  std::cout << "<style>"
	    << " th, td { border: 1px solid rgb(180,180,200) ; } "
	    << " th { background-color: rgb(180,180,200) ; } "
    	    << " td,th { text-align: center; } "     
    	    << "</style>";
  
  
  std::cout << "</head>";
  

  //
  // BODY
  //
  
  std::cout << "<body style=\"font-family:georgia;"
	    << " margin-left: 5%; "
	    << " background-image: url('http://zzz.bwh.harvard.edu/media/zzz.png'); "
	    << " background-repeat: repeat-y;"    
	    << " background-size: 5%;"
	    << "\">"; 
    


}



void web_t::write_footer()
{
  std::cout  << "<hr></body></html>";
}



void web_t::user_header( const std::string & f )
{
  if ( ! Helper::fileExists( f ) ) return;
  std::ifstream IN1( f.c_str() , std::ios::in );
  
  while ( ! IN1.eof() ) 
    {
      std::string t;
      IN1 >> t;
      if ( IN1.eof() ) break;
      std::cout << " " << t;
     }
  IN1.close();
}


std::string web_t::brief( const std::string & s , int l )
{
  if ( s.size() < l ) return s;
  return s.substr(0,l-3) + "...";
}

std::string web_t::pp(const std::string & str , const int len ) 
{
  // if contains <br> in the sequence, assume that it has been formated okay
  if ( str.find( "<br>" ) != std::string::npos ) return str;
  if ( str.size() < len ) return str;
  return "<abbr title=\"" + str + "\">" + str.substr(0,len) + "...</abbr>";
}


std::string web_t::abbr(const std::string & tag , const std::string & str )
{
  return "<abbr title=\"" + str + "\">" + tag + "</abbr>";
}


std::string web_t::project_link( const std::string & txt , const std::string & p )
{
  std::stringstream ss;
  ss << "<a href=\"lunaweb.cgi?m=pv&p=" << p << "\">" << txt << "</a>";
  return ss.str();
}


std::string web_t::headers_link( const std::string & txt , const std::string & p , const std::string & edf , const std::string & i )
{
  std::stringstream ss;
  ss << "<a href=\"lunaweb.cgi?m=hv&p=" << p << "&edf="<< edf << "&i=" << i << "\">" << txt << "</a>";
  return ss.str();
}


std::string web_t::staging_link( const std::string & txt , const std::string & p , const std::string & edf , const std::string & i )
{
  std::stringstream ss;
  ss << "<a href=\"lunaweb.cgi?m=summv&p=" << p << "&edf="<< edf << "&i=" << i << "\">" << txt << "</a>";
  return ss.str();
}


std::string web_t::outputs_link( const std::string & txt , const std::string & p ,  const std::string & i , const std::string & t , const std::string & dump_mode , const std::string & var )
{
  std::stringstream ss;
  ss << "<a href=\"lunaweb.cgi?m=ov&i=" << i 
     << "&o=" << t 
     << "&v=" << var 
     << "&edf=" << param.edf_file 
     << "&d=" << dump_mode 
     << "&p=" << p << "\">" << txt << "</a>";
  return ss.str();
}



std::string web_t::hilbert_link( const std::string & txt , const std::string & p , const std::string & edf ,  // project and EDF
				 const std::string & i , // indiv
				 const std::string & s , // signals
				 const int e , const int ed , const double a , const double b ) // location in file
{
  bool org = param.hilbert;
  param.hilbert = true;
  std::string ss = signals_link( txt , p , edf , i , s, e , ed , a, b );
  param.hilbert = org;
  return ss;
}


std::string web_t::raw_link( const std::string & txt , const std::string & p , const std::string & edf ,  // project and EDF
			     const std::string & i , // indiv
			     const std::string & s , // signals
			     const int e , const int ed , const double a , const double b ) // location in file
{
  bool org = param.hilbert;
  param.hilbert = false;
  std::string ss = signals_link( txt , p , edf , i , s, e , ed , a, b );
  param.hilbert = org;
  return ss;
}


std::string web_t::signals_link( const std::string & txt , const std::string & p , const std::string & edf ,  // project and EDF
				 const std::string & i , // indiv
				 const std::string & s , // signals
				 const int e , const int ed , const double a , const double b ) // location in file
{

  std::stringstream ss;
  ss << "<a href=\"lunaweb.cgi?m=sv&i=" << i;
  
  if ( param.hilbert ) 
    ss << "&hlt=1";

  if ( a > 0 && b > 0 ) 
    ss << "&a=" << a 
       << "&b=" << b ;
  else
    ss << "&e=" << e 
       << "&ed=" << ed ;

  
  ss << "&edf=" << edf
     << "&s=" << s
     << "&p=" << p 
     << "\">" << txt << "</a>";

  return ss.str();
  
}





std::string web_t::topo_link( const std::string & txt , const std::string & p , const std::string & edf, const std::string & i, const std::string & s, 
			       const int e ,  const std::string & stage  )
{
  std::stringstream ss;
  ss << "<a href=\"lunaweb.cgi?m=tv&i=" << i;
  
  if ( e != -1 ) ss << "&e=" << e ;  
  if ( stage != "" ) ss << "&stg=" << stage ;
       
  ss << "&edf=" << edf
     << "&s=" << s
     << "&p=" << p 
     << "\">" << txt << "</a>";

  return ss.str();

}

std::string web_t::coh_link( const std::string & txt , const std::string & p , const std::string & edf, const std::string & i, const std::string & s)
			     
{
  std::stringstream ss;
  ss << "<a href=\"lunaweb.cgi?m=cv&i=" << i;
  
  ss << "&edf=" << edf
     << "&s=" << s
     << "&p=" << p 
     << "\">" << txt << "</a>";

  return ss.str();

}


std::string web_t::psd_link( const std::string & txt, const std::string & p , const std::string & edf, const std::string & i, const std::string & s , 
			     const int e ,  const std::string & stage )
{
  std::stringstream ss;
  ss << "<a href=\"lunaweb.cgi?m=psdv&i=" << i;
  
  if ( e != -1 ) ss << "&e=" << e ;  
  if ( stage != "" ) ss << "&stg=" << stage ;
  
  ss << "&edf=" << edf
     << "&s=" << s
     << "&p=" << p 
     << "\">" << txt << "</a>";

  return ss.str();

}


std::string web_t::exe_link( const std::string & txt , const std::string & p , const std::string & edf , const std::string & i ,  const std::string & s ,  const std::string & t , const std::string & cluster )
{
  
  std::stringstream ss;
  ss << "<a href=\"lunaweb.cgi"
     << "?m=ev"
     << "&p=" << p 
     << "&i=" << i
     << "&edf=" << edf;
 
  if ( s != "." & t != "." ) 
    ss << "&exes=" << s
       << "&exet=" << t ;

  if ( cluster != "." ) 
    ss << "&exec=" << cluster;

  ss << "\">" << txt << "</a>";
  
  return ss.str();

}


