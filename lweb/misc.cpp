
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "misc.h"
#include <sstream>

std::set<std::string> strset( const std::string & k , const std::string & delim )
{
  std::set<std::string> s;
  std::vector<std::string> tok = Helper::quoted_parse( k , delim );
  for (int i=0;i<tok.size();i++) s.insert( Helper::unquote( tok[i]) );
  return s;
}


std::string stringize( const std::set<std::string> & s , const std::string & delim )
{
  std::stringstream ss;
  std::set<std::string>::const_iterator ii = s.begin();
  while ( ii != s.end() )
    {
      if ( ii != s.begin() ) ss << delim;
      ss << *ii;
      ++ii;
    }
  return ss.str();
}


std::string stringize_except( const std::set<std::string> & s , const std::string & ex , const std::string & delim  )
{
  std::stringstream ss;
  std::set<std::string>::const_iterator ii = s.begin();
  bool firstdone = false;
  while ( ii != s.end() )
    {
      if ( *ii == ex ) { ++ii; continue; } 
      if ( firstdone ) ss << delim;
      ss << *ii;
      firstdone = true;
      ++ii;
    }
  return ss.str();
}


std::string stringize_add( const std::set<std::string> & s , const std::string & ex , const std::string & delim )
{
  std::stringstream ss;
  std::set<std::string>::const_iterator ii = s.begin();
  ss << ex;
  while ( ii != s.end() )
    {
      ss << delim;
      ss << *ii;
      ++ii;
    }
  return ss.str();
}

