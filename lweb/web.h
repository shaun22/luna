
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#ifndef __WEB_H__
#define __WEB_H__

#include <cstdlib>
#include <string>
#include <iostream>

void web_bail_function( const std::string & msg );

struct web_t 
{
  static void parse_cgi();
  static void write_header();
  static void write_footer();  

  static void user_header( const std::string & f );
  
  static std::string brief( const std::string & s , int l );

  static std::string pp(const std::string & str , const int len );

  static std::string abbr(const std::string & tag , const std::string & str );

  static std::string project_link( const std::string & , const std::string & p ); 

  static std::string headers_link( const std::string & , const std::string & p , const std::string & edf_file , const std::string & indiv ); 

  static std::string staging_link( const std::string & , const std::string & p , const std::string & edf_file , const std::string & indiv ); 

  static std::string signals_link( const std::string & , const std::string & p , const std::string & edf, const std::string & i, const std::string & s = "" , 
				   const int e = 1 ,  const int ed = 3 , const double a = -1 ,  const double b = -1 );    
  
  static std::string hilbert_link( const std::string & , const std::string & p , const std::string & edf, const std::string & i, const std::string & s = "" , 
				   const int e = 1 ,  const int ed = 3 , const double a = -1 ,  const double b = -1 );    
  
  static std::string raw_link( const std::string & , const std::string & p , const std::string & edf, const std::string & i, const std::string & s = "" , 
			       const int e = 1 ,  const int ed = 3 , const double a = -1 ,  const double b = -1 );    

  static std::string topo_link( const std::string & , const std::string & p , const std::string & edf, const std::string & i, const std::string & s = "" , 
				const int e = 1 ,  const std::string & stage = "" );
  
  static std::string psd_link( const std::string & , const std::string & p , const std::string & edf, const std::string & i, const std::string & s = "" , 
			       const int e = 1 ,  const std::string & stage = "" );

  static std::string coh_link( const std::string & , const std::string & p , const std::string & edf, const std::string & i, const std::string & s = "" );

  static std::string outputs_link( const std::string & , const std::string & , const std::string & , const std::string & t = "." , const std::string & dump_mode = "n" , const std::string & var = "." );


  static std::string exe_link( const std::string & , const std::string & , const std::string & , const std::string & ,  const std::string & s = "." ,  const std::string & t = "." , const std::string & cluster = "." );

  static std::string zzz_icon( const std::string & label );
  

};




#endif
