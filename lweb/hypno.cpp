
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "lw.h"
#include "web.h"
#include "param.h"

#include "../sstore/sstore.h"

extern lw_param_t  param;


lw_hypnogram_summary_t::lw_hypnogram_summary_t( const std::string & edf_file , const std::string & indiv_id )
{
  
  edf = new edf_t;

  bool okay = edf->attach( edf_file , indiv_id );

  if ( ! okay ) Helper::halt( "could not load EDF" + edf_file );
  else std::cout << "<p><em>Attached : </em> " << edf_file << "</p>";

  
  //
  // Look for stage summary db
  //

  const std::string & filename = param.folder + "/" + param.indiv + "/ss/stage-summary.db";
  
  if ( ! Helper::fileExists( filename ) ) 
    Helper::halt( "no staging summary information found" );
  
  
  //
  // Load sstore, 
  //

  sstore_t ss( filename );
  
  data = ss.fetch_base(); 

  
  //
  // Extract measures
  //

  LIGHTS_OFF = data.data[ sstore_key_t( "LIGHTS_OFF" ) ].dbl_value;
  SLP_ONSET = data.data[ sstore_key_t( "SLEEP_ONSET" ) ].dbl_value;
  SLP_MIDPOINT = data.data[ sstore_key_t( "SLEEP_MIDPOING" ) ].dbl_value;
  FINAL_WAKE = data.data[ sstore_key_t( "FINAL_WAKE" ) ].dbl_value;
  LIGHTS_ON  = data.data[ sstore_key_t( "LIGHTS_ON" ) ].dbl_value;
  
  TIB = data.data[ sstore_key_t( "TIB" ) ].dbl_value;
  TST = data.data[ sstore_key_t( "TST" ) ].dbl_value;
  TPST = data.data[ sstore_key_t( "TPST" ) ].dbl_value;
  TWT = data.data[ sstore_key_t( "TWT" ) ].dbl_value;
  WASO = data.data[ sstore_key_t( "WASO" ) ].dbl_value;
  
  MINS_N1  = data.data[ sstore_key_t( "MINS_N1" ) ].dbl_value;
  MINS_N2  = data.data[ sstore_key_t( "MINS_N2" ) ].dbl_value;
  MINS_N3  = data.data[ sstore_key_t( "MINS_N3" ) ].dbl_value;
  MINS_N4  = data.data[ sstore_key_t( "MINS_N4" ) ].dbl_value;
  MINS_REM = data.data[ sstore_key_t( "MINS_REM" ) ].dbl_value;
  
  PCT_N1  = data.data[ sstore_key_t( "PCT_N1" ) ].dbl_value;
  PCT_N2  = data.data[ sstore_key_t( "PCT_N2" ) ].dbl_value;
  PCT_N3  = data.data[ sstore_key_t( "PCT_N3" ) ].dbl_value;
  PCT_N4  = data.data[ sstore_key_t( "PCT_N4" ) ].dbl_value;
  PCT_REM = data.data[ sstore_key_t( "PCT_REM" ) ].dbl_value;
  
  NREMC      = data.data[ sstore_key_t( "NREMC" ) ].dbl_value;
  NREMC_MINS = data.data[ sstore_key_t( "NREMC_MINS" ) ].dbl_value;
  
  SLP_LAT = data.data[ sstore_key_t( "SLP_LAT" ) ].dbl_value;
  SLP_EFF = data.data[ sstore_key_t( "SLP_EEF" ) ].dbl_value;
  SLP_EFF2 = data.data[ sstore_key_t( "SLP_EFF2" ) ].dbl_value;
  SLP_MAIN_EFF = data.data[ sstore_key_t( "SLP_MAIN_EEF" ) ].dbl_value;

  if ( MINS_REM > 0 ) 
    REM_LAT = data.data[ sstore_key_t( "REM_LAT" ) ].dbl_value;

  PER_SLP_LAT = data.data[ sstore_key_t( "PER_SLP_LAT" ) ].dbl_value;


}


lw_hypnogram_summary_t::~lw_hypnogram_summary_t()
{
  if ( edf ) 
    {
      delete edf; 
      edf = NULL; 
    }
}



void lw_hypnogram_summary_t::tabulate()
{
  
  //
  // Stage plot, with sleep cycles annotated
  //
  
  std::cout << "<hr>";

  
  std::map<int,int> cycles;
  
  for (int c = 1 ; c <= NREMC ; c++ ) 
    {
      int e1 = data.data[ sstore_key_t( "NREMC_START" , "C=" + Helper::int2str(c) , "" ) ].dbl_value;
      double mins = data.data[ sstore_key_t( "NREMC_MINS" , "C=" + Helper::int2str(c) , "" ) ].dbl_value; 
      int e2 = e1 + mins * ( 60 / param.esize ) ;  // 60/param.esize is # of epochs per minute
      cycles[ e1 ] = e2; // start->stop in base-1 epochs      
    }
  

  //
  // Fetch mask, if present
  //

  // number of (whole) 30-second epochs
  int ne30 = edf->timeline.set_epoch( param.esize , param.esize  );
  
  std::string mask_db = param.folder + "/" + param.indiv + "/ss/mask.db";
  
  bool has_mask = Helper::fileExists( mask_db );

  // record all 30-second epoch masks
  std::vector<bool> mask30( ne30 , false ) ;
  
  if ( has_mask )
    {
      lw_signals_t::fetch_mask( mask_db , &mask30 );
    }

  
  //
  // Fetch stages, if present (they should be, if we've been able to get the stage summary)
  //

  
  std::string staging_db = param.folder + "/" + param.indiv + "/ss/staging.db";
  
  bool has_staging = Helper::fileExists( staging_db );

  // record all 30-second epoch stages
  std::vector<std::string> stages30;
  std::vector<bool> wake30;
    

  if ( has_staging )
    {
      
      lw_signals_t::stage_plot( staging_db , ne30, ne30 , -1 , &stages30 , &wake30 , has_mask ? &mask30 : NULL , &cycles );
  
      // end staging canvas
      std::cout << "}</script></canvas>";
    }


  //
  // Tabulate key details
  //


  std::cout << "<hr>";

  std::cout << "<table width=100% border=0><tr><td width=30% style=\"border:none; vertical-align:top\">";

  std::cout << "<table>";
  std::cout << "<tr><td>Individual ID</td><td>" << param.indiv << "</td></tr>";

  std::cout << "<tr><td>Start time</td>";
  
  clocktime_t starttime( edf->header.starttime );
  
  if ( starttime.valid )
    std::cout << "<td>" << starttime.as_string() ;
  else
    std::cout << "<td>n/a>";
  std::cout << "</td></tr>";

  uint64_t duration_tp = globals::tp_1sec * (uint64_t)edf->header.nr * edf->header.record_duration ;
  std::cout << "<tr><td>Total duration</td><td>" << Helper::timestring( duration_tp ) << "</td></tr>";
  
  if ( starttime.valid ) 
    {
      clocktime_t stop_time = starttime;
      stop_time.advance( duration_tp );
      std::cout << "<tr><td>Stop time</td><td>" << stop_time.as_string() << "</td></tr>";
    }


  clocktime_t lightsout_time, slponset_time, slpmid_time , final_wake_time , lightson_time; 
  //  clocktime_t lightsout_time, sleeponset_time, slpmid_time , final_wake_time , lightson_time;

  if ( starttime.valid ) 
    {
      lightsout_time = starttime;
      slponset_time = starttime;
      slpmid_time = starttime;
      final_wake_time = starttime;
      lightson_time = starttime;
    }

  lightsout_time.advance( LIGHTS_OFF );
  slponset_time.advance( SLP_ONSET );
  slpmid_time.advance( SLP_MIDPOINT );
  final_wake_time.advance( FINAL_WAKE );
  lightson_time.advance( LIGHTS_ON );

  std::cout << "<tr><td>Lights out</td><td>" << lightsout_time.as_string() << " (" << Helper::dbl2str( LIGHTS_OFF,1) << " hrs)</td></tr>";
  std::cout << "<tr><td>Sleep onset</td><td>" << slponset_time.as_string() << " (" << Helper::dbl2str( SLP_ONSET,1) << " hrs)</td></tr>";
  std::cout << "<tr><td>Sleep midpoint</td><td>" << slpmid_time.as_string() << " (" << Helper::dbl2str( SLP_MIDPOINT,1) << " hrs)</td></tr>";
  std::cout << "<tr><td>Final wake</td><td>" << final_wake_time.as_string() << " (" << Helper::dbl2str( FINAL_WAKE,1) << " hrs)</td></tr>";
  std::cout << "<tr><td>Lights on</td><td>" << lightson_time.as_string() << " (" << Helper::dbl2str( LIGHTS_ON,1) << " hrs)</td></tr>";
  
  std::cout << "</table>";

  // middle table
  
  std::cout << "</td><td style=\"border:none;\" width=5%>&nbsp;</td><td width=30% style=\"border:none; vertical-align:top\">";

  std::cout << "<table>";
  std::cout << "<tr><td>N1 duration</td><td>" << MINS_N1 << " (mins), " << Helper::dbl2str(100*PCT_N1,1) << "%</td></tr>";
  std::cout << "<tr><td>N2 duration</td><td>" << MINS_N2 << " (mins), " << Helper::dbl2str(100*PCT_N2,1) << "%</td></tr>";
  std::cout << "<tr><td>N3/N4 duration</td><td> " << MINS_N3+MINS_N4 << " (mins), " << Helper::dbl2str(100*(PCT_N3+PCT_N4),1) << "%</td></tr>";
  std::cout << "<tr><td>REM duration</td><td>" << MINS_REM << " (mins), " << Helper::dbl2str(100*PCT_REM,1) << "%</td></tr>";
  std::cout << "</table>";

  // far  table
  
  std::cout << "</td><td style=\"border:none;\" width=5%>&nbsp;</td><td width=30% style=\"border:none; vertical-align:top\">";

  std::cout << "<table>";
  std::cout << "<tr><td>TIB</td><td>" << TIB<< " (mins)</td></tr>";
  std::cout << "<tr><td>TWT</td><td>" << TWT<< " (mins)</td></tr>";
  std::cout << "<tr><td>WASO</td><td>" << WASO << " (mins)</td></tr>";
  std::cout << "<tr><td>TST</td><td>" << TST << " (mins)</td></tr>";
  std::cout << "<tr><td>TpST</td><td>" << TPST<< " (mins)</td></tr>";

  std::cout << "<tr><td>Sleep latency</td><td>" << SLP_LAT<< " (mins)</td></tr>";
  std::cout << "<tr><td>Persistent sleep latency</td><td>" << PER_SLP_LAT<< " (mins)</td></tr>";
  std::cout << "<tr><td>REM latency</td><td>" << ( MINS_REM > 0 ? Helper::dbl2str( REM_LAT,1) : "n/a" ) << " (mins)</td></tr>";

  if ( SLP_EFF > 0 ) 
    std::cout << "<tr><td>Sleep efficiency</td><td>" << Helper::dbl2str( SLP_EFF , 1 ) << "</td></tr>";
  if ( SLP_MAIN_EFF > 0 ) 
    std::cout << "<tr><td>Sleep maintainance efficiency</td><td>" << Helper::dbl2str( SLP_MAIN_EFF , 1 ) << "</td></tr>";

  std::cout << "<tr><td>Sleep efficiency(alt)</td><td>" << Helper::dbl2str( SLP_EFF2 , 1 ) << "</td></tr>";

  std::cout << "<tr><td># NREM cycles</td><td>" << NREMC << "</td></tr>";
  std::cout << "<tr><td>Mean NREM duration</td><td>" << Helper::dbl2str( NREMC_MINS , 1 ) << " (mins)</td></tr>";
  std::cout << "</table>";

  // end, close outer table
  std::cout << "</td></tr></table>";

  



}


