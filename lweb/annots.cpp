
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "lw.h"
#include "web.h"
#include "param.h"
#include "misc.h"
#include "../sstore/sstore.h"

extern lw_param_t param;


void lw_signals_t::annotplots( lw_annot_t & annots , lw_signal_disp_t & disp , int tot_sec , int ne30 )
{

  // nb. tot_sec is total duration of all whole epochs, given the current epoch size
  // i.e. truncates anything at end, so that this plot lines up with other plots

  // number of channels
  const int na = annots.all();
  
  //  std::cout << "want to plot " << na << " annot " << tot_sec << "</p>";  
  
  const double hgt = 20;
  const double yspacer = 15;
  const double height = yspacer + hgt * na;

  std::cout << "<canvas id=\"aCanvas\" height=\"" << height << "\" width=\"" << param.width << "\">"
	    << "<script>"
	    << "var canvas1 = document.getElementById('aCanvas');"
	    << "if (canvas1.getContext) { "
	    << "var ctx1 = canvas1.getContext('2d');";
  
  std::cout << "ctx1.strokeStyle = 'orange';"
	    << "ctx1.lineWidth = '3';"
	    << "ctx1.strokeRect(0, 0, " << param.width << "," << height << ");";
  
  
  
  // for each annotation
  int acnt = 0;
  std::map<lw_annot_key_t,lw_annot_evts_t>::const_iterator aa = annots.annots.begin();
  
  while ( aa != annots.annots.end() )
    {
      
      const std::string & label = aa->first.label;
      const std::string & desc  = aa->first.desc;
      const std::string & col   = aa->first.col;

      // any channels?
      std::map<std::string,std::map<interval_t,lw_annot_evt_t> >::const_iterator ee = aa->second.evts.begin();
      while ( ee != aa->second.evts.end() )
	{

	  const std::string ch_label = desc + ( ee->first == "." ? "" : " (" +  ee->first + ")" );

	  double y = yspacer + acnt * hgt;
	  
	  std::cout << "ctx1.fillStyle = '" << col << "';";  
	  std::cout << "ctx1.fillText(\"" << ch_label << "\", " << 10  << "  , " << y << ");";
	  
	  // plot actual intervals now
	  
	  std::map<interval_t,lw_annot_evt_t>::const_iterator ii = ee->second.begin();
	  while ( ii != ee->second.end() )
	    {
	      const interval_t & interval = ii->first;
	      const lw_annot_evt_t & evt = ii->second;
	      
	      // translate positions to fractional places on the map
	      
	      double sec1 = interval.start / (double)globals::tp_1sec;
	      double sec2 = interval.stop / (double)globals::tp_1sec;
	      
	      double x1 = sec1 / (double)tot_sec;
	      double x2 = sec2 / (double)tot_sec;
	      
	      double px1 = param.width * x1;
	      double px2 = param.width * x2;
	      
	      std::cout << "ctx1.fillRect( " << px1 << "," << y+5 << "," << px2-px1+1 << "," << 5 << ");";

	      
	      ++ii;
	    }
	  
	  
	  // next channel
	  ++ee;
	}
      
      // next annotation
      ++acnt;
      ++aa;
    }


  
        
  //
  // Allow mouse click to redirect to this (30-second) epoch in the signals plot 
  // i.e. this currently assumes we'll only display the annot plot in the signal plot view
  // if we change this, need to do as for stage_plot and let the caller finish off this last 
  // part of the canvas
  //
  
  std::cout << "function getMousePos(canvas, evt) { var rect = canvas.getBoundingClientRect(); return { x: evt.clientX - rect.left, y: evt.clientY - rect.top }; }";
  
  
  // note: this sets 'ed' to 4 which here means 30 second epochs
  
  std::cout << "canvas1.addEventListener( \"click\", function ( e ) { var mousePos = getMousePos(canvas1, e); "
	    << "window.location = \"lunaweb.cgi?m=sv"
	    << "&edf=" << param.edf_file 
	    << "&i=" << param.indiv 
	    << "&s=" << stringize( param.signal_set ) 
	    << "&p=" << param.folder 
	    << "&e=\" + Math.floor( (mousePos.x/" << param.width << ") * " << ne30 << ") + \"&ed="<< param.def_ed << "\"" ;  
  std::cout << "; }  , false ); "; 
  
  // end staging canvas
  std::cout << "}</script></canvas>";

}



// only plot annotations within this window

void lw_signals_t::annotplots( lw_annot_t & annots , lw_signal_disp_t & disp , const interval_t & window )
{

  
  
  // nb. tot_sec is total duration of all whole epochs, given the current epoch size
  // i.e. truncates anything at end, so that this plot lines up with other plots

  // number of channels
  const int na = annots.all();
  
  //  std::cout << "want to plot " << na << " annot " << tot_sec << "</p>";  
  
  const double hgt = 20;
  const double yspacer = 15;
  const double height = yspacer + hgt * na;

  
  // these must match signals.cpp signal plot
  const double px = param.width * 0.88;
  const double smain = param.width * 0.1; 
  
  std::cout << "<canvas id=\"a2Canvas\" height=\"" << height << "\" width=\"" << param.width << "\">"
	    << "<script>"
	    << "var canvas1 = document.getElementById('a2Canvas');"
	    << "if (canvas1.getContext) { "
	    << "var ctx1 = canvas1.getContext('2d');";
  
  std::cout << "ctx1.strokeStyle = 'purple';"
	    << "ctx1.lineWidth = '3';"
	    << "ctx1.strokeRect(0, 0, " << param.width << "," << height << ");";
  
  
  
  // for each annotation within the interval window
  // will not be dealing with massive numbres of events, so okay to use 
  // use a dummy method (i.e. we already will have plotted all annotations above anyway)

  int acnt = 0;
  std::map<lw_annot_key_t,lw_annot_evts_t>::const_iterator aa = annots.annots.begin();
  
  while ( aa != annots.annots.end() )
    {
      
      const std::string & label = aa->first.label;
      const std::string & desc = aa->first.desc;
      const std::string & col   = aa->first.col;
      
      // any channels?
      std::map<std::string,std::map<interval_t,lw_annot_evt_t> >::const_iterator ee = aa->second.evts.begin();
      while ( ee != aa->second.evts.end() )
	{

	  const std::string ch_label = desc + ( ee->first == "." ? "" : " (" +  ee->first + ")" );

	  double y = yspacer + acnt * hgt;
	  
	  std::cout << "ctx1.fillStyle = '" << col << "';";  
	  std::cout << "ctx1.fillText(\"" << ch_label << "\", " << 10  << "  , " << y << ");";
	  
	  // plot actual intervals that overlap
	  
	  std::map<interval_t,lw_annot_evt_t>::const_iterator ii = ee->second.begin();
	  while ( ii != ee->second.end() )
	    {
	      const interval_t & interval = ii->first;

	      if ( !interval.overlaps( window ) ) { ++ii; continue; } 
	      
	      const lw_annot_evt_t & evt = ii->second;
	      
	      // translate positions to fractional places on the map
	      
	      double sec1 = interval.start / (double)globals::tp_1sec;
	      double sec2 = interval.stop / (double)globals::tp_1sec;
	      	     
	      // truncate at edge of window
	      
	      double x1 = interval.start < window.start ? 0 : ( interval.start - window.start ) / (double) ( window.stop - window.start );	      
	      double x2 = interval.stop  > window.stop ? 1 : ( interval.stop - window.start ) / (double)( window.stop - window.start );
	      
	      double px1 = smain + px * x1;
	      double px2 = smain + px * x2;
	      
	      std::cout << "ctx1.fillRect( " << px1 << "," << y+5 << "," << px2-px1+1 << "," << 5 << ");";

	      
	      ++ii;
	    }
	  
	  
	  // next channel
	  ++ee;
	}
      
      // next annotation
      ++acnt;
      ++aa;
    }
  
  // done drawing
  std::cout << " } </script></canvas>";
  
}







bool lw_signals_t::fetch_annots( lw_annot_t * annots )
{
  
  
  std::string annot_list = param.folder + "/annots";

  if ( ! Helper::fileExists( annot_list ) ) return false;
  
  annots->annots.clear();
  
  int display_order = 0;

  std::ifstream IN1( annot_list.c_str() , std::ios::in );
  while ( ! IN1.eof() ) 
    {
      
      std::string line;

      std::getline( IN1, line );
      if ( IN1.eof() ) break;

      std::vector<std::string> tok = Helper::parse( line , "\t" );
      if ( tok.size() != 3 ) continue;
      
      const std::string & label= tok[0];
      const std::string & col = tok[1];
      const std::string & desc = tok[2];
      
      // this determines order of annotation display
      ++display_order; 
      
      // does this db exist?
      // name shold be ss/annot-${name}.db

      const std::string db = param.folder + "/" + param.indiv + "/ss/annot-" + label + ".db";
      
      
      
      if ( Helper::fileExists( db ) ) 
	{

	  // initiate a new, empty annotation class
	  // which may be for 0 or more specific channels
	  lw_annot_key_t key;
	  key.label = label;
	  key.desc  = desc;
	  key.col = col;
	  key.order = display_order;

	  lw_annot_evts_t events;
	  	 	  	  
	  // read data

	  sstore_t ss( db );
	  
	  std::map<interval_t,sstore_data_t> intervals = ss.fetch_intervals(); 
	  
	  //	  std::cout << "<p>" <<  intervals.size() << "<em> intervals</em></p>";

	  std::map<interval_t,sstore_data_t>::const_iterator ii = intervals.begin();
	  
	  while ( ii != intervals.end() )
	    {
	      
	      const std::map<sstore_key_t,sstore_value_t> & datum   = ii->second.data;
	      
	      std::map<sstore_key_t,sstore_value_t>::const_iterator kk = datum.begin();
	      while ( kk != datum.end() )
		{
		  
		  const interval_t & interval = ii->first;
		  const std::string   ch = kk->first.ch == "" ? "." : kk->first.ch;
		  const std::string & var = kk->first.id;
		  // ignore lvl here
		  
		  // only take single txt or numeric 
		  std::string value;
		  if      ( kk->second.is_text ) value = kk->second.str_value ;
		  else if ( kk->second.is_double ) value = Helper::dbl2str( kk->second.dbl_value );

		  
		  lw_annot_evt_t event;
		  
		  event.attrib[ var ] = value; 
		  
		  events.evts[ ch ][ interval ] = event;
		  
		  
		  // next interval/value
		  ++kk;

		}

	      
	      // next interval
	      ++ii;
	    }

	  
	  // all done reading db, now store
	  annots->annots[ key ] = events;
	  
	}

      // next line in annot_list file
      
    }

  IN1.close();


  // did we load anything?
  return annots->annots.size() > 0 ;

}
