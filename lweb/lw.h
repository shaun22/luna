
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#ifndef __LWEB_H__
#define __LWEB_H__

#include <string>
#include <set>
#include <map>
#include <vector>
#include <iostream>

#include "edf/edf.h"
#include "clocs/topo.h"
#include "sstore/sstore.h"

struct lw_indiv_t { 
  std::string id;
  std::string edf;
  std::set<std::string> annots;
  std::set<std::string> outputs;
  std::map<std::string,std::string> metainf;
};


struct lw_project_t { 
  
  int populate( const std::string & ) ;
  void tabulate();  
  std::vector<lw_indiv_t> indivs;
  
};


struct lw_signal_disp_t { 
  lw_signal_disp_t( const std::string & f , edf_t * );
  std::string col( const std::string & s ) const; 
  int n_signals() const { return ord.size(); } 
  int size() const { return ord.size(); } 
  std::string order( const int i ) const;
  std::string display_label( const std::string & ) const;
  
  // display order
  std::vector<std::string> ord;

  // display colours
  std::map<std::string,std::string> cols;

  // type of signal
  std::vector<std::string> types;
  
  // display labels
  std::map<std::string,std::string> disp;

  // channel location information
  clocs_t clocs;  // 3D map
  topo_t topo;    // 2D topoplot 

  // staging channels
  std::string eeg, eog, emg;

};

struct lw_headers_t { 
  lw_headers_t( const std::string & , const std::string & );
  void tabulate();  
  edf_t * edf;
  ~lw_headers_t();
};


struct lw_hypnogram_summary_t { 
  lw_hypnogram_summary_t( const std::string & , const std::string & );
  void tabulate();  
  edf_t * edf;
  sstore_data_t data;
  ~lw_hypnogram_summary_t();

  double  LIGHTS_OFF,SLP_ONSET ,SLP_MIDPOINT ,FINAL_WAKE ,LIGHTS_ON  ,
    TIB, TST, TPST, TWT, WASO, 
    MINS_N1, MINS_N2, MINS_N3, MINS_N4,  MINS_REM,     
    PCT_N1, PCT_N2, PCT_N3,  PCT_N4,  PCT_REM, 
    NREMC,  NREMC_MINS,     
    SLP_LAT,  SLP_EFF,  SLP_EFF2,  SLP_MAIN_EFF, REM_LAT, PER_SLP_LAT ; 

};

struct lw_coh_pair_t { 
  lw_coh_pair_t() { } 
  lw_coh_pair_t( const std::string & ch1 , const std::string & ch2 ) 
  : ch1(ch1) , ch2(ch2) 
  { 
    order();
  } 
  
  lw_coh_pair_t( const std::string & ch ) 
  {
    size_t p = ch.find( "_x_" );
    if ( p == std::string::npos ) 
      Helper::halt( "expecting channel-pair string in format C1_x_C2" );      
    ch1 = ch.substr(0,p);
    ch2 = ch.substr(p+3); 
    order();
  }

  void order() 
  {
    if ( ch2 < ch1 ) 
      {
	std::string tmp = ch1;
	ch1 = ch2;
	ch2 = tmp;
      }
  }

  std::string ch1;
  std::string ch2;
  bool operator<( const lw_coh_pair_t & rhs ) const 
  {
    if ( ch1 == rhs.ch1 ) return ch2 < rhs.ch2;
    return ch1 < rhs.ch1;
  }

  bool has( const std::string & ch ) const
  { return ch == ch1 || ch == ch2; } 
  
};

struct lw_coh_t { 
  lw_coh_t();
  void coh_plot( const std::string & band );
  void coh_topo_plot( const std::string & band , lw_signal_disp_t & , const std::string & ch );
  // chs -> band -> coh
  std::map<lw_coh_pair_t,std::map<std::string,double> > coh;
  // ch -> band -> ch2 -> coh (as above, but easier to key by channel
  std::map<std::string,std::map<std::string,std::map<std::string,double> > > chcoh;
  // all channels
  std::set<std::string> chs;  
};

struct lw_exe_t { 
  static void display_exe_list();
  lw_exe_t(); 
  edf_t * edf;
  ~lw_exe_t();
  std::map<int,std::set<int> > exe;  // exemplar --> 
  void display();           // display for all cluseters (exemplars only, for each global and single plot)
  void display(const int);  // display for a single cluster (one global plot, many single_epoch plots for exemplar and members)
  void single_epoch_plot( const int );
  void global_epoch_plot( const std::set<int> & , const int ex , const int ne30, const std::vector<std::string> * s = NULL , 
			  const std::vector<bool> * m = NULL );
  
};

struct lw_annot_key_t { 
  std::string label;
  std::string desc;
  std::string col;
  int order;
  bool operator<( const lw_annot_key_t & rhs ) const 
  {
    return order < rhs.order;  // i.e. allow position in 'annots' file to determine display order
    //    if ( label == rhs.label ) return ch < rhs.ch;
    //return label < rhs.label;
  }
};

// key/value attribute pairs for an event
struct lw_annot_evt_t {
  std::map<std::string,std::string> attrib;
};


struct lw_annot_evts_t {
  // ch --> interval/event --> info
  // if ch==. means not channel specific
  std::map<std::string,std::map<interval_t,lw_annot_evt_t> > evts;
};


struct lw_annot_t { 
  int all() const { 
    int na = 0;
    std::map<lw_annot_key_t,lw_annot_evts_t>::const_iterator aa = annots.begin();
    while ( aa != annots.end() )
      {
	na += aa->second.evts.size();
	++aa;
      }
    return na;
  }

  std::map<lw_annot_key_t,lw_annot_evts_t> annots;
};


struct lw_signals_t { 

 
  lw_signals_t( const std::string & , const std::string & );
  
  void topoplots();
  
  void psdplots();

  void activityplots(   const signal_list_t & signals,     const lw_signal_disp_t & disp , const interval_t & interval , mslice_t * );

  void annotplots( lw_annot_t & , lw_signal_disp_t & , int , int );

  void annotplots( lw_annot_t & annots , lw_signal_disp_t & disp , const interval_t & interval );

  static bool fetch_stage( const std::string & stage_db , std::vector<std::string> *  );

  static bool fetch_mask( const std::string & mask_db , std::vector<bool> *  );
  
  bool fetch_annots( lw_annot_t * ); 
  
  void hjorth_plot( const std::string & hjorth_db , 
		    const int ne30 , 
		    const std::vector<bool> * wake30 , 
		    const std::vector<bool> * mask30 );
    
  static void stage_plot( const std::string & hjorth_db , 
			  const int ne , 
			  const int ne30 , 
			  const int current_eppch , 
			  std::vector<std::string> * stages30 , 
			  std::vector<bool> * wake30 , 
			  const std::vector<bool> * mask30 ,
			  const std::map<int,int> * cycles = NULL );
  
  
  void epoch( );
  edf_t * edf;
  std::vector<double> epoch_duration_sec; 
  ~lw_signals_t();
};

struct lw_outdbs_t { 
  lw_outdbs_t( const std::string & );
  std::vector<std::string> db;
  void tabulate();
};

struct lw_outdb_t { 
  lw_outdb_t( const std::string & );
  void dump( const std::string & );
};


struct lw_topo_t { 
  
  // topo map
  
};



#endif
