
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#ifndef __LW_TOPO_H__
#define __LW_TOPO_H__

#include <vector>

struct xy_t { 
  void add( double _x, double _y) { x.push_back(_x); y.push_back(_y); } 
  int size() const { return x.size() ;} 
  std::vector<double> x, y;
};

void lweb_make_topo( Data::Matrix<double> & I , const std::string & label , const std::string & canvas , int px , int py , double * pzmin = NULL , double * pzmax = NULL , xy_t * xy = NULL );

// histogram of original channel values
void lweb_make_hist( const std::string & ,  int px , int py , const std::map<std::string,double> & z , const lw_signal_disp_t & );


#endif
