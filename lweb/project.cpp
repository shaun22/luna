
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#include "lw.h"
#include "web.h"
#include "param.h"

#include "helper/helper.h"

#include <fstream>

extern lw_param_t  param;
  
int lw_project_t::populate( const std::string & fileroot ) 
{

  indivs.clear();

  std::string project_file = fileroot + "/samples";

  if ( ! Helper::fileExists( project_file ) )
    Helper::halt( "could not find sample list " + project_file );

  std::ifstream O1( project_file.c_str() , std::ios::in );
  
  while ( ! O1.eof() ) 
    {
      std::string line;
      std::getline( O1 , line );      
      if ( line == "" || O1.eof() ) break;
      std::vector<std::string> tok = Helper::parse( line );
      if ( tok.size() < 2 ) Helper::halt( "improperly formatted " + project_file );
      lw_indiv_t indiv;
      indiv.id = tok[0];
      indiv.edf = tok[1];
      indivs.push_back( indiv );
    }

  O1.close();

  return indivs.size();
  
}
  

void lw_project_t::tabulate( ) 
{
  

  std::cout << "<p><em>Sample list for " << indivs.size() << " individuals</em></p>";
  
  std::cout << "<table border=0 width=100%>";
  // header
  std::cout << "<tr><th>ID</th><th>EDF</th>"
	    << "<th>Staging</th>"
	    << "<th>Signals</th><th>Power spectra</th><th>Topographic maps</th>"
	    << "<th>ExE</th>"
	    << "<th>Coherence</th>"
	    << "<th>Outputs</th><th>Meta-information</th></tr>";
  

  for (int i=0;i<indivs.size();i++)
    {
      // for output_link()
      param.edf_file = indivs[i].edf ;
      
      // row
      std::cout << "<tr>"
		<< "<td>" << indivs[i].id << "</td>"
		<< "<td>" << web_t::headers_link( "EDF header" , param.folder , indivs[i].edf , indivs[i].id ) << "</td>"
		<< "<td>" << web_t::staging_link( "Staging" , param.folder , indivs[i].edf , indivs[i].id ) << "</td>"
		<< "<td>" << web_t::signals_link( "View signals" , param.folder , indivs[i].edf , indivs[i].id ) << "</td>"
		<< "<td>" << web_t::psd_link( "View PSD" , param.folder , indivs[i].edf , indivs[i].id ) << "</td>"
		<< "<td>" << web_t::topo_link( "View topoplot" , param.folder , indivs[i].edf , indivs[i].id ) << "</td>"
		<< "<td>" << web_t::exe_link( "ExE" , param.folder , indivs[i].edf , indivs[i].id ) << "</td>"
		<< "<td>" << web_t::coh_link( "COH" , param.folder , indivs[i].edf , indivs[i].id ) << "</td>"
		<< "<td>" << web_t::outputs_link( "Output(s)" , param.folder , indivs[i].id ) 
 		<< "<td>" << "." << "</td>"		
		<< "</tr>";
      
    }
  std::cout << "</table>";
  

}

