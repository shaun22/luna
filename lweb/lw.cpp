
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#include "param.h"
#include "web.h"
#include "lw.h"

// luna library headers
#include "defs/defs.h"
#include "db/db.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

lw_param_t  param;


//
// from Luna library
//

globals global;


// hmm, a few globals from the luna libraries need to be defined
// todo: clean this up in future...
writer_t writer;
std::map<std::string,std::string>  cmd_t::label_aliases;
std::map<std::string,std::vector<std::string> >  cmd_t::primary_alias;


int main(int argc , char ** argv )
{

  //
  // always need to init luna globals
  //

  global.init_defs();


  //
  // web-friendly exception handler for Helper::halt()
  //

  globals::bail_function = &web_bail_function;
  
  globals::silent = true;
  

  //
  // Output header
  //

  web_t::write_header();


  //
  // Parse input
  //

  web_t::parse_cgi();      


  //
  // Any user-specified header/title
  //

  web_t::user_header( param.folder + "/header" );

  //
  // Do something
  //
  
  std::cout << "<p><em>Project :</em> <b>" << web_t::project_link( param.folder , param.folder ) << "</b></p>";


  //
  // Project view
  //

  if ( param.mode == "pv" ) 
    {

      lw_project_t project;
      
      int n = project.populate( param.folder ); 
  
      project.tabulate();
      
    }


  //
  // Header view
  //

  if ( param.mode == "hv" ) 
    {
      
      lw_headers_t headers( param.edf_file , param.indiv );
      
      headers.tabulate();
      
    }


  //
  // Hypnogram summary view
  //

  if ( param.mode == "summv" ) 
    {
      
      lw_hypnogram_summary_t hypno( param.edf_file , param.indiv );
      
      hypno.tabulate();
      
    }


  //
  // Signal view
  //

  if ( param.mode == "sv" ) 
    {
      
      lw_signals_t signals( param.edf_file , param.indiv );
      
      signals.epoch( );
      
    }


  //
  // Coherence view
  //

  if ( param.mode == "cv" )
    {
      lw_coh_t coh;
    }


  //
  // ExE view
  //

  if ( param.mode == "ev" )
    {
      // get and display list file
      lw_exe_t::display_exe_list();
      
      // any specific plot?
      lw_exe_t exe;
      
    }
  
  //
  // Topo view
  //

  if ( param.mode == "tv" ) 
    {
      
      lw_signals_t signals( param.edf_file , param.indiv );
      
      signals.topoplots( );
    }

  //
  // PSD view
  //

  if ( param.mode == "psdv" )
    {
      lw_signals_t signals( param.edf_file , param.indiv );
      
      signals.psdplots();
    }


  //
  // Output view
  //
  
  if ( param.mode == "ov" )
    {

      //
      // either list all available outdbs
      // 

      if ( param.outdb == "." ) 
	{
	  lw_outdbs_t outdbs( param.folder );
	}
      else // or we are looking for a particular one 
	{

	  std::string filename = param.folder + "/" + param.indiv + "/ss/" + param.outdb + ".db";
	  
	  lw_outdb_t outdb( filename );
	  
	}
      
    } 
  


  //
  // All done
  //
  
  web_t::write_footer();

  
  exit(0);
  
}



//
// 'signals' file
//


std::string lw_signal_disp_t::display_label( const std::string & s ) const
{
  std::map<std::string,std::string>::const_iterator ss = disp.find( s );
  if ( ss == disp.end() ) return s;
  return ss->second;
}


lw_signal_disp_t::lw_signal_disp_t( const std::string & f , edf_t * edf )
{
  
  
  
  // requires header row, all tab-delimited
  // EDF     TYPE    DISP    STAGING COL     TH      R       X       Y       Z

  // label     type      col       x      y      z

  
  std::string fname = f + "/signals";
  if ( ! Helper::fileExists( fname ) ) return;
  
  
  std::ifstream O1( fname.c_str() , std::ios::in );
  for (int t=0;t<10;t++)
    {
      std::string dummy;
      O1 >> dummy;
      if ( O1.eof() ) Helper::halt( "bad format for signals file" );
      if ( t == 0 && dummy != "EDF" ) Helper::halt( "bad format for signals file" );
      if ( t == 1 && dummy != "TYPE" ) Helper::halt( "bad format for signals file" );
      if ( t == 2 && dummy != "DISP" ) Helper::halt( "bad format for signals file" );
      if ( t == 3 && dummy != "STAGING" ) Helper::halt( "bad format for signals file" );
      if ( t == 4 && dummy != "COL" ) Helper::halt( "bad format for signals file" );
      if ( t == 5 && dummy != "TH" ) Helper::halt( "bad format for signals file" );
      if ( t == 6 && dummy != "R" ) Helper::halt( "bad format for signals file" );
      if ( t == 7 && dummy != "X" ) Helper::halt( "bad format for signals file" );
      if ( t == 8 && dummy != "Y" ) Helper::halt( "bad format for signals file" );
      if ( t == 9 && dummy != "Z" ) Helper::halt( "bad format for signals file" );      
    }

  
  cols.clear(); ord.clear();
  
  while ( ! O1.eof() ) 
    {
      std::string line;
      std::getline( O1, line );
      if ( O1.eof() ) break;
      std::vector<std::string> tok = Helper::parse( line , "\t" );
      if ( tok.size() != 10 ) continue;
  
      
    
      const std::string & label   = tok[0];

      // check actually exists in the EDF
      if ( edf->header.label_all.find( label ) == edf->header.label_all.end() ) continue;

      const std::string & type    = tok[1];
      const std::string & display = tok[2];
      const std::string & staging = tok[3];
      const std::string & c       = tok[4];
      const std::string & thloc   = tok[5];
      const std::string & rloc    = tok[6];
      const std::string & xloc    = tok[7];
      const std::string & yloc    = tok[8];
      const std::string & zloc    = tok[9];
      
      ord.push_back( label );
      
      cols[ label ] = c == "." ? "gray" : c ;

      types.push_back( type );

      disp[ label ] = display;

      
      // 3D location
      bool gotloc = true;
      double x, y , z ;
      if ( ! Helper::str2dbl( xloc , &x ) ) gotloc = false;
      if ( ! Helper::str2dbl( yloc , &y ) ) gotloc = false;
      if ( ! Helper::str2dbl( zloc , &z ) ) gotloc = false;
      if ( gotloc ) clocs.add_cart( label , x , y , z );
      
      // 2D topography (polar co-ords)
      gotloc = true;
      double th, radius;
      if ( ! Helper::str2dbl( thloc , &th ) ) gotloc = false;
      if ( ! Helper::str2dbl( rloc , &radius ) ) gotloc = false;
      if ( gotloc ) topo.add( label , topoloc_t( th , radius ) );

      // set staging info?  EEG, EOG and EMG
      if ( staging == "EEG" ) eeg = label;
      if ( staging == "EOG" ) eog = label;
      if ( staging == "EMG" ) emg = label;

      
    }
  O1.close();
}



std::string lw_signal_disp_t::col( const std::string & s ) const
{
  std::map<std::string,std::string>::const_iterator ii = cols.find( s );
  if ( ii == cols.end() ) return "gray";
  return ii->second;
}

std::string lw_signal_disp_t::order( const int i ) const
{
  if ( i < 0 || i >= ord.size() ) return "";
  return ord[i];
}


