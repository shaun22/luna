
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#ifndef __LWEB_MISC_H_
#define __LWEB_MISC_H_

#include <string>
#include <set>
#include "../helper/helper.h"

std::set<std::string> strset( const std::string & k , const std::string & delim = "," );
std::string stringize( const std::set<std::string> & s , const std::string & delim = "," );
std::string stringize_except( const std::set<std::string> & s , const std::string & ex, const std::string & delim = "," );
std::string stringize_add( const std::set<std::string> & s , const std::string & ex, const std::string & delim = "," );


#endif
