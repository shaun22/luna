
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "lw.h"
#include "web.h"
#include "param.h"
#include "misc.h"
#include "../sstore/sstore.h"
#include "../fftw/fftwrap.h"

extern lw_param_t  param;

void make_psd( const std::string & canvas , int px , int py , const std::vector<double> & p , const std::string & label );

void lw_signals_t::psdplots( )
{

  // either per 30-epoch or per stage

  bool is_epoch = param.e != -1 ;
  
  int epoch = param.e;
  

  //
  // display information, and channel locations
  //
  
  lw_signal_disp_t disp( param.folder , edf );

  

  //
  // Some basic duration/epoch information
  //
  
  // total EDF duration in seconds (may be fractional)
  double total_duration_sec = edf->header.nr * edf->header.record_duration; 
  
  // number of (whole) 30-second epochs
  int ne30 = edf->timeline.set_epoch( param.esize , param.esize  );
  
  
  //
  // Header
  //
  
  std::cout << "<hr><small>";

  if ( is_epoch )
    {

      
      //
      // Get time interval for this epoch
      //
      
      // first sanity check (i.e. w/ zooming in/out, sometimes epoch may end up being larger than ne)
      
      if ( epoch >= ne30 ) epoch = ne30-1;
      
      interval_t interval = edf->timeline.epoch( epoch );  
  
      
      //
      // Header
      //

      std::cout << "<p>";
      
      std::cout << "[" << web_t::psd_link( "<em>start</em>" , param.folder , param.edf_file , param.indiv , param.signals , 0  ) << "]"; 
      
      if ( epoch > 1 ) 
	std::cout << "[" << web_t::psd_link( "<em>prev</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch - 1 ) << "]"; 
      
      
      std::cout << " <b>epoch " << epoch+1 << "</b> of " << ne30 << " ";

      // next epoch button
      if ( epoch + 1 < ne30 ) 
	std::cout << "[" << web_t::psd_link( "<em>next</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch + 1 , "" ) << "]"; 
      
      std::cout << "[" << web_t::psd_link( "<em>end</em>" , param.folder , param.edf_file , param.indiv , param.signals , ne30-1 , "" ) << "]"; 
      
      std::cout << "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";


      //
      // Duration
      //
      
      uint64_t duration_tp = globals::tp_1sec * (uint64_t)edf->header.nr * edf->header.record_duration ;
      std::cout << " | <em>duration<em>: <b>" <<   Helper::timestring( duration_tp ) << "</b>";
      
      
      //
      // Clocktime and/or elapsed time
      //
      
      double hours1 = interval.start_sec() / 3600.0;
      double hours2 = interval.stop_sec() / 3600.0;
      clocktime_t zerotime1, zerotime2;
      zerotime1.advance( hours1 );
      zerotime2.advance( hours2 );

      clocktime_t starttime( edf->header.starttime );
      if ( starttime.valid ) 
	{
	  starttime.advance( hours1 );
	  std::cout << " | <em>clock</em>: <b>" << starttime.as_string() << "</b>";
	}
      
      std::cout << " | <em>elapsed:</em> <b>" << zerotime1.as_string() << " .. " << zerotime2.as_string() << "</b>";
      
      
    }
  else  // ... othewise STAGE level data
    {
      std::cout << "<p>";

      if ( param.stage == "NREM1" ) std::cout << " <b>NREM1</b> "; 
      else std::cout << " [" << web_t::psd_link( "<em>NREM1</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , "NREM1" ) << "]" ; 

      if ( param.stage == "NREM2" ) std::cout << " <b>NREM2</b> "; 
      else std::cout << "[" << web_t::psd_link( "<em>NREM2</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , "NREM2" ) << "] ";

      if ( param.stage == "NREM3" ) std::cout << " <b>NREM3</b> "; 
      else std::cout << " [" << web_t::psd_link( "<em>NREM3</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , "NREM3" ) << "] ";

      if ( param.stage == "REM" ) std::cout << " <b>REM</b> ";
      else std::cout << " [" << web_t::psd_link( "<em>REM</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , "REM" ) << "] "; 
 
    }
  


  //
  // Signals
  //

  signal_list_t signals;


  // if param.signals is empty, this means to look at all signals
  
  if ( param.signals == "." ) 
    { 
      param.signal_set.clear();
      for (int o=0; o < disp.size(); o++ ) 
	{
	  signals.add( o , disp.order(o) );
	  param.signal_set.insert( disp.order(o) );	
	}
    }
  else
    {
      
      //
      // create a signal-string, in the correct order
      //
      
      std::string ordered_signal_string = "";
      bool firstdone = false;
      for (int o=0; o < disp.size(); o++ ) 
	{
	  if ( param.signal_set.find( disp.order(o) ) == param.signal_set.end() ) { continue; }
	  if ( firstdone ) ordered_signal_string += ",";
	  ordered_signal_string += "," + disp.order( o );       
	  firstdone = true;
	}
      
      signals = edf->header.signal_list( ordered_signal_string );
      
    }


  param.signals = stringize( param.signal_set );
  

  //
  // restrict to EEG? 
  //

  // old code: from topoplots() restricting to channels w/ topo-info
  //   const int ntot = disp.topo.size();
  //   std::set<std::string> channels = disp.topo.channels();  
  //   std::set<std::string> sigs2 = param.signal_set;
  //   param.signal_set.clear();
  //   std::set<std::string>::const_iterator ss = sigs2.begin();
  //   while ( ss != sigs2.end() )
  //     {
  //       if ( channels.find( *ss ) != channels.end() ) param.signal_set.insert( *ss );
  //       ++ss;
  //     }
  //   param.signals = stringize( param.signal_set );
  

  //
  // link to signal view (ed = 4 --> 30 second epochs
  //

  std::cout << "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
  
  std::cout << "[" << web_t::signals_link( "<em>signals</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , 4 ) << "] ";

  std::cout << " [" << web_t::topo_link( "<em>topoplot</em>" , param.folder , param.edf_file , param.indiv , param.signals , epoch , param.stage  ) << "]";

  //
  // print options
  //

  std::cout << "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
  
  std::set<std::string> excls;
  for (int o=0; o < disp.size(); o++ ) 
    {
      if ( param.signal_set.find( disp.order(o) ) != param.signal_set.end() ) { continue; } // already in
      //if ( channels.find( disp.order(o) ) == channels.end() ) { continue; }   // no topo info
      excls.insert( disp.order(o) );
    }

  std::cout << "<em><b>signals</b> :</em> [" 
	    << web_t::psd_link( "<em>all</em>" , param.folder , param.edf_file , param.indiv , "." , epoch , param.stage )
	    << "] ["
	    << web_t::psd_link( "<em>none</em>" , param.folder , param.edf_file , param.indiv , "" , epoch , param.stage )
	    << "] ["
	    << web_t::psd_link( "<em>swap</em>" , param.folder , param.edf_file , param.indiv , stringize( excls ) , epoch , param.stage )
	    << "]";
  
  std::cout << "</p><hr>";



  
  std::cout << "<p><em>Incl:</em>";
  std::set<std::string>::const_iterator ii = param.signal_set.begin();
  while ( ii != param.signal_set.end() )
    {      
      std::cout << " [" << web_t::psd_link( disp.display_label( *ii ) , param.folder , param.edf_file , param.indiv , stringize_except( param.signal_set , *ii ) , epoch , param.stage ) << "] " ;
      ++ii;
    }


  
  std::cout << "</p><p><em>Excl:</em>";


  ii = excls.begin();
  while ( ii != excls.end() )
    {
      std::cout << " [" << web_t::psd_link( disp.display_label( *ii ) , param.folder , param.edf_file , param.indiv , stringize_add( param.signal_set , *ii ) , epoch , param.stage ) << "] " ;
      ++ii;
    }

  
  //
  // End of header
  //
    
  std::cout << "</p></small><hr>";



  //
  // Mask information?
  //

  std::string mask_db = param.folder + "/" + param.indiv + "/ss/mask.db";
  
  bool has_mask = Helper::fileExists( mask_db );

  // record all 30-second epoch masks
  std::vector<bool> mask30( ne30 , false ) ;
  
  if ( has_mask )
    {
      fetch_mask( mask_db , &mask30 );
    }


  //
  // Stage plot
  //
  

  std::string staging_db = param.folder + "/" + param.indiv + "/ss/staging.db";
  
  bool has_staging = Helper::fileExists( staging_db );

  // record all 30-second epoch stages    

  if ( has_staging )
    {

      // always 30-second epochs here
      stage_plot( staging_db , ne30, ne30 , epoch , NULL , NULL , has_mask ? &mask30 : NULL );

      
      //
      // Allow mouse click to redirect to this (30-second) epoch
      //
      
      std::cout << "function getMousePos(canvas, evt) { var rect = canvas.getBoundingClientRect(); return { x: evt.clientX - rect.left, y: evt.clientY - rect.top }; }";
										   

      // note: this sets 'ed' to 4 which here means 30 second epochs
      
      std::cout << "canvas1.addEventListener( \"click\", function ( e ) { var mousePos = getMousePos(canvas1, e); "
		<< "window.location = \"lunaweb.cgi?m=psdv"
		<< "&edf=" << param.edf_file 
		<< "&i=" << param.indiv 
		<< "&s=" << stringize( param.signal_set ) 
		<< "&p=" << param.folder 
		<< "&e=\" + Math.floor( (mousePos.x/" << param.width << ") * " << ne30 << ") + \"&ed="<< param.def_ed << "\"" ;
      
      std::cout << "; }  , false ); "; 

      // end staging canvas
      std::cout << "}</script></canvas>";
      
    }
  




  
  //
  // Get data from psd sstore_t
  //

  const int ns = signals.size();


  
  std::string band_db = param.folder + "/" + param.indiv + "/ss/psd-epoch.db";
  
  bool has_band = Helper::fileExists( band_db );

  // channel, vector of freqs
  std::map<std::string,std::vector<double> > psd;
  
  if ( is_epoch )
    {
      
      sstore_t ss( band_db );
      
      // epochs stored as 1-based ints
      sstore_data_t data = ss.fetch_epoch( epoch + 1);
      
      std::map<sstore_key_t,sstore_value_t>::const_iterator kk = data.data.begin();
      while ( kk != data.data.end() )
	{
	  
	  // include this signal? 	  
	  if ( param.signal_set.find( kk->first.ch ) == param.signal_set.end() ) { ++kk; continue; }
	  
	  psd[ kk->first.ch ] = kk->second.vec_value;	  
	  
	  ++kk;

	}
      
    }

  //
  // Noramlize across all channels?
  //
  
  
  //
  // Channel-specific histograms
  //
  
  // as channel labels may have '-' or other illegal symbols, use a numeric scheme here
  
  // assume 1..20 in 9,5

  int cnt = 0;
  std::map<std::string,std::vector<double> >::const_iterator jj = psd.begin();
  while ( jj != psd.end() )
    {	  
      std::string label = disp.display_label( jj->first );
      make_psd( "p_" + Helper::int2str( cnt++ ) , 150 , 150 , jj->second , label );
      ++jj;
    }
  
  
}


void make_psd( const std::string & canvas , int px , int py , const std::vector<double> & p , const std::string & label )
{


  const double xspacer = 25;
  const double width = px + 2 * xspacer;
  const double xlabel = 15;
  px -= xlabel;
  
  const double yspacer = 10;
  const double height = py + yspacer;
  const double ylabel = 15;
  py -= ylabel;
  
  // skip 0Hz bin
  const int np = p.size() - 1;

  if ( np != 40 ) 
    Helper::halt( "expecting different spectra size (i.e. 0..20 Hz in 0.5 Hz intervals)" );

  double zmin = p[1];
  double zmax = p[1];

  for (int j=2;j<p.size();j++)
    {
      if ( p[j] < zmin ) zmin = p[j];
      if ( p[j] > zmax ) zmax = p[j];
    }


  
  double xinc = px / (double)(np+1);
  
  std::cout << "<canvas id=\"" << canvas << "\" width=\"" << width << "\" height=\"" << height << "\">"
 	    << "<script>"
 	    << " var ctx_"<< canvas << " = document.getElementById('" << canvas << "').getContext('2d');";
  
  // font
  std::cout << "ctx_" << canvas << ".font = \"12px Arial\";";
  
  // scale


  
  std::cout << "ctx_" << canvas << ".beginPath();";      
  double x = xspacer + xlabel + 1 * xinc;;
  double y = yspacer + ylabel + py * ( 1 - ( p[1] - zmin ) / ( zmax - zmin ) );

  std::cout << "ctx_" << canvas << ".moveTo(" << x << " , " << y << ");";
    
  for (int j=2;j<=np;j++)
    {
      double x = xspacer + xlabel + j * xinc;
      double y = yspacer + py * ( 1 - ( p[j] - zmin ) / ( zmax - zmin ) ) ; // label at bottom
      std::cout << "ctx_" << canvas << ".lineTo(" << x << " , " << y  << ");";
    }

  std::cout << "ctx_" << canvas << ".strokeStyle = 'purple';"
	    << "ctx_" << canvas << ".lineWidth = 2;";  
  


  std::cout << "ctx_" << canvas << ".stroke();";  
  std::cout << "ctx_" << canvas << ".closePath();";

  // title

  std::cout << "ctx_" << canvas << ".fillStyle = 'black';";  
  std::cout << "ctx_" << canvas << ".fillText(\"" << label << "\", " << xspacer + xlabel + px*0.8  << "  , " << yspacer + ylabel  << ");";
  
  // axis

  std::cout << "ctx_" << canvas << ".beginPath();";      
  std::cout << "ctx_" << canvas << ".moveTo(" << xspacer + xlabel << " , " << yspacer << ");";
  std::cout << "ctx_" << canvas << ".lineTo(" << xspacer + xlabel << " , " << yspacer + py << ");";
  std::cout << "ctx_" << canvas << ".lineTo(" << xspacer + xlabel + px << " , " << yspacer  + py << ");";

   std::cout << "ctx_" << canvas << ".strokeStyle = 'blue';"
 	    << "ctx_" << canvas << ".lineWidth = 2;" ;


  std::cout << "ctx_" << canvas << ".stroke();";
  std::cout << "ctx_" << canvas << ".closePath();";

  std::cout << "ctx_" << canvas << ".fillStyle = 'black';";  

  std::cout << "ctx_" << canvas << ".fillText(\"5Hz\"," << xspacer + xlabel + 10  * xinc - 10<< "  , " << yspacer + py + ylabel  << ");";
  std::cout << "ctx_" << canvas << ".fillText(\"10Hz\"," << xspacer + xlabel + 20  * xinc - 10 << "  , " << yspacer + py + ylabel  << ");";
  std::cout << "ctx_" << canvas << ".fillText(\"15Hz\"," << xspacer + xlabel + 30  * xinc -10 << "  , " << yspacer + py + ylabel  << ");";
  std::cout << "ctx_" << canvas << ".fillText(\"20Hz\"," << xspacer + xlabel + 40  * xinc -10 << "  , " << yspacer + py + ylabel  << ");";

  // grid line at 5,10,15,20

  std::cout << "ctx_" << canvas << ".setLineDash([5, 3]);"
	    << "ctx_" << canvas << ".lineWidth = 1;" 
	    << "ctx_" << canvas << ".strokeStyle = 'gray';";

  // 5Hz
  std::cout << "ctx_" << canvas << ".beginPath();";      
  std::cout << "ctx_" << canvas << ".moveTo(" << xspacer + xlabel + 10  * xinc << " , " << yspacer << ");";
  std::cout << "ctx_" << canvas << ".lineTo(" << xspacer + xlabel + 10  * xinc << " , " << yspacer + py << ");";  
  std::cout << "ctx_" << canvas << ".stroke();";
  std::cout << "ctx_" << canvas << ".closePath();";

  // 10Hz
  std::cout << "ctx_" << canvas << ".beginPath();";      
  std::cout << "ctx_" << canvas << ".moveTo(" << xspacer + xlabel + 20  * xinc << " , " << yspacer << ");";
  std::cout << "ctx_" << canvas << ".lineTo(" << xspacer + xlabel + 20  * xinc << " , " << yspacer + py << ");";  
  std::cout << "ctx_" << canvas << ".stroke();";
  std::cout << "ctx_" << canvas << ".closePath();";

  // 15Hz
  std::cout << "ctx_" << canvas << ".beginPath();";      
  std::cout << "ctx_" << canvas << ".moveTo(" << xspacer + xlabel + 30  * xinc << " , " << yspacer << ");";
  std::cout << "ctx_" << canvas << ".lineTo(" << xspacer + xlabel + 30  * xinc << " , " << yspacer + py << ");";  
  std::cout << "ctx_" << canvas << ".stroke();";
  std::cout << "ctx_" << canvas << ".closePath();";

  // 20Hz
  std::cout << "ctx_" << canvas << ".beginPath();";      
  std::cout << "ctx_" << canvas << ".moveTo(" << xspacer + xlabel + 40  * xinc << " , " << yspacer << ");";
  std::cout << "ctx_" << canvas << ".lineTo(" << xspacer + xlabel + 40  * xinc << " , " << yspacer + py << ");";  
  std::cout << "ctx_" << canvas << ".stroke();";
  std::cout << "ctx_" << canvas << ".closePath();";
  
   // all done
  std::cout << "</script></canvas>";
  
}

