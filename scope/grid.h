
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#ifndef __GRID_H__
#define __GRID_H__

#include <wx/wx.h>

#include "wx/spinbutt.h"

#include <vector>
#include <string>
#include <set>
#include <map>

#include <stdint.h>

#include "../intervals/intervals.h"

class Canvas : public wxWindow 
{ 

 public:

  Canvas( wxPanel * parent );
  void OnChar( wxKeyEvent & event );
  void OnPaint(wxPaintEvent & evt);
  void OnMouseEvent( wxMouseEvent & evt );
  void EmptyBackground(wxEraseEvent & evt);
  void Background(wxDC & dc);
  void render(wxDC& dc);
  
  wxPoint last;
  
  static std::vector<wxPen> inks;
  
  static std::vector<int> zoom_levels;
  static std::vector<std::string> zoom_labels;
  static std::vector<int> downsampling;
  static const int MAX_ZOOM_LEVELS = 12;
  static const int xborder  = 10;
  static       int xborder2;
  static const int yborder  = 10;

  int max_size;

  // y-axis scales
  bool fixed_scales;
  std::map<int,double> f_min;
  std::map<int,double> f_max;  

  // show FFT
  static bool show_fft;
  
  // show spindles/SO power/phase
  static bool show_spso;

  
  void free_scales() 
  { fixed_scales = false; f_min.clear(); f_max.clear(); } 
  
  double fixed_min( const int i , double value ) 
  { 
    if ( ! fixed_scales ) return 0;
    if ( value < f_min[i] ) f_min[i] = value;
    return f_min[i]; 
  }
 
  double fixed_max( const int i , double value ) 
  { 
    if ( ! fixed_scales ) return 0;
    if ( value > f_max[i] ) f_max[i] = value;
    return f_max[i]; 
  }
  

  // labels
  
/*   wxStaticText * view_status;   */
/*   wxStaticText * options_status; */

  wxStaticText * tmp_text;

  // text line
  wxTextCtrl * cmdline;
  
  // parameters
  bool add_psd_curves;
  double psd_lower;
  double psd_upper;
  
  bool spectrogram_mode;

  
  // wxwidget internals

  DECLARE_EVENT_TABLE()    
    
};


class Viewer : public wxFrame
{
  
 public:

  Viewer(const wxString& title);

  void OnChar( wxKeyEvent & event );  
  void OnSampleBox( wxCommandEvent & event );
  void OnZoom( wxCommandEvent & event );
  void OnQuit( wxCommandEvent & event );
  void OnSignalBox( wxCommandEvent & event );
  void OnAnnotBox( wxCommandEvent & event );
  void OnAnnotBox2( wxCommandEvent & event );
  void OnSpecificAnnotBox( wxCommandEvent & event );
  void OnLeftRight( wxSpinEvent & event );
  void OnCommandLine( wxCommandEvent & event );
  void update_lists();

 private:

  Canvas * canvas;

  //  wxStaticText * filename;
  wxListBox * sample_box;

  wxCheckListBox * signal_box;
  std::set<std::string> last_signals;
  
  wxCheckListBox * annot_box;
  std::set<std::string> last_annot;

  wxCheckListBox * annot_box2;
  std::set<std::string> last_annot2;

  wxListBox * specific_annot_box;
  std::vector<uint64_t> specific_annot_positions;
  std::vector<feature_t> specific_annot_features;

  wxSpinButton * lr;
  wxSlider * zoom;
  
  bool hide_mask;

  DECLARE_EVENT_TABLE();

};


#endif
