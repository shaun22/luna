
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "wx/wx.h"
#include "grid.h"
#include "edf-handler.h"
#include "extras.h"

#include "../defs/defs.h"
#include "../dsp/reduce.h"
#include "../fftw/fftwrap.h"

#include <sstream>

extern edf_handler_t edf;

// helper functions, below

wxColour set_color( const std::string & c ) { return wxTheColourDatabase->Find( c ); }


void Canvas::render(wxDC&  dc)
{

  //
  // Populates edf.slice and sets edf.interval to the correct
  // position, used below
  //

  //
  // first pass: obviously can improve performance in a number of ways, 
  // but let's see how this works as is...
  //
  
  bool completed = edf.pull_data();
  

  //
  // Get number of channels to draw
  //
  
  const int nlines = edf.slice->size();
  
  const int nannot = edf.annots.size();
  
  const int ntotal = nlines + nannot;


  //
  // Min/max/range -- these get populated downstream (annots. first)
  //

  std::vector<double> min( ntotal );
  std::vector<double> max( ntotal );
  std::vector<double> rng( ntotal );


  //
  // Pull annotations (and get min/max)
  //

  std::map<std::string,interval_evt_map_t> allannots;
  int acnt = 0;
  std::map<std::string,annot_t*>::const_iterator ai = edf.annots.begin();
  while ( ai != edf.annots.end() )
    {

      annot_t * annot = ai->second;      
      
      std::cout << "pulling [" << annot->name << "] \n";

      allannots[ annot->name ] = annot->extract( interval_t( edf.interval ) );

      std::cout << "found " << allannots[ annot->name ].size() << "\n";
      
      if ( annot->has_range ) // use fixed range given in file
	{
	  min[ nlines + acnt ] = annot->min;
	  max[ nlines + acnt ] = annot->max;
	}
      else // OR calculate ranges empirically
	{
	        
	  interval_evt_map_t & tmp = allannots[ annot->name ];

	  bool first = true;
	  double mymin = 0, mymax = 0;

	  interval_evt_map_t::const_iterator ii = tmp.begin();
	  while ( ii != tmp.end() )
	    {	      
	      const int n = ii->second.size();
	      for (int i=0;i<n;i++)
		{
		  const event_t * event = ii->second[i];
		  double value = event->double_value();

		  if ( first ) 
		    {		      
		      mymin = value;
		      mymax = value;
		      first = false;
		    }
		  else 
		    {
		      if ( value < mymin ) mymin = value;
		      if ( value > mymax ) mymax = value;
		    }
		}
	      ++ii;
	    }
	
	  if ( mymax == mymin ) 
	    {
	      mymin--;
	      mymax++;
	    }

	  // using 'fixed' scales (i.e. only expand)
	  if ( fixed_scales ) 
	    {
	      mymin = fixed_min( nlines + acnt , mymin );
	      mymax = fixed_max( nlines + acnt , mymax );
	    }
	  
	  min[ nlines + acnt ] = mymin;
	  max[ nlines + acnt ] = mymax;
	  
	}
      
      rng[ nlines + acnt ] = max[ nlines + acnt ] - min[ nlines + acnt ];
      if ( rng[ nlines + acnt ] < 1e-8 ) rng[ nlines + acnt ] = 1;
      
      ++acnt;
      ++ai;
    }
  
  
  //
  // Get window dimensions
  //
  
  wxSize dim = GetClientSize();
  
  xborder2 = show_fft ? dim.x * 0.15 : xborder ;

  int xs = dim.x - ( xborder + xborder2 );
  int ys = dim.y - yborder*2;
  

  //
  // Get phyiscal min/max scalings for signals 
  // (we've already done the annotations, above)
  //

  for (int l=0;l<nlines;l++) 
    {

      const std::vector<double> * data = edf.slice->channel[l]->pdata();

      double mymin = (*data)[0];
      double mymax = (*data)[0];

      for (int i=0;i<data->size();i++) 
	{
	  if      ( (*data)[i] < mymin ) mymin = (*data)[i];
	  else if ( (*data)[i] > mymax ) mymax = (*data)[i];
	}

      // using 'fixed' scales?
      if ( fixed_scales ) 
	{
	  mymin = fixed_min( l , mymin );
	  mymax = fixed_max( l , mymax );
	}

      min[l] = mymin;
      max[l] = mymax;
      rng[l] = mymax - mymin;
      if ( rng[l] < 1e-8 ) rng[l] = 1;      
    }
  

  //
  // Get size of each plot
  //

  const int lower_px = 20;
  int height = ( ys - lower_px )  / ( ntotal ? ntotal : 1 ) ;
  
  
  //
  // Draw MASKs (i.e. in background, so draw first)
  //

  std::map<std::string,annot_t*>::const_iterator aii = edf.annots.begin();
  
  for (int l=0; l<nannot;l++) 
    {      
      annot_t * annot = aii->second;

      // obtain interval lists of annotations for this window
      interval_evt_map_t & a = allannots[ annot->name ];  
      uint64_t span = edf.interval.stop - edf.interval.start + 1;
      
      std::cout << "annot->name " << annot->name << "\n";
      std::cout << "a sz = " << a.size() << "\n";

      interval_evt_map_t::const_iterator iii = a.begin();
      while ( iii != a.end() )
	{
	  
	  uint64_t x1 = iii->first.start;
	  uint64_t x2 = iii->first.stop;
	  
	  double px1 = x1 > edf.interval.start ? ( x1 - edf.interval.start ) / (double)span : 0.0 ;
	  double px2 = x2 > edf.interval.start ? ( x2 - edf.interval.start ) / (double)span : 0.0 ;

	  int gx1 = xs * px1 + xborder;
	  int gx2 = xs * px2 + xborder;
	  
	  // ensure at least 2 pixels (for visibility)
	  if ( gx1 == gx2 ) 
	    {
	      --gx1;
	      ++gx2;
	    }
	  else if ( gx2 - gx1 == 1 ) 
	    {
	      ++gx2;
	    }
	  
	  if ( gx1 < xborder ) gx1 = xborder;
	  if ( gx2 > xborder + xs ) gx2 = xborder + xs;
	  
	  
	  // assume 1 value for now...
	  if ( iii->second.size() > 0 ) 
	    {
	      if ( annot->type[0] == ATYPE_MASK )
		{		  
		  dc.SetPen( wxPen( wxColor(255,255,255) , 0 ) );
		  dc.SetBrush( wxBrush( wxColor(100,100,100)  , wxBDIAGONAL_HATCH ) ) ;		  
		  dc.DrawRectangle( gx1 , yborder , gx2-gx1+1 , ys );	 
		}
	    }
	  ++iii;
	}
      // next annotation
      ++aii;
    }

  
  

  //
  // Draw SIGNALS
  //
  
  for (int l=0; l<nlines;l++) 
    {
      

      const std::vector<double>   * data  = edf.slice->channel[l]->pdata();
      const std::vector<uint64_t> * tp    = edf.slice->channel[l]->ptimepoints();
      const std::vector<int>      * rec   = edf.slice->channel[l]->precords();
      
      //
      // Get implied time interval
      //
      
      const uint64_t start_tp = (*tp)[0];
      const uint64_t stop_tp   = (*tp)[ tp->size() - 1 ];
      const uint64_t interval_tp = stop_tp - start_tp;
      // number of implied datapoints (unless a discontinuous EDF, should equal 'npoints'
      const double   interval_sec = interval_tp * globals::tp_duration;
      const int      Fs = edf.edf->header.sampling_freq( edf.signals(l) ) ;
      const int      interval_points = 1 + interval_sec * Fs;      
      
      const int npoints = data->size();      
      const int base = yborder + (l+1) * height;


      //
      // Draw standard lines (i.e. actual signal )
      //
      
      // baseline (at y == 0 )
      if ( l < ntotal )
	{
	  
	  int yp ;
	  if ( show_spso ) yp = base - height*0.5 - 0.5*height * ( ( 0 - min[l] ) / rng[l]  ) ;
	  else yp = base - height * ( ( 0 - min[l] ) / rng[l]  ) ;
	  
	  dc.SetPen( wxPen( wxColor(180,180,180) , 1 , wxSHORT_DASH) );
	  dc.DrawLine( xborder , yp , xs + xborder , yp );
	}
      
      // Two draw modes: 
      //  either more points than pixels (reduce) 
      //  OR     more pixels than points (draw lines as usual)
      
      if ( npoints <= xs ) 
	{
	  
	  const uint64_t range = edf.interval.stop - edf.interval.start + 1;
	  
	  // // expected increment for continuous signal
	  
	  const uint64_t inc = 
	    edf.edf->header.record_duration_tp / 
	    (uint64_t)edf.edf->header.n_samples[ edf.signals(l) ] ;
	  
	  // std::cout << "inc = " << inc << "\n";
	  // std::cout << "edf.s,s = " << edf.interval.start << " " 
	  // 		<< edf.interval.stop << "\n";
	  
	  
	  //
	  // draw points
	  //
	  
	  if ( npoints > 1 )
	    {
	      
	      // will probably remove this... but keep for now
	      if ( edf.use_downsampling )
		dc.SetPen( wxPen( wxColor(180,120,180) , 1 ) );
	      else
		dc.SetPen( wxPen( wxColor(0,255,255) , 1 ) );
	      //		dc.SetPen( wxPen( wxColor(0,0,0) , 1 ) );
	      
	      // allow for EDF+D	  
	      std::vector<wxPoint> points;
	      
	      for (int i=0;i<npoints-1;i++)
		{	  
		  
		  double frac = (*tp)[i] < edf.interval.start ? 0 : (double)( (*tp)[i] - edf.interval.start ) / range;
		  
		  const int xp = xborder + frac * xs;
		  
		  int yp;
		  if ( show_spso )
		    yp = base - height*0.5 - 0.5*height * ( ( (*data)[i] - min[l] ) / rng[l]  ) ;
		  else
		    yp = base - height * ( ( (*data)[i] - min[l] ) / rng[l]  ) ;

		  // std::cout << (*rec)[i+1] << " " <<  (*rec)[i] << "\n";
		  points.push_back( wxPoint( xp , yp ) );
		  
		  // same record? 
		  if ( (*rec)[i+1] != (*rec)[i] )
		    {		 
		      // next record?
		      if ( (*tp)[i+1] - (*tp)[i] > inc )
			{
			  if ( points.size() > 0 ) 
			    dc.DrawLines( points.size() , &(points[0]) );		    
			  points.clear();
			}
		    }
		}
	      
	      // finish up
	      if ( points.size() > 0 ) 
		dc.DrawLines( points.size() , &(points[0]) );
	      
	    }
	      
	}
      else
	{
	  
	  // Data-reduction draw mode
	  reduce_t r( data , tp , start_tp , stop_tp , xs );
	  
	  // allow for EDF+D	  
	  std::vector<wxPoint> points;
	  
	  
	  double yfac = show_spso ? 0.5 : 1; 

	  for (int xi=0;xi<xs;xi++)
	    {
	      // blank segment i.e. EDF+
	      if ( r.n[xi] == 0 ) continue;
	      
	      // 	      dc.SetPen( wxPen( wxColor(100,100,100) , 2 ) );
	      // 	      dc.DrawLine( xi , base - height * ( ( r.lo[xi] - min[l] ) / rng[l]  ) ,
	      // 			   xi , base - height * ( ( r.hi[xi] - min[l] ) / rng[l]  ) );
	      
	      
	      dc.SetPen( wxPen( wxColor(0,0,0) , 1 ) );
	      dc.SetPen( wxPen( wxColor(0,255,255) , 1 ) );
	      
	      if ( show_spso ) 
		{
		  wxRealPoint pmean( xi , base - height * 0.5 - 0.5 *  height * ( ( r.mean[xi] - min[l] ) / rng[l]  ) );
		  points.push_back( pmean );
		}
	      else
		{
		  wxRealPoint pmean( xi , base - yfac *  height * ( ( r.mean[xi] - min[l] ) / rng[l]  ) );
		  points.push_back( pmean );
		}
	      // connect dots...
	    }
	  dc.DrawSpline( points.size() , &(points[0]) );
	  
	}
          
      
      //
      // Show spindle power and SOs
      //

      if ( show_spso )
	{
	  
	  std::map<std::string,std::vector<double> > spso = scope_spindle_slowwave_calc( data , tp , Fs , xs );
	  
	  // slow spindles
	  const std::vector<double> & spin11 = spso[ "spindles-11" ]; 

	  double mn = 0 , rg = 1;

	  std::vector<wxPoint> points;
	  dc.SetPen( wxPen( wxColor(100,0,255) , 1 ) );	  
	  for (int c=0;c<spin11.size();c++)
	    {
	      wxRealPoint pmean( c , base - 0.5 *  height * ( ( spin11[c] - mn ) / rg  ) );
	      points.push_back( pmean );
	    }
	  dc.DrawSpline( points.size() , &(points[0]) );	  

	  // fast s[omd;es
	  const std::vector<double> & spin15 = spso[ "spindles-15" ]; 
	  points.clear();
	  dc.SetPen( wxPen( wxColor(200,100,55) , 1 ) );	  
	  for (int c=0;c<spin11.size();c++)
	    {
	      wxRealPoint pmean( c , base - 0.5 *  height * ( ( spin15[c] - mn ) / rg  ) );
	      points.push_back( pmean );
	    }
	  dc.DrawSpline( points.size() , &(points[0]) );	  

	  // sw magnitude
 	  const std::vector<double> & swmag = spso[ "sw-mag" ]; 
 	  points.clear();
 	  dc.SetPen( wxPen( wxColor(100,5,85) , 1 ) );	  
 	  for (int c=0;c<swmag.size();c++)
 	    {
 	      wxRealPoint pmean( c , base - 0.5 *  height * ( ( swmag[c] - mn ) / rg  ) );
 	      points.push_back( pmean );
 	    }
 	  dc.DrawSpline( points.size() , &(points[0]) );	  

	  // so-phase
	  const std::vector<double> & swph = spso[ "sw-phase" ]; 
	  mn = 0;
	  points.clear();
	  dc.SetPen( wxPen( wxColor(00,100,155) , 1 ) );	  
	  for (int c=0;c<spin11.size();c++)
	    {
	      wxRealPoint pmean( c , base - 0.5 * height * ( ( swph[c] - mn ) / rg  ) );
	      points.push_back( pmean );
	    }
	  dc.DrawSpline( points.size() , &(points[0]) );	  
	  
	}

    
      //
      // FFT
      //

      if ( show_fft )
	{
	  
	  int xs2 = dim.x - xborder * 2 ;
	  
	  const std::vector<double> * fft_data = edf.fft_slice->channel[l]->pdata();
	  
	  std::map<double,double> fft = fft_spectrum( fft_data , Fs );
	  
	  dc.SetPen( wxPen( wxColor(180,180,180) , 1 , wxSHORT_DASH) );	  

	  double xpx = xborder + ( 0.9 + ( (4-0.5 )/(30-0.5)) * 0.1 ) * xs2;	  
	  dc.DrawLine( xpx , base , xpx , base-height );

	  xpx = xborder + ( 0.9 + ( (8-0.5 )/(30-0.5)) * 0.1 ) * xs2;	  
	  dc.DrawLine( xpx , base , xpx , base-height );

	  xpx = xborder + ( 0.9 + ( (11-0.5 )/(30-0.5)) * 0.1 ) * xs2;	  
	  dc.DrawLine( xpx , base , xpx , base-height );

	  xpx = xborder + ( 0.9 + ( (15-0.5 )/(30-0.5)) * 0.1 ) * xs2;	  
	  dc.DrawLine( xpx , base , xpx , base-height );

	  std::vector<wxPoint> points;
	  
	  std::map<double,double>::const_iterator ff = fft.begin();
	  while ( ff != fft.end() )
	    {

	      //	      std::cout << "\t" << ff->first << "\t" << ff->second << "\n";
	      
	      double f = ff->first;
	      f = ( f - 0.5 ) / ( 30 - 0.5 ); // scale to 0..1
	      f = 0.9 + f * 0.10;             // rescale to 0.9 ... 1.0
	      double xpx = xborder + f * xs2;
	      double ypx = base - height * ff->second;
	      points.push_back( wxRealPoint( xpx , ypx ) );
	      //std::cout << xpx << " -- " << ypx << "\n";
	      ++ff;
	    }
	  
	  dc.SetPen( wxPen( wxColor( 255,255,0) , 2 ) );
	  dc.DrawSpline( points.size() , &(points[0]) );
	  	  
	  // x -- frac:  from 0.8 to 0.95
	  // y -- yp = base - heigth * FFT
	  
	}


      //
      // Text label for each signal, and min/max value
      //
      
      wxPoint pt( 10 , base-height+10 );
            
      wxString text( edf.slice->label(l) );

      wxFont font(12,wxFONTFAMILY_SWISS , wxNORMAL , wxFONTWEIGHT_BOLD );
      dc.SetFont(font);
      dc.SetBackgroundMode( wxTRANSPARENT );
      dc.SetTextForeground( *wxRED );
      dc.SetTextBackground( *wxWHITE );
      dc.DrawText( text , pt );

      // min/max
      std::stringstream ssmin;
      ssmin.precision(3);
      ssmin << min[l] << edf.edf->header.phys_dimension[edf.signals(l)];

      dc.DrawText( ssmin.str() , wxPoint( xs - 30 , base - 20 ) );
      
      std::stringstream ssmax;
      ssmax.precision(3);
      ssmax << max[l] << edf.edf->header.phys_dimension[edf.signals(l)];
      dc.DrawText( ssmax.str() , wxPoint( xs - 30 , base - height + 10  ) );

    }
  


  //
  // Time scale label
  //
  
  int xvalue = xs - 120;
  int yvalue = ys - lower_px+5 ;
  wxPoint pt( xvalue , yvalue );

  int h0,m0,s0;
  double pct = Helper::position( edf.total_size-1 , edf.total_size , &h0,&m0,&s0);  
  
  wxString text( Canvas::zoom_labels[ edf.zoom_factor ] + " of " + Helper::timestring(h0,m0,s0) );
  
  wxFont font(12,wxFONTFAMILY_SWISS , wxNORMAL , wxFONTWEIGHT_BOLD  );
  dc.SetFont(font);
  dc.SetBackgroundMode( wxTRANSPARENT );
  dc.SetTextForeground( *wxBLUE );
  dc.SetTextBackground( *wxWHITE );
  dc.DrawText( text , pt );
  

  //
  // Draw ANNOTs
  //
  
  // 1. Get any EPOCHS
  // 2. Get all annotations in this window
  
  // already defined above (in MASK); reset to beginning here
  aii = edf.annots.begin();  

  for (int l=0; l<nannot;l++) 
    {
      
      annot_t * annot = aii->second;
      
      std::cout << "looking at annot " << annot->name << "\n";
      
      // draw underneath
      const int gl = nlines + l;
      const int base = yborder + (gl+1) * height;


      // baseline dash line ... perhaps do not have this for annotations
      if ( gl < ntotal  )
	{	  
	  // 	  dc.SetPen( wxPen( wxColor(180,180,180) , 1 , wxSHORT_DASH) );
	  // 	  dc.DrawLine( xborder , base - 0.5*height, xs + xborder , base - 0.5*height);
	}
      
      // obtain interval lists of annotations for this window
      
      //      interval_evt_map_t & a = allannots[ aii->first ];
      interval_evt_map_t & a = allannots[ annot->name ];
      
      
      //      std::cout << "AAA sz = " << a.size() << "\t for " << aii->first << "\n";
      
      uint64_t span = edf.interval.stop - edf.interval.start + 1;

      // for QUANT drawing mode
      std::vector<std::vector<wxPoint> > apoints( annot->cols.size() );

      interval_evt_map_t::const_iterator iii = a.begin();
      while ( iii != a.end() )
	{
	  
	  uint64_t x1 = iii->first.start;
	  uint64_t x2 = iii->first.stop;
	  
	  double px1 = x1 > edf.interval.start ? ( x1 - edf.interval.start ) / (double)span : 0.0 ;
	  double px2 = x2 > edf.interval.start ? ( x2 - edf.interval.start ) / (double)span : 0.0 ;
	  	  	  
	  int gx1 = xs * px1 + xborder;
	  int gx2 = xs * px2 + xborder;

	  // std::cout << "ANNOT " << iii->second[0]->text_value()
	  // 	    << "\t" << x1 << " to " << x2 << "\t"
	  // 	    << edf.interval.start << "\t"
	  // 	    << iii->first.start_sec() << " to " << iii->first.stop_sec() << "\t"
	  // 	    << px1 << " to " << px2 << "\t"
	  // 	    << gx1 << " to " << gx2 << "\n";

	  
	  // ensure at least 2 pixels (for visibility
	  if ( gx1 == gx2 ) 
	    {
	      if ( gx1 > 0 ) --gx1;
	      ++gx2;
	    }
	  else if ( gx2 - gx1 == 1 ) 
	    {
	      ++gx2;
	    }
	  
	  if ( gx1 < xborder ) gx1 = xborder;

	  if ( gx2 > xborder + xs ) gx2 = xborder + xs;
	  
	  // assume 1 value for now...
	  if ( iii->second.size() > 0 ) 
	    {
	      
	      // Only QUANT can have multiple values displayed... (for now)
	      // so fix at 0 index
	      
	      double y = iii->second[0]->double_value();
	      
	      if ( annot->type[0] == ATYPE_SLEEP_STAGE )
		{
		  
		  std::string sstr = iii->second[0]->text_value();		  
		  
		  sleep_stage_t ss = globals::stage( sstr );
		  
		  int ss_offset = 0;
		  if      ( ss == WAKE  )  ss_offset = 4;
		  else if ( ss == NREM1 )  ss_offset = 2;
		  else if ( ss == NREM2 )  ss_offset = 1;
		  else if ( ss == NREM3 )  ss_offset = 0;
		  else if ( ss == NREM4 )  ss_offset = 0;
		  else if ( ss == REM   )  ss_offset = 3; 
		  else                     ss_offset = 6;
		  
		  // TMP fix
		  ss_offset = iii->second[0]->int_value();
		  std::cout << "ss_offset" << ss_offset << "\n";
		  if      ( ss_offset == 0 ) dc.SetBrush( wxBrush(wxColour(240,240,240)) );
		  else if ( ss_offset == 2 ) dc.SetBrush( wxBrush(wxColour(80,80,250)) );
		  else if ( ss_offset == 5 )   dc.SetBrush( wxBrush(wxColour(40,180,100)) );
		  else dc.SetBrush( wxBrush(wxColour(10,10,10)) );

		  if ( 0 ) // replace this back.
		    {
		      if  ( ss == WAKE )  // WAKE
			{ 
			  dc.SetBrush( wxBrush(wxColour(240,240,240)) );
			}    
		      else if  ( ss == NREM1 )  // NREM1
			{ 
			  dc.SetBrush( wxBrush(wxColour(100,220,255)) );
			}    
		      else if  ( ss == NREM2 )  // NREM2
			{ 
			  dc.SetBrush( wxBrush(wxColour(80,80,250)) );
			}    
		      else if  ( ss == NREM3 )  // NREM3/4
			{ 
			  dc.SetBrush( wxBrush(wxColour(30,60,120)) );
			}    
		      else if  ( ss == NREM4 )  // NREM3/4
			{ 
			  dc.SetBrush( wxBrush(wxColour(0,30,90)) );
			}    
		      else if  ( ss == REM )  // REM
			{ 
			  dc.SetBrush( wxBrush(wxColour(40,180,100)) );
			}    
		      else if  ( ss == MOVEMENT || ss == ARTIFACT )  // Other/missing
			{ 
			  dc.SetBrush( wxBrush(wxColour(200,200,200)) );
			}    
		    }
		       
		       // END TMP FIX



		  // NOTE -- FIXED value here -- '8' 		  
		  int yp = base - ( (height/6) + ss_offset * (height / 6 ) );		  
		  
		  dc.SetPen(wxPen(wxColour(50,50,50))); 
		  dc.DrawRectangle( gx1 , yp , gx2-gx1+1 , height/6 );
	
		}
	      
	      //
	      // Binary events
	      //

	      else if ( annot->type[0] == ATYPE_BINARY )
		{
		  int yp = base - ( 20 + y * (height/2) );
		  int v = round(y);
		  dc.SetPen( wxPen( wxColor(255,255,255) , 0 ) );
		  if ( v > 0 ) dc.SetBrush( *wxRED_BRUSH ) ;
		  else dc.SetBrush( *wxWHITE_BRUSH ) ;
		  dc.DrawRectangle( gx1 , yp , gx2-gx1+1 , 10 );
		  
		}

	
	      //
	      // Mask type
	      //
	      
	      else if ( annot->type[0] == ATYPE_MASK )
		{		  
		  // dc.SetPen( wxPen( wxColor(255,255,255) , 0 ) );
		  // dc.SetBrush( wxBrush( wxColor(100,100,100)  , wxBDIAGONAL_HATCH ) ) ;		  
		  // dc.DrawRectangle( gx1 , yborder , gx2-gx1+1 , ys );
		  
		  int yp = base - (height/2)  ;
		  dc.SetPen( wxPen( wxColor(100,100,100) , 0 ) );		  
		  dc.SetBrush( wxBrush( wxColor(100,100,100) ) ) ;		  
		  dc.DrawRectangle( gx1 , yp , gx2-gx1+1 , height/4 );

		}

	      //
	      // Textual events
	      //

	      else if ( annot->type[0] == ATYPE_TEXTUAL )
		{
		  int yp = base - ( 10 + (height/2) );		  
		  //		  wxPen pen( 
		  dc.SetPen( wxPen( wxColor(255,255,255) , 0 ) );
		  dc.SetBrush( *wxBLUE_BRUSH ) ;
		  dc.DrawRectangle( gx1 , yp , gx2-gx1+1 , 20 );		  
		}


	      //
	      // Colour Categories
	      //
	      
	      else if (  annot->type[0] == ATYPE_COLCAT )
		{
		  const std::string mycol = iii->second[0]->text_value();
		  wxColour col = set_color( mycol );
		  if ( ! col.IsOk() ) col = wxColor(100,100,100); // gray
		  int yp = base - (height/4.0);
		  dc.SetPen( wxPen( wxColor(10,10,10) , 0 ) );
		  dc.SetBrush( col ) ;
		  dc.DrawRectangle( gx1 , yp , gx2-gx1+1 , -(height/2.0) );

		}

	      //
	      // QUANTITATIVE
	      //

	      else if ( annot->type[0] == ATYPE_QUANTITATIVE )
		{
		  
		  const int npoints = iii->second.size();
		  
		  for (int j=0;j<npoints;j++)
		    {
		      
		      double y = iii->second[j]->double_value();
		      const int yp = base - ( height * ( ( y - min[gl] ) / rng[gl]  ) ) ;
		      
		      // save, to draw as line for whole annotation
		      apoints[j].push_back( wxPoint( gx1 , yp ) );
		      apoints[j].push_back( wxPoint( gx2 , yp ) );
		      
		    }
		  
		}
	      
	    }	   
	  ++iii;
	    	  
	}
      
      // need to draw the lines?
      if ( annot->type[0] == ATYPE_QUANTITATIVE ) 
	{	  	  
	  const int npoints = apoints.size();
	  for (int j=0;j<npoints;j++)
	    {
	      dc.SetPen( Canvas::inks[ j % 7 ] ); 
	      
	      for (int k=0;k<apoints[j].size();k+=2)		
		dc.DrawLine( apoints[j][k] , apoints[j][k+1] ); 	      
	    }
	    
	}
            

      //
      // Text label for each annot
      //
      
      wxPoint pt( 10 , base-height+10 );
      wxString text( annot->name );
      
      wxFont font(12,wxFONTFAMILY_SWISS , wxNORMAL , wxFONTWEIGHT_BOLD  );
      dc.SetFont(font);
      dc.SetBackgroundMode( wxTRANSPARENT );
      dc.SetTextForeground( *wxRED );
      dc.SetTextBackground( *wxWHITE );
      dc.DrawText( text , pt );

      // min/max labels for QUANTITATIVE annotations
      if ( annot->type[0] == ATYPE_QUANTITATIVE ) 
	{
	  std::stringstream ssmin;
	  ssmin.precision(3);
	  ssmin << min[gl];
	  dc.DrawText( ssmin.str() , wxPoint( xs - 35 , base - 20 ) );
	  
	  std::stringstream ssmax;
	  ssmax.precision(3);
	  ssmax << max[gl];
	  dc.DrawText( ssmax.str() , wxPoint( xs - 35 , base - height + 10  ) );

	}
      
      // next annotation
      ++aii;
      
    }


  //
  // Update view status
  //

  std::stringstream str;
  str.precision(0);
  str << std::fixed;
  
  int h,m,s;
  double pct1 = Helper::position( edf.interval.start , edf.total_size , &h,&m,&s);  
  int h1 = h, m1 = m, s1 = s;
  double pct2 = Helper::position( edf.interval.stop , edf.total_size , &h,&m,&s);  
  
  str << round(pct1 * 100) << "% - " << round(pct2*100) << "%; ";
  str << Helper::timestring(h1,m1,s1) << " - " << Helper::timestring(h,m,s) << "; ";
  str << edf.interval.start_sec() << " - " << edf.interval.stop_sec() << " secs; ";

  // asuming 30sec epochs
  int epoch1 = 1+(edf.interval.start/globals::tp_1sec)/30;
  int epoch2 = 1+(edf.interval.stop/globals::tp_1sec)/30;
  if ( epoch1 == epoch2 ) str << "epoch " << epoch1 ;
  else str << "epochs " << epoch1 << "-" << epoch2 ;

//   wxString mystring = str.str();
//   view_status->SetLabel( mystring );
  
  //
  // HMS time of window
  //
  
  h0 = 0; m0 = 0; s0 = 0;

  bool has_starttime = Helper::timestring( edf.edf->header.starttime , &h0, &m0, &s0 );
//   std::cout << "edf.edf->header.starttime  = " << edf.edf->header.starttime  << "\n";
//   std::cout << "has st = " << has_starttime << "\n";

  if ( has_starttime )
    {

      int h1 = h0, m1 = m0, s1 = s0;
      int h2 = h0, m2 = m0, s2 = s0;
      int msec1 = 0 , msec2 = 0;
      Helper::add_clocktime( &h1, &m1, &s1 , edf.interval.start , &msec1 );
      Helper::add_clocktime( &h2, &m2, &s2 , edf.interval.stop , &msec2 ); 
      
      std::stringstream str;
      str.precision(0);
      str << std::fixed;
      str << Helper::timestring(h1,m1,s1) << ":" << msec1 << " - " << Helper::timestring(h2,m2,s2) << ":" << msec2;
      wxString mystring = str.str();

      int xvalue = 40;
      int yvalue = ys - lower_px+5 ;
      wxPoint pt( xvalue , yvalue );
      
      wxFont font(12,wxFONTFAMILY_SWISS , wxNORMAL , wxFONTWEIGHT_BOLD  );
      dc.SetFont(font);
      dc.SetBackgroundMode( wxTRANSPARENT );
      dc.SetTextForeground( *wxBLUE );
      dc.SetTextBackground( *wxWHITE );
      dc.DrawText( mystring , pt );

    }
  
  
  
  //
  // Options label
  //


  
  // wxString optstring = "[ ";
  // if ( fixed_scales ) optstring.Append( "fixed" );
  // optstring.Append(" ]");
  //  options_status->SetLabel( optstring );
  
  
  //
  // Timeline
  //

  int tl_yvalue = ys + 10 ;
  
  dc.SetPen( wxPen( wxColor(20,20,20), 2 ) ); // black line, 3 pixels thick
  dc.DrawLine( xborder , tl_yvalue , xs , tl_yvalue );
  
  int x1 = xs * pct1;
  int x2 = xs * pct2;
  
  int px1 = xborder + x1;
  int px2 = xborder + x2;
  
  // dc.SetPen( wxPen( wxColor(20,200,20), 8 ) ); 
  // dc.DrawLine( xborder+x1 , tl_yvalue , xborder+x2 , tl_yvalue );  
  
  dc.SetBrush( wxBrush(wxColour(20,20,200)) );
  dc.SetPen(wxPen(wxColour(50,50,50))); 
  dc.DrawRectangle( px1 , tl_yvalue-4 , px2-px1+1 , 8 );

  // clear up data frame
  delete edf.slice;

}



