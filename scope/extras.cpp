
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "extras.h"

#include "../cwt/cwt.h"
#include "../dsp/hilbert.h"


void reduce_vecs( const std::vector<double> * x , std::vector<double> * reduced )
{
  const int npnt = x->size();
  const int npix = reduced->size();
  const double r_inc = npnt / (double)npix;  
  double r = 0;
  double s = r + r_inc;
  int c = 0;
  std::vector<double> w( npix , 0 );

  for (int p = 0 ; p < npnt ; p++ )
    {

      // boundaries
      int ri = floor(r);
      int si = floor(s);
      
      //      std::cout << "p = " << p << " " << r << " " << ri << " -- " << s << " " << si << " -- " << c << "\n";
      
      // on a boundary?
      if ( p == ri ) 
	{
	  double fraction = 1 - ( r - ri ) ;
	  (*reduced)[ c ] += fraction * (*x)[p];
	  w[c] += fraction;
	}
      else if ( p == si )
	{
	  double fraction = s - si ;
	  (*reduced)[ c ] += fraction * (*x)[p];
	  w[c] += fraction;
	  // advance to next bin, but repeat this 'p'
 	  if ( ++c == npix ) break;
	  r = s;
	  s += r_inc;
	  --p;
	}
      else if ( p >= r && p < s ) 
	{
	  (*reduced)[ c ] += (*x)[p];	  
	  w[c]++;
	}
      else if ( p >= s ) 
	{
 	  if ( ++c == npix ) break;
	  r = s;
	  s += r_inc;
	  --p;
	}

      // next time-point
    }
  
  // normalize
  // denominator: for now, make 0..1 
  double mx = -99999;
  double mn = 99999;
  for (int c=0;c<npix;c++) 
    {
      if ( (*reduced)[c] > mx ) mx = (*reduced)[c];
      if ( (*reduced)[c] < mn ) mn = (*reduced)[c];
    }
  
  if ( mx == mn ) { mx = 1;  mn = 0 ; } 
  
  for (int c=0;c<npix;c++) (*reduced)[c] = ( (*reduced)[c] - mn ) / ( mx - mn );
  
}


std::map<std::string,std::vector<double> > scope_spindle_slowwave_calc( const std::vector<double> * d , 
									const std::vector<uint64_t> * tp , 
									const int fs , 
									const int npix )
{

  const int npnt = d->size();
  
  std::map<std::string,std::vector<double> > results;
  results[ "spindles-11" ].resize( npix , 0 );
  results[ "spindles-15" ].resize( npix , 0 );
  results[ "sw-mag" ].resize( npix , 0 );
  results[ "sw-phase" ].resize( npix , 0 );
  
  std::vector<double> & reduced11 = results[ "spindles-11" ];
  std::vector<double> & reduced15 = results[ "spindles-15" ];
  
  std::vector<double> & reduced_swph = results[ "sw-phase" ];
  std::vector<double> & reduced_swmag = results[ "sw-mag" ];

  
  // input data = 'd' with sampling rate 'fs'
  
  // generate npix levels of output bins for the following:
  
  //  - wavelet coeff. for 11 Hz, 15 Hz
  //  - slow waves, and instantaneous phase
  
  CWT cwt;
  cwt.set_sampling_rate( fs );
  
  const int num_cycles = 12; 
  cwt.add_wavelet( 11 , num_cycles );  // f( Fc , number of cycles ) 
  cwt.add_wavelet( 15 , num_cycles );  // f( Fc , number of cycles ) 
  cwt.load( d );
  cwt.run();
  
  
  const std::vector<double> & spin11 = cwt.results(0);
  const std::vector<double> & spin15 = cwt.results(1);
  
  reduce_vecs( &spin11 , &reduced11 );
  reduce_vecs( &spin15 , &reduced15 );
  
  //
  // Slow waves
  //
  
//   double lwr = 0.6;
//   double upr = 0.8;
  double lwr = 0.95;
  double upr = 1.05;

  //  double mag = 0.75; 
  
  // filter-Hilbert raw signal for SWs
  hilbert_t hilbert( *d , fs , lwr , upr );  
  //  slow_waves_t sw( *d , *tp , fs );

  const std::vector<double> * ph = hilbert.phase();
  const std::vector<double> * mag = hilbert.magnitude();
  
  reduce_vecs( ph , &reduced_swph );
  reduce_vecs( mag , &reduced_swmag );
  
  return results;
}



void scope_spindle_slowwave_draw( std::map<std::string,std::vector<double> > & d )
{

  std::map<std::string,std::vector<double> >::const_iterator ii = d.begin();
  while ( ii != d.end() )
    {
      std::cout << ii->first << "\n";
      const int n = ii->second.size();
      for (int i=0;i<n;i++) std::cout << "  " << ii->second[i] << "\n";
      ++ii;
    }
  
}
