
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "grid.h"
#include "edf-handler.h"

#include "wx/spinbutt.h"

extern edf_handler_t edf;

const int ID_SLIDER    = 1000;
const int ID_SIGNALBOX = 1001;
const int ID_LEFTRIGHT = 1002;
const int ID_ANNOTBOX  = 1003;
const int ID_ANNOTBOX2 = 1004;
const int ID_SPECIFICANNOTBOX  = 1005;
const int ID_SAMPLEBOX = 1006;
const int ID_CMDLINE   = 1007;

std::vector<int> Canvas::zoom_levels;
std::vector<std::string> Canvas::zoom_labels;
std::vector<int> Canvas::downsampling;
std::vector<wxPen> Canvas::inks;
bool Canvas::show_fft;
int Canvas::xborder2;
bool Canvas::show_spso;

//
// Canvas things
//

Canvas::Canvas(wxPanel * parent) : 
  wxWindow( parent , wxID_ANY , wxDefaultPosition , wxSize(3600,400) , wxBORDER_SIMPLE ) 
{ 
  
  fixed_scales = false;
  edf.use_downsampling = true;
  show_fft = false;
  show_spso = false;
  xborder2 = xborder;

  // inks
  
  wxPen pen = wxPen( wxT("CORAL")     , 8 ) ;
  pen.SetCap( wxCAP_BUTT );
  inks.push_back( pen );
  
  inks.push_back( wxPen( wxT("GREEN")     , 8 ) );
  inks.push_back( wxPen( wxT("BLUE")      , 8 ) );
  inks.push_back( wxPen( wxT("RED")       , 8 ) );
  inks.push_back( wxPen( wxT("GRAY")      , 8 ) );
  inks.push_back( wxPen( wxT("CYAN")      , 8 ) );
  inks.push_back( wxPen( wxT("GOLDENROD") , 8 ) );
  
  // 12 zoom levels in seconds
  zoom_levels.clear();
  zoom_labels.clear();
  downsampling.clear();
  
  max_size = 100000;

  zoom_levels.push_back( 1 );
  zoom_levels.push_back( 5 );
  zoom_levels.push_back( 10 );
  zoom_levels.push_back( 20 );
  zoom_levels.push_back( 30 ); 
  zoom_levels.push_back( 60 );       // 1 min
  zoom_levels.push_back( 60 * 5 );   // 5 min
  zoom_levels.push_back( 60 * 10 ); 
  zoom_levels.push_back( 60 * 30 ); 
  zoom_levels.push_back( 60 * 60 );  // 1 hr
  zoom_levels.push_back( 5 * 60 * 60 );  // 5 hr
  zoom_levels.push_back( 0 );  // whole trace

  zoom_labels.push_back( "1 sec" );
  zoom_labels.push_back( "5 sec" );
  zoom_labels.push_back( "10 sec" );
  zoom_labels.push_back( "20 sec" );
  zoom_labels.push_back( "30 sec" );
  zoom_labels.push_back( "1 min" );
  zoom_labels.push_back( "5 min" );
  zoom_labels.push_back( "10 min" );
  zoom_labels.push_back( "30 min" );
  zoom_labels.push_back( "1 hr" );
  zoom_labels.push_back( "5 hr" );
  zoom_labels.push_back( "whole trace" );

  downsampling.push_back( 1   );  // 1s
  downsampling.push_back( 1   );  // 5s
  downsampling.push_back( 2   );  // 10s
  downsampling.push_back( 2   );  // 20s
  downsampling.push_back( 2   );  // 30s
  downsampling.push_back( 5   );  // 1m
  downsampling.push_back( 10   );  // 5m
  downsampling.push_back( 20   );  // 10m
  downsampling.push_back( 50   );  // 30m
  downsampling.push_back( 100  );  // 1h
  downsampling.push_back( 500  );  // 5hr
  downsampling.push_back( 500  );  // whole

}


BEGIN_EVENT_TABLE(Canvas, wxWindow)
 EVT_PAINT(Canvas::OnPaint)
 EVT_ERASE_BACKGROUND(Canvas::EmptyBackground)
 EVT_MOUSE_EVENTS( Canvas::OnMouseEvent )
 EVT_KEY_DOWN( Canvas::OnChar )
END_EVENT_TABLE()


void Canvas::EmptyBackground(wxEraseEvent & evt) { } // empty


void Canvas::Background(wxDC& dc )
{
  dc.SetBrush( wxBrush( wxColour(255,255,255) ) );
  dc.SetBrush( wxBrush( wxColour(0,0,0) ) );
  dc.SetPen( wxPen( wxColour(100,100,100) , 1 ) );
  wxRect windowRect( wxPoint(0,0) , GetClientSize() );
  dc.DrawRectangle( windowRect );
}


void Canvas::OnPaint(wxPaintEvent & evt)
{
  wxPaintDC dc(this);
  Background(dc);
  render(dc);
}



//
// Main grid
//


Viewer::Viewer(const wxString& title)
  : wxFrame(NULL, -1, title, wxPoint(-1, -1), wxSize(1200, 600))
{ 

  //
  // Overall grid structure
  //

  //  std::cout << "constructing Viewer()\n";
  
  wxPanel         * panel = new wxPanel(this, -1);  
  
  panel->SetBackgroundColour(wxColour(* wxBLACK));


  wxBoxSizer      * hbox = new wxBoxSizer(wxHORIZONTAL);

  // ORIG was 3
  wxFlexGridSizer * fgs = new wxFlexGridSizer(3, 1, 20, 20);  


  
  //
  // signals -- obtained from edf_handler_t
  //
  
  wxArrayString signals;
  int ns = edf.num_channels();
  for (int i=0;i<ns;i++) 
    {
      if ( edf.channel_labels[i].substr(0,9) != "EDF Annot" )
	signals.Add( edf.channel_labels[i] );
    }

  signal_box = new wxCheckListBox( panel , 
				   ID_SIGNALBOX , 
				   wxDefaultPosition , 
				   wxSize(240,180) , 
				   signals , 
				   wxLB_MULTIPLE );

  signal_box->SetWindowVariant( wxWINDOW_VARIANT_SMALL );

  //  signal_box->SetBackgroundColour(wxColour(* wxBLACK));
  signal_box->SetBackgroundColour(wxColour(100,100,200) );
  
  //dc.SetPen( wxPen( wxColour(100,100,100) , 1 ) );

//   wxFont font = signal_box->GetFont();
//   font.setColour( wxColour(* wxWHITE));
//   font.SetFamily(wxFONTFAMILY_MODERN);
//   font.SetFaceName("Courier");
//  signal_box->SetForegroundColour( wxColour(* wxYELLOW ));
  
  //
  // Canvas
  //
    
  canvas = new Canvas( panel );
  
  //
  // Populate a sizer of controls in the left frame
  //
  
  //  filename = new wxStaticText(panel, -1, "Sample ID: " + edf.current_sample_id );
    
  // status line
  //  canvas->view_status = new wxStaticText(panel, -1, wxT("n/a"));

  // options 
  //  canvas->options_status = new wxStaticText(panel,-1,wxT(""));


  //
  // Middle status line: ID, time, etc, and command line
  //

  wxFlexGridSizer * midgrid = new wxFlexGridSizer(1, 1, 100, 5);  
//   midgrid->Add( filename , 1 );
//   midgrid->Add( canvas->view_status , 5 );
//   midgrid->Add( canvas->options_status , 5 );

  canvas->cmdline = new wxTextCtrl( panel , ID_CMDLINE, wxT(""), 
				    wxPoint(-1, -1),wxSize(2000, -1) ,
				    wxTE_PROCESS_ENTER );
  midgrid->Add( canvas->cmdline , 5 , wxEXPAND );  // 5?
  
  //  wxFlexGridSizer * midgrid2 = new wxFlexGridSizer(1, 1, 20, 20);  
  //   canvas->cmdline 
  //   midgrid2->Add( canvas->cmdline , 1 , wxEXPAND );  // 5?
  
  // EVT_TEXT_ENTER(id, func):
  
  //
  // Respond to a wxEVT_TEXT_ENTER event, generated when enter is
  // pressed in a text control which must have wxTE_PROCESS_ENTER
  // style for this event to be generated.
  //

  
  //
  // Samples
  //
  
  wxArrayString samples;
  ns = edf.sample.size();
  for (int i=0;i<ns;i++) samples.Add( edf.sample[i] );
  samples.Sort();
  
  //  std::cout << "ns = " << ns << "\n";
  
  sample_box = new wxListBox( panel , 
  			      ID_SAMPLEBOX , 
  			      wxDefaultPosition , 
  			      wxSize(100,100) , 
  			      samples,
  			      wxLB_SINGLE );
  
  sample_box->SetWindowVariant( wxWINDOW_VARIANT_SMALL );
  
  sample_box->SetSelection(0);


  //
  // Annotations (to be displayed on main canvas)
  //

  wxArrayString annots;
  std::vector<std::string> annot_labels = edf.edf->timeline.annotations.names();
  int na = annot_labels.size();
  for (int i=0;i<na;i++) 
    {
      if ( annot_labels[i] != "EDF Annotations" ) 
	annots.Add( annot_labels[i] );
    }

  annot_box = new wxCheckListBox( panel , 
				  ID_ANNOTBOX , 
				  wxDefaultPosition , 
				  wxSize(80,80) , 
				  annots , 
				  wxLB_MULTIPLE );
  
  annot_box->SetWindowVariant( wxWINDOW_VARIANT_SMALL );
  annot_box->SetBackgroundColour(wxColour(200,100,100) );

  //
  // Annotations (to be listede in Specific-Annot box)
  //
  
  annot_box2 = new wxCheckListBox( panel , 
				   ID_ANNOTBOX2 , 
				   wxDefaultPosition , 
				   wxSize(80,80) , 
				   annots , 
				   wxLB_MULTIPLE );
  
  annot_box2->SetWindowVariant( wxWINDOW_VARIANT_SMALL );
  annot_box2->SetBackgroundColour(wxColour(200,100,200) );

  //
  // Annotations (list)
  //

  wxArrayString specific_annots; // empty here
  
  specific_annot_box = new wxListBox( panel , 
				      ID_SPECIFICANNOTBOX , 
				      wxDefaultPosition , 
				      wxSize(80,80) , 
				      specific_annots , 
				      wxLB_SINGLE );
  
  specific_annot_box->SetWindowVariant( wxWINDOW_VARIANT_SMALL );
  specific_annot_box->SetBackgroundColour(wxColour(100,200,100) );

  
  //
  // Lower panel of controls
  //

  wxFlexGridSizer * lgrid = new wxFlexGridSizer(1, 5, 5, 5);  
  lgrid->Add( sample_box , 4 , wxEXPAND );
  lgrid->Add( signal_box , 4 , wxEXPAND );
  lgrid->Add( annot_box  , 4 , wxEXPAND );
  lgrid->Add( annot_box2 , 4 , wxEXPAND );
  lgrid->Add( specific_annot_box , 4 , wxEXPAND );

  //  lgrid->AddGrowableCol(1,1);
  lgrid->AddGrowableCol(2,1);
  lgrid->AddGrowableCol(3,1);
  lgrid->AddGrowableCol(4,1);
  

  //
  // Line everything up in the main frames
  //
  
  fgs->Add( canvas , 16, wxALL | wxEXPAND );  
  fgs->Add( midgrid, 1, wxEXPAND );
  //  fgs->Add( midgrid2, 1, wxEXPAND );
  fgs->Add( lgrid, 10, wxEXPAND );
  fgs->AddGrowableRow(0, 1);
  
  hbox->Add(fgs, 1, wxALL | wxEXPAND, 10);
  panel->SetSizer(hbox);

  Centre();

}


//
// Event table
//

//  EVT_BUTTON( wxID_EXIT , Viewer::OnQuit )
//  EVT_SLIDER( ID_SLIDER , Viewer::OnZoom )
//  EVT_SPIN( ID_LEFTRIGHT , Viewer::OnLeftRight )

BEGIN_EVENT_TABLE( Viewer , wxFrame )
  EVT_TEXT_ENTER( ID_CMDLINE , Viewer::OnCommandLine )
  EVT_LISTBOX( ID_SAMPLEBOX , Viewer::OnSampleBox )
  EVT_CHECKLISTBOX( ID_SIGNALBOX , Viewer::OnSignalBox )
  EVT_CHECKLISTBOX( ID_ANNOTBOX , Viewer::OnAnnotBox )
  EVT_CHECKLISTBOX( ID_ANNOTBOX2 , Viewer::OnAnnotBox2 )
  EVT_LISTBOX( ID_SPECIFICANNOTBOX , Viewer::OnSpecificAnnotBox )
  EVT_KEY_DOWN( Viewer::OnChar )
END_EVENT_TABLE()


//
// Handle events
//


void Viewer::OnZoom( wxCommandEvent & event )
{
  // Zoom is from 1 to zoom_levels.size()
  edf.zoom_factor = event.GetInt();  
  canvas->Refresh();
}


void Viewer::OnLeftRight( wxSpinEvent & event )
{
  edf.position = lr->GetValue();
  canvas->Refresh();  
}


void Viewer::OnCommandLine( wxCommandEvent & event )
{  
  std::string t = std::string( canvas->cmdline->GetValue().mb_str() );
  canvas->cmdline->Clear();
  std::cout << "Enter wqs pressed : [" << t << "]\n";
  
}

void Viewer::OnSampleBox( wxCommandEvent & event )
{
  
  // 1. save signal/annot lists
  last_signals.clear();
  last_annot.clear();
  last_annot2.clear();

  // signals
  int cnt = signal_box->GetCount();
  for (int i=0;i<cnt;i++)
    if ( signal_box->IsChecked(i) ) last_signals.insert( std::string(signal_box->GetString(i) ));

  // annot
  cnt = annot_box->GetCount();
  for (int i=0;i<cnt;i++)
    if ( annot_box->IsChecked(i) ) last_annot.insert( std::string(annot_box->GetString(i) ));

  // annot (2)
  cnt = annot_box2->GetCount();
  for (int i=0;i<cnt;i++)
    if ( annot_box2->IsChecked(i) ) last_annot2.insert( std::string(annot_box2->GetString(i) ));

  // 3. clear these first, so user knows something is happening on click
  signal_box->Clear();
  annot_box->Clear();  
  annot_box2->Clear();  
    
  wxString id = sample_box->GetString( sample_box->GetSelection() );
  
  edf.attach_sample( id.ToStdString() );
  
  update_lists();

  canvas->Refresh();
}



void Viewer::OnSignalBox( wxCommandEvent & event )
{
  int cnt = signal_box->GetCount();
  edf.channels.clear();
  for (int i=0;i<cnt;i++)
    {
      bool selected = signal_box->IsChecked(i);
      if ( selected ) edf.channels.insert( i );
    }
  canvas->Refresh();  
}



//
// Handle annot box #1 (i.e. linked to main canvas)
//


void Viewer::OnAnnotBox( wxCommandEvent & event )
{
  
  int cnt = annot_box->GetCount();
  
  edf.annots.clear();
  
  for (int i=0;i<cnt;i++)
    {
      
      bool selected = annot_box->IsChecked(i);
      
      if ( selected )
	{
	  std::string label = std::string(annot_box->GetString(i));	  
	  
	  annot_t * a = edf.edf->timeline.annotations.find( label );
	  
	  std::cout << "attaching  [" << label << "]\n";
	  
	  if ( a != NULL )
	    edf.annots[ label ] = a; 
	  else 
	    std::cout << "could not attach " << label << "\n";

	}
    }

  //
  // Refresh canvas
  //

  canvas->Refresh();  


}



void Viewer::OnAnnotBox2( wxCommandEvent & event )
{
  
  int cnt = annot_box2->GetCount();
  
  std::set<annot_t*> sannots;
  
  for (int i=0;i<cnt;i++)
    {
      
      bool selected = annot_box2->IsChecked(i);
      
      if ( selected )
	{
	  std::string label = std::string(annot_box2->GetString(i));	  
	  annot_t * a = edf.edf->timeline.annotations.find( label );
	  if ( a != NULL && label != "EDF Annotations" ) sannots.insert( a );	  
	}
    }


  //
  // Populate specific annot box
  //

  std::map<std::string,interval_evt_map_t> allannots;
  
  std::map<feature_t,int> features; // interval + labels  
 
  std::set<annot_t*>::const_iterator ai = sannots.begin();
  while ( ai != sannots.end() )
    {      

      std::string aclass = (*ai)->name;
      allannots[ aclass ] = (*ai)->extract( interval_t( 0 , edf.total_size ) );      
      interval_evt_map_t & tmp = allannots[ (*ai)->name ];      
      interval_evt_map_t::const_iterator ii = tmp.begin();
      while ( ii != tmp.end() )
	{	 
	  
	  feature_t f;
	  f.feature = ii->first;
	  f.window = ii->first;

	  // get event(s)
	  const std::vector<const event_t*> & events = ii->second;
	  std::string label = "";
	  for (int e=0;e<events.size();e++)
	    {
	      if ( e!=0 ) label += ",";
	      label += events[e]->label;
	      f.add_data( events[e]->metadata );
	    }
	  	  
	  
	  if ( label != aclass )
	    f.signal = label;
	  
	  f.label = aclass;

	  // store
	  features[ f ] = features.size() - 1 ; 

	  // next interval
	  ++ii;
	}

      // next annotation 
      ++ai;
    }
  
  //
  // Populate specific annotation box
  //

  specific_annot_positions.clear();
  specific_annot_features.clear();
  specific_annot_box->Clear();
  
  wxArrayString specific_annots;
  std::map<feature_t,int>::const_iterator ff = features.begin();
  while ( ff != features.end() )
    {
      specific_annots.Add( ff->first.as_string() );
      specific_annot_positions.push_back( ff->first.feature.mid() );
      specific_annot_features.push_back( ff->first );
      ++ff;
    }

  if ( specific_annots.size() > 0 ) 
    specific_annot_box->InsertItems( specific_annots , 0 );    


  //
  // Refresh canvas
  //

  canvas->Refresh();  


}





void Viewer::OnSpecificAnnotBox( wxCommandEvent & event )
{
  
  // which annotation has been selected?
  int aid = specific_annot_box->GetSelection();
  
  if ( aid == -1 ) return;
  
  // find position of this annotation
  uint64_t center_position = specific_annot_positions[ aid ];

  // move window to this position
  edf.position = center_position;

  // display any additional data in the console
  const feature_t & feature = specific_annot_features[ aid ];
  std::cout << "\n" << feature.as_string( "\t" ) << "\n";
  std::cout << feature.print_data( )  << "\n";
  
  // refresh canvas
  canvas->Refresh();  
}



void Viewer::OnQuit( wxCommandEvent & WXUNUSED(event) )
{
  Close(true);
}


//
// Mouse handler for Canvas
//


void Canvas::OnMouseEvent( wxMouseEvent & event )
{
  
  SetFocus();
  
  wxPoint pt( event.GetPosition() );
//   std::cerr << "mouse " << pt.x << " " << pt.y << "\n";
//   std::cerr << "last  " << last.x << " " << last.y << "\n";

  // bound

  wxSize dim = GetClientSize();

  if ( pt.x < 0 ) pt.x = 0;
  if ( pt.y < 0 ) pt.y = 0;
  if ( pt.x > dim.x ) pt.x = dim.x;
  if ( pt.y > dim.y ) pt.y = dim.y;

  // double click --> center and zoom
  if ( event.LeftDClick() )
    {
      
      // center window
      
      // 1. figure out current implied location
            
      uint64_t window = Canvas::zoom_levels[ edf.zoom_factor ] * globals::tp_1sec;
      uint64_t start  = edf.position;   
      uint64_t stop  = start + window;      
      if ( stop >= edf.total_size ) stop = edf.total_size - 1;      
      if ( Canvas::zoom_levels[ edf.zoom_factor ] == 0 ) // whole trace
	{
	  start = 0;
	  stop = edf.total_size - (uint64_t)1;
	}
      
      // where in this are we clicking?
      // get 0..1 scale from xs
      int pos = pt.x;
      if ( pos < Canvas::xborder ) pos = Canvas::xborder;
      if ( pos >= dim.x - Canvas::xborder ) pos = dim.x - Canvas::xborder - 1;
      double p = pos / (double)( dim.x - 2 * Canvas::xborder );
      
      uint64_t wspan = stop - start;
      --wspan;
      
      uint64_t newpos_center = start + (uint64_t)(p * wspan);
      
      // shift window
      newpos_center -= wspan/(uint64_t)2;
      
      // normally would then shift over half window to get start, let's do that no
      edf.position = newpos_center;
      
      // increase zoom
      //       if ( edf.zoom_factor < Canvas::MAX_ZOOM_LEVELS )
// 	++edf.zoom_factor;

      Refresh();
      
    }

  // mouse down?
  if ( event.LeftDown() )
    {
      last = pt;
      //      std::cerr << "set mouse position\n";
    }

  if ( event.Dragging() )
    {
//       std::cerr << "DRAG : " << last.x << "x" << last.y << " --> "
// 		<< pt.x << "x" << pt.y << "\n";

      double xy_motion = ( pt.x - last.x ) / (double)dim.x;
      double zoom_factor = ( pt.y - last.y ) / (double)dim.y;
      
      // the above will be on rough scale of -1 to +1;
      
      // map ZOOM to 1/2 and 2.0
      
      //      zoom_factor = ( zoom_factor + 1.0 ) / 2.0;
      
      //zoom_factor = pow(2,zoom_factor);
      
//       std::cout << "xy, zm\t"
// 		<< xy_motion << "\t"
// 		<< zoom_factor << "\n";
      
      
      // redraw

      edf.zoom_factor *= 2 * zoom_factor;
      if ( edf.zoom_factor < 1 ) edf.zoom_factor = 1;
      if ( edf.zoom_factor > 100 ) edf.zoom_factor = 100;
      //      std::cout << "edf z = " << edf.zoom_factor << "\n";
      Refresh();
      
    }

}


void Viewer::OnChar( wxKeyEvent & event )
{
  // Set primary options for viewing
  
  // 1) fixed scale  ('F')
  // 2) hide mask elements  ('M')
  // 3) collapse EDF+D or no ('D')
  
  wxChar uc = event.GetUnicodeKey();

  if ( (int)uc != (int)WXK_NONE )
    {

      if ( uc == wxT('s') || uc == wxT('S') )
	{
	  Canvas::show_fft = ! Canvas::show_fft;
	  //	  std::cerr << " FFT spectrum = " << Canvas::show_fft << "\n";
	}

      if ( uc == wxT('x') || uc == wxT('X') )
	{
	  Canvas::show_spso = ! Canvas::show_spso;
	}
      

      if ( uc == wxT('f') || uc == wxT('F') ) 
	{
	  if ( canvas->fixed_scales )
	    {
	      canvas->free_scales();
	      canvas->fixed_scales = false;	      
	    }
	  else
	    {
	      canvas->fixed_scales = true;
	    }
	}
      
      if ( uc == wxT('d') || uc == wxT('D') ) 
	{
	  edf.use_downsampling = ! edf.use_downsampling;
	  std::cout << "set downsampling " << ( edf.use_downsampling ? "on" : "off" ) 
		    << "\n";
	}

      if ( uc == wxT('m') || uc == wxT('M') ) 
	{
	  
	}
      
      if ( uc == wxT('d') || uc == wxT('D') ) 
	{
	  
	}

    }

}


void Canvas::OnChar( wxKeyEvent & event )
{

  //
  // epoch-based commands?
  //

  wxChar uc = event.GetUnicodeKey();

  if ( (int)uc != (int)WXK_NONE )
    {
      
      if ( uc == wxT('s') || uc == wxT('S') ) 
	{
	  show_fft = ! show_fft;
	  //	  std::cerr << " FFT spectrum = " << show_fft << "\n";
	}
      
      if ( uc == wxT('x') || uc == wxT('X') ) 
	{
	  show_spso = ! show_spso;
	}

      if ( uc == wxT('f') || uc == wxT('F') ) 
	{
	  if ( fixed_scales )
	    {	      
	      fixed_scales = false;	      
	    }
	  else
	    {
	      fixed_scales = true;
	    }
	}

      // rescale
      if ( uc == wxT('r') || uc == wxT('R') ) 
	{
	  free_scales();
	}


      // downsampling
      if ( uc == wxT('d') || uc == wxT('D') ) 
	{
	  edf.use_downsampling = ! edf.use_downsampling;
	  std::cout << "set downsampling " << ( edf.use_downsampling ? "on" : "off" ) 
		    << "\n";
	}

      uint64_t epoch_tp = 30 * globals::tp_1sec;
      uint64_t half_epoch = 15 * globals::tp_1sec;

      if ( uc == wxT('e') || uc == wxT('E') ) 
	{
	  // center around middle position (unlike 'n' and 'p')	  
	  edf.zoom_factor = 4;  // i.e. 30sec EPOCHS
	  uint64_t epoch = edf.position / epoch_tp;	  
	  edf.position = epoch_tp*epoch;
	  if ( half_epoch > edf.position ) edf.position = 0;
	  else edf.position += half_epoch;	  
	}
      else if ( uc == wxT('n') || uc == wxT('N') || uc == wxT('w') || uc == wxT('W') ) 
	{
	  edf.zoom_factor = 4;  // i.e. 30sec EPOCHS
	  uint64_t epoch = edf.interval.start / epoch_tp;  
	  ++epoch;
	  edf.position = epoch_tp*epoch;
	  if ( half_epoch > edf.position ) edf.position = 0; 
	  else edf.position += half_epoch;
	}
      else if ( uc == wxT('p') || uc == wxT('P') || uc == wxT('Q') || uc == wxT('q') ) 
	{
	  edf.zoom_factor = 4;  // i.e. 30sec EPOCHS
	  uint64_t epoch = edf.interval.start / epoch_tp;
	  if ( epoch != 0 ) --epoch;
	  edf.position = epoch_tp*epoch;
	  if ( half_epoch > edf.position ) edf.position = 0;
	  else edf.position += half_epoch;
	}
    }

  
  //
  // zoom?
  //

  if ( event.GetKeyCode() == WXK_DOWN )
    {
      if ( edf.zoom_factor < Canvas::MAX_ZOOM_LEVELS - 1 )
	++edf.zoom_factor;
    }
  else if ( event.GetKeyCode() == WXK_UP ) 
    {
      if ( edf.zoom_factor > 0 ) --edf.zoom_factor;
    }

  //
  // Shifts
  //
  
  // if in whole-trace mode, then no shifting allowed
  if ( Canvas::zoom_levels[ edf.zoom_factor ] != 0 ) 
    {

      // shift forward one window
      if ( event.GetKeyCode() == WXK_SPACE )
	{    
	  edf.position += (uint64_t)Canvas::zoom_levels[ edf.zoom_factor ] * globals::tp_1sec;
	}
      
      else if ( event.GetKeyCode() == WXK_RIGHT ) // 10% shift
	{    
	  edf.position += (uint64_t)Canvas::zoom_levels[ edf.zoom_factor ] * 0.1 * globals::tp_1sec;
	}
      
      else if ( event.GetKeyCode() == WXK_LEFT ) // 10% shift
	{    
	  uint64_t shift = (uint64_t)Canvas::zoom_levels[ edf.zoom_factor ] * 0.1 * globals::tp_1sec;
	  if ( shift > edf.position ) edf.position = 0;
	  else edf.position -= shift;
	}
    }

  if ( edf.position >= edf.edf->timeline.last_time_point_tp ) 
    edf.position = edf.edf->timeline.last_time_point_tp - 1;

  // old/incorrect
  // if ( edf.position >= edf.total_size ) 
  //   edf.position = edf.total_size - 1 ;

  Refresh();
  
}


void Viewer::update_lists()
{

  // if a new sample has been selected, need to change the list boxes
  //  filename->SetLabel( "Sample ID: " + edf.current_sample_id );
    
  signal_box->Clear();
  annot_box->Clear();  
  
  wxArrayString signals;
  int ns = edf.num_channels();
  for (int i=0;i<ns;i++) 
    {
      //      std::cout << "add [" <<  edf.channel_labels[i].substr(0,9)  << "]\n";
      if ( edf.channel_labels[i].substr(0,9) != "EDF Annot" )
	signals.Add( edf.channel_labels[i] );
    }

  if ( signals.size() > 0 )
    signal_box->InsertItems( signals, 0 );

  // annots

  wxArrayString annots;
  std::vector<std::string> annot_labels = edf.edf->timeline.annotations.display_names();
  int na = annot_labels.size();
  for (int i=0;i<na;i++) 
    {
      //      std::cout << "annot_labels[i] = [" << annot_labels[i] << "]\n";

      if ( annot_labels[i] != "EDF Annotations" )
	annots.Add( annot_labels[i] );
    }

//   std::vector<std::string> annot_keys = edf.edf->timeline.annotations.names();
//   for (int aa=0;aa<annot_keys.size();aa++) std::cout << "name = " << annot_keys[aa] << "\n";

  if ( annots.size() > 0 ) 
    {
      annot_box->InsertItems( annots , 0 );
      annot_box2->InsertItems( annots , 0 );
    }

  // flag signals checked on last sample
  
  int cnt = edf.channel_labels.size();
  for (int i=0;i<cnt;i++) if ( last_signals.find( edf.channel_labels[i] ) != last_signals.end() ) signal_box->Check( i );
  
  cnt = annot_labels.size();
  for (int i=0;i<cnt;i++) if ( last_annot.find( annot_labels[i] ) != last_annot.end() ) annot_box->Check( i );
  for (int i=0;i<cnt;i++) if ( last_annot2.find( annot_labels[i] ) != last_annot2.end() ) annot_box2->Check( i );

  // specific annots?
  specific_annot_box->Clear();
  
  last_signals.clear();
  last_annot.clear();
  last_annot2.clear();

  
  //
  // from check boxes, now update the actual edf representation
  //
  
  cnt = signal_box->GetCount();
  
  edf.channels.clear();
  for (int i=0;i<cnt;i++)
    {
      bool selected = signal_box->IsChecked(i);
      if ( selected ) edf.channels.insert( i );
    }

  // annots
  cnt = annot_box->GetCount();  
  edf.annots.clear();
  
  for (int i=0;i<cnt;i++)
    {
      bool selected = annot_box->IsChecked(i);
      if ( selected )
	{
	  std::string label = std::string(annot_box->GetString(i));	  
	  annot_t * a = edf.edf->timeline.annotations.find( label );
	  if ( a != NULL ) edf.annots[ label ] = a ;
	}
    }

  // Annot2 (specific annotations)

  cnt = annot_box2->GetCount();
  std::set<annot_t*> sannots;
  for (int i=0;i<cnt;i++)
    {
      bool selected = annot_box2->IsChecked(i);
      if ( selected )
	{
	  std::string label = std::string(annot_box2->GetString(i));	  
	  annot_t * a = edf.edf->timeline.annotations.find( label );
	  if ( a != NULL && label != "EDF Annotations" ) sannots.insert( a );	  
	}
    }

  // Populate specific annot box

  std::map<std::string,interval_evt_map_t> allannots;
  std::map<feature_t,int> features; // interval + labels   
  std::set<annot_t*>::const_iterator ai = sannots.begin();
  while ( ai != sannots.end() )
    {      
      
      std::string aclass = (*ai)->name;
      allannots[ aclass ] = (*ai)->extract( interval_t( 0 , edf.total_size ) );      
      interval_evt_map_t & tmp = allannots[ (*ai)->name ];      
      interval_evt_map_t::const_iterator ii = tmp.begin();
      while ( ii != tmp.end() )
	{	 

	  feature_t f;
	  f.feature = ii->first;
	  f.window = ii->first;
	  

	  // get event(s)
	  const std::vector<const event_t*> & events = ii->second;
	  std::string label = "";
	  for (int e=0;e<events.size();e++)
	    {
	      if ( e!=0 ) label += ",";
	      label += events[e]->label;
	      f.add_data( events[e]->metadata );
	    }
	  
	  if ( label != aclass )
	    f.signal = label;
	  
	  f.label = aclass;
	  
	  // store
	  features[ f ] = features.size() - 1 ; 
	  
	  // next interval
	  ++ii;
	}

      // next annotation 
      ++ai;
    }
  
  // Populate specific annotation box

  specific_annot_positions.clear();
  specific_annot_features.clear();
  specific_annot_box->Clear();
  
  wxArrayString specific_annots;
  std::map<feature_t,int>::const_iterator ff = features.begin();
  while ( ff != features.end() )
    {
      specific_annots.Add( ff->first.as_string() );
      specific_annot_positions.push_back( ff->first.feature.mid() );
      specific_annot_features.push_back( ff->first );
      ++ff;
    }
  
  if ( specific_annots.size() > 0 ) 
    specific_annot_box->InsertItems( specific_annots , 0 );    

  
  //
  // Finally, after updating the lists, now update the canvas
  //

  canvas->Refresh();

}
