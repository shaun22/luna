
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#ifndef __EDF_HANDLER_H__
#define __EDF_HANDLER_H__

#include <set>
#include <string>
#include "../edf/edf.h"
#include "../annot/annot.h"
#include "grid.h"

struct edf_handler_t
{
  
  edf_handler_t()
  {
    edf = NULL;
    position = 0;
    zoom_factor = 2; // 10sec
    current_sample_id = "";
  }
  
  edf_handler_t( edf_t * e ) 
  {
    edf = e;
    position = 0;
    zoom_factor = 2; // 10sec
    current_sample_id = "";
  }
  
  void attach( edf_t * e )
  { 
    edf=e;
    channels.clear();
    channel_labels.clear();
    for (int i=0;i<edf->header.ns;i++) 
      { channel_labels.push_back( edf->header.label[i] ); }    
  }

  int  num_channels() const { return channel_labels.size(); }
  void add_channel( int c) { channels.insert(c); }  
  void remove_channel( int c ) { channels.erase( channels.find(c) ); }  
  void remove_channels() { channels.clear(); }
  
  // obtain data from EEG
  bool pull_data();
  
  edf_t      * edf;

  std::map<std::string,annot_t*> annots;
  
  std::set<int> channels;    
  signal_list_t signals;

  std::vector<std::string> channel_labels;
  
  uint64_t total_size;

  interval_t interval;
  
  uint64_t duration;
  
  double   duration_sec;

  mslice_t * slice;

  mslice_t * fft_slice;
  
  // from GUI
  int      zoom_factor;
  uint64_t position; // left base in MSEC
  
  bool use_downsampling;

  int  max_size;

  // sample handling  
  std::string current_sample_id;
  void read_samples( const std::string & ); 
  void attach_sample( const std::string & s );
  std::vector<std::string> sample;
  std::map<std::string,std::vector<std::string> > annot_files;
  std::map<std::string,std::string> edf_files; 
  
};

#endif
