
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "wx/wx.h"

#include "../edf/edf.h"
#include "../timeline/timeline.h"
#include "../annot/annot.h"
#include "../defs/defs.h"
#include "../db/db.h"

#include "edf-handler.h"

#include "grid.h"

edf_handler_t edf;

globals global;

writer_t writer;

class MyApp : public wxApp
{
public:
  virtual bool OnInit();
};

std::map<std::string,std::string>  cmd_t::label_aliases;
std::map<std::string,std::vector<std::string> >  cmd_t::primary_alias;

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{

  
  if ( wxGetApp().argc != 2 && wxGetApp().argc != 3 ) 
    {
      std::cerr << "./scope sample.lst\n";
      std::exit(1);
    }

  global.init_defs();


  //
  // extra annotation folder?
  //

  if (  wxGetApp().argc == 3 ) 
    {
      wxString  afolder( wxGetApp().argv[2]);
      globals::annot_folder = afolder.ToStdString();
    }

  //
  // Get sample list
  //

  wxString  fn( wxGetApp().argv[1]);
  edf.read_samples( fn.ToStdString() );

  
  // display GUI
  
  Viewer * fgs = new Viewer( wxT("scope|8-jun-17|v1.0") );

  fgs->Show(true);

  while (1)
    {
      break;
      std::string line; //stores the most recent line of input
      while(std::getline(std::cin, line)) //read entire lines at a time
	{
	  std::cout << "[" << line << "]\n";
	  break;
	}
      
    }
  
  return true;
}
