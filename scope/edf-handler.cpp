
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------

#include "edf-handler.h"
#include "../helper/helper.h"
#include <dirent.h>


bool edf_handler_t::pull_data()
{
    
  uint64_t window     = 
    Canvas::zoom_levels[ zoom_factor ] * globals::tp_1sec;
  
  uint64_t halfwindow = window / 2;

  uint64_t start = halfwindow < position ? position - halfwindow : 0 ;
  uint64_t stop  = start + window - 1;
  
  // reset position (ie. if hit boundary, this will give 'correct' center position)
  position = start + halfwindow;

  if ( stop >= total_size ) stop = total_size - (uint64_t)1;
  
  if ( Canvas::zoom_levels[ zoom_factor ] == 0 ) // whole trace
    {
      start = 0;
      stop = total_size - (uint64_t)1;
    }

  interval.start = start;
  interval.stop  = stop;

  signals.clear();
  std::set<int>::const_iterator ci = channels.begin();
  while ( ci != channels.end() ) 
    { 
      signals.add( *ci , channel_labels[ *ci ] ); 
      ++ci; 
    } 
  
  interval_t interval( start , stop );
  
  int ds_factor = use_downsampling ? Canvas::downsampling[ zoom_factor ] : 1 ;
  
  slice = new mslice_t( *edf , signals , interval , ds_factor );

  // no down-sampling for FFT
  if ( Canvas::show_fft )
    {
      fft_slice = new mslice_t( *edf , signals , interval );
    }
  return true;
}



void edf_handler_t::read_samples( const std::string & filename )
{
  
  sample.clear();
  edf_files.clear();
  annot_files.clear();
  
  std::ifstream IN1( filename.c_str() , std::ios::in );

  while ( ! IN1.eof() )
    {
      
      std::string line;
      std::getline( IN1 , line );
      if ( IN1.bad() ) continue;
      if ( line == "" ) continue;

      // ID  EDF  annot files...
      std::vector<std::string> tok = Helper::parse( line , "\t " );
      if ( tok.size() < 2 ) Helper::halt( "line too short" );

      const std::string & id = tok[0];
      
      sample.push_back( id );   // ID
      edf_files[ id ] =tok[1] ; // EDF

      // add any global annot folder
      if ( globals::annot_folder != "" ) 
	{
	  tok.push_back(  globals::annot_folder );
	}
      
      for (int j=2;j<tok.size();j++)
	{
	  // expand any folders/
	  
	  
	  if ( Helper::is_folder( tok[j] ) )
	    {
	  
	      DIR * dir;		  
	      struct dirent *ent;
	      if ( (dir = opendir ( tok[j].c_str() ) ) != NULL )
		{
		  /* print all the files and directories within directory */
		  while ((ent = readdir (dir)) != NULL)
		    {
		      std::string fname = ent->d_name;		      
		      if ( Helper::file_extension( fname , "ftr" ) ||
			   Helper::file_extension( fname , "xml" ) )
			annot_files[ id ].push_back( tok[j] + fname );	 			   
		    }
		  closedir (dir);
		}
	      else 
		Helper::halt( "could not open folder " + tok[j] );
	    }
	  else
	    {
	      if ( Helper::fileExists( tok[j] ) )
		annot_files[ id ].push_back( tok[j] );
	      else
		std::cerr << "Warning: could not locate [" << tok[j] << "]\n";
	    }
	}

      std::cerr << annot_files[id].size() << " annotation files for " << id << ":\n";
      for (int i=0;i<annot_files[id].size();i++) std::cerr << " " << annot_files[id][i] << "\n";

    }
  IN1.close();
  std::cerr << "read " << sample.size() << " samples\n";
  
  // attach first sample
  if ( sample.size() > 0 )
    attach_sample( sample[0] );


}



void edf_handler_t::attach_sample( const std::string & s )
{

  //
  // clean up (this also cleans up all annotations
  //

  if ( edf ) 
    {
      delete edf;
      edf = NULL;
    }

  // clear this (the actual annot_t will already have been removed)
  annots.clear();


  //
  // Attach an EDF
  //
  
  if ( edf_files.find( s ) == edf_files.end() )
    Helper::halt( "no EDF for " + s );
  
  const std::string edffile   = edf_files[ s ];
  
  edf_t * rawedf = new edf_t;  
  
  bool okay = rawedf->attach( edffile , s );
    
  if ( ! okay ) Helper::halt( "problem reading EDF file : " + edffile );

  std::cout << "sample " << s << " attached\n";
  
  //
  // read any annotation files
  //
  
  const int na = annot_files[ s ].size();

  std::cout << "expecting " << na << " annotations\n";

  for (int a=0;a<na;a++)
    {

      const std::string & annotfile = annot_files[ s ][ a ];

      // Load fully any XML annotations; register names for any non-XML files
      if ( ! rawedf->populate_alist( annotfile ) ) continue;
      
      // Do not process XML files here; these have already been added
      if ( Helper::file_extension( annotfile , "xml" ) ) continue;
      if ( Helper::file_extension( annotfile , "ftr" ) ) continue;

      // Get annot name in this file
      if ( rawedf->flist.find( annotfile ) == rawedf->flist.end() ) 
	Helper::halt( "problem finding " + annotfile );

      const std::string & annot_name = rawedf->flist[ annotfile ];
        
      // Connect to the timeline
      annot_t * myannot = rawedf->timeline.annotations.add( annot_name );
      
      bool loaded = myannot->load( annotfile );

      if ( ! loaded ) 
	std::cerr << "Warning... could not find annotation " << annot_name << " in " << annotfile << "\n";
      else
	std::cerr << "loaded annotation " << annot_name << " from " << annotfile << "\n";

    }

  
  //
  // Try to make a unified SleepStage variable
  //
  
  // SHHS/NSRR specific
  bool created_new_NSRR_sleep_stage = 
    rawedf->timeline.annotations.make_sleep_stage( );

  if ( created_new_NSRR_sleep_stage ) 
    {
      annot_files[ rawedf->filename ].push_back( "SleepStage" );
    }  
    
  //
  // attach EDF and timeline to this handler
  //
  
  attach( rawedf );


  //
  // Set total size
  //

  total_size = rawedf->timeline.last_time_point_tp + 1;

  //  std::cout << "total size = " << total_size << "\n";
  
  //
  // Update total_size based on the annotations rather than the EDF (last_time_point_tp of signal)
  //
  
  std::map<std::string,annot_t*>::const_iterator ai = rawedf->timeline.annotations.annots.begin();
  while ( ai != rawedf->timeline.annotations.annots.end() )
    {

      annot_t * annot = ai->second;      
      
      uint64_t max_tp = annot->maximum_tp();

      //      std::cout << "max t = " << ai->first << " --> " << max_tp << "\n";
      
      if ( max_tp >= total_size ) 
	{
	  total_size = max_tp + 1;
	  //std::cout << "adjusting total size to " << total_size << "\n";
	}
      ++ai;
    }
  
  //
  // and ID
  //

  current_sample_id = s;

}

