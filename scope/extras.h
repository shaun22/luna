
//    --------------------------------------------------------------------
//
//    This file is part of Luna.
//
//    LUNA is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Luna is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Luna. If not, see <http://www.gnu.org/licenses/>.
//
//    Please see LICENSE.txt for more details.
//
//    --------------------------------------------------------------------


#include <vector>
#include <map>
#include <string>
#include <stdint.h>

std::map<std::string,std::vector<double> > scope_spindle_slowwave_calc( const std::vector<double> * d , 
									const std::vector<uint64_t> * tp , 
									const int fs , 
									const int npix );
  // input data = 'd' with sampling rate 'fs'
  // generate npix levels of output bins for the following:
  
  //  - wavelet coeff. for 11 Hz, 15 Hz
  //  - slow waves, and instantaneous phase

void scope_spindle_slowwave_draw( std::map<std::string,std::vector<double> > & d );
